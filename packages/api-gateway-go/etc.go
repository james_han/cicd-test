package main

import (
	"errors"
	"fmt"
	"io"
	"math/big"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"plugin"
	"regexp"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/labstack/echo/v4"
)

type AuthFunc func(events.APIGatewayV2CustomAuthorizerV2Request) (events.APIGatewayV2CustomAuthorizerSimpleResponse, error)

const PLUGIN_FOLDER = ".plugin_handlers"

func compileAndLoadHandler(path string, handlerName string) (plugin.Symbol, error) {
	// Create temporary directory for the compiled package
	err := os.Mkdir(PLUGIN_FOLDER, 0755)
	if err != nil && !os.IsExist(err) {
		fmt.Printf("Error creating folder: %v\n", err)
		return nil, err
	}

	// defer os.RemoveAll(tmpDir)

	// Compile package as a plugin
	outputFile := filepath.Join(PLUGIN_FOLDER, base62Encode(path)+".so")
	args := []string{"build", "-buildmode=plugin", "-o", outputFile, path}
	cmd := exec.Command("go", args...)
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return nil, fmt.Errorf("error compiling package: %v", err)
	}

	// Load the compiled plugin
	handlerPlugin, err := plugin.Open(outputFile)
	if err != nil {
		return nil, fmt.Errorf("error opening plugin: %v", err)
	}

	// Get the Handler symbol
	handlerSymbol, err := handlerPlugin.Lookup(handlerName)
	if err != nil {
		return nil, fmt.Errorf("error looking up symbol '%s': %v", handlerName, err)
	}

	return handlerSymbol, nil
}

func getAuthorizerFunction(path string) (AuthFunc, error) {
	handlerSymbol, err := compileAndLoadHandler(path, "Handler")
	if err != nil {
		return nil, err
	}

	// Assert the function type
	handlerFunc, ok := handlerSymbol.(func(request events.APIGatewayV2CustomAuthorizerV2Request) (events.APIGatewayV2CustomAuthorizerSimpleResponse, error))
	if !ok {
		return nil, errors.New("error asserting function type")
	}

	return handlerFunc, nil
}

func getHandlerFunction(path string) (func(events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error), error) {
	handlerSymbol, err := compileAndLoadHandler(path, "Handler")
	if err != nil {
		return nil, err
	}

	// Assert the function type
	handlerFunc, ok := handlerSymbol.(func(events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error))
	if !ok {
		return nil, errors.New("error asserting function type")
	}

	return handlerFunc, nil
}

type FunctionsYaml struct {
	Authorizers []FunctionAuthorizer `yaml:"authorizers"`
	Routes      []FunctionRoute      `yaml:"routes"`
}

type FunctionAuthorizer struct {
	Name     string `yaml:"name"`
	Handler  string `yaml:"handler"`
	CacheTTL string `yaml:"cacheTTl"`
}

type FunctionRoute struct {
	Path           string `yaml:"path"`
	Method         string `yaml:"method"`
	Handler        string `yaml:"handler"`
	AuthorizerName string `yaml:"authorizerName"`
}

type PathInfo struct {
	Path string
	Keys []string
}

func processPath(input string) PathInfo {
	regex := regexp.MustCompile("{([^}]+)}")

	matches := regex.FindAllStringSubmatch(input, -1)
	keys := make([]string, len(matches))

	for i, match := range matches {
		key := match[1]
		input = strings.Replace(input, "{"+key+"}", ":"+key, 1)
		keys[i] = key
	}

	return PathInfo{
		Path: input,
		Keys: keys,
	}
}

func cookiesToStrings(cookies []*http.Cookie) []string {
	result := make([]string, len(cookies))
	for i, cookie := range cookies {
		result[i] = cookie.String()
	}
	return result
}

func headersToString(headers http.Header) map[string]string {
	result := make(map[string]string)
	for k, v := range headers {
		result[strings.ToLower(k)] = strings.Join(v, ",")
	}
	return result
}

func queryToString(queryParams url.Values) map[string]string {
	result := make(map[string]string)
	for k, v := range queryParams {
		if len(v) > 0 {
			result[k] = v[0]
		}
	}
	return result
}

func paramsToString(c echo.Context, keys []string) map[string]string {
	result := make(map[string]string)
	for _, v := range keys {
		result[v] = c.Param(v)
	}
	return result
}

func parseRequestBody(c echo.Context) string {
	body, err := io.ReadAll(c.Request().Body)
	if err != nil {
		panic(err)
	}
	bodyString := string(body)
	return bodyString
}

const base62Chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

func base62Encode(input string) string {
	base := big.NewInt(62)
	remainder := new(big.Int)
	num := new(big.Int)

	// Convert the input string to a big.Int (treat it as a base-256 number)
	for _, b := range []byte(input) {
		num.Mul(num, big.NewInt(256))
		num.Add(num, big.NewInt(int64(b)))
	}

	// Encode the big.Int to a base62 string
	var encoded string
	for num.Cmp(big.NewInt(0)) > 0 {
		num.DivMod(num, base, remainder)
		encoded = string(base62Chars[remainder.Uint64()]) + encoded
	}

	return encoded
}
