package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gopkg.in/yaml.v2"
)

func main() {
	filePath := flag.String("f", "", "Path to the functions.yaml file")
	flag.Parse()
	if *filePath == "" {
		panic("Error: Please provide a functions.yaml file path using the -f flag.")
	}

	var (
		content []byte
		err     error
	)

	if content, err = os.ReadFile(*filePath); err != nil {
		panic(fmt.Sprintf("Error reading file: %v\n", err))
	}

	var functions FunctionsYaml
	if err = yaml.Unmarshal(content, &functions); err != nil {
		panic(fmt.Sprintf("Error unmarshaling YAML: %v\n", err))
	}

	e := echo.New()
	e.Use(middleware.Logger())

	var authorizers map[string]AuthFunc = make(map[string]AuthFunc, len(functions.Authorizers))
	for _, authorizer := range functions.Authorizers {
		fmt.Printf("\nAuthorizer를 찾습니다\n\t└───┤ %s\n", authorizer.Handler)
		var tmpAuthorizer AuthFunc
		if tmpAuthorizer, err = getAuthorizerFunction(authorizer.Handler); err != nil {
			panic(err)
		}
		authorizers[authorizer.Name] = tmpAuthorizer
		fmt.Printf("\t└───┤ %s \n", authorizer.Name)
	}

	for _, route := range functions.Routes {
		var handler func(events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error)
		fmt.Printf("\n핸들러를 찾습니다\n\t└───┤ %s\n", route.Handler)
		if handler, err = getHandlerFunction(route.Handler); err != nil {
			panic(fmt.Sprintf("Error getting handler function: %v\n", err))
		}

		// todo when has authorizer...
		pathInfo := processPath(route.Path)
		fmt.Printf(
			"\t└───┤ %s%s %s\n",
			func() string {
				if route.AuthorizerName != "" {
					return "\033[0;31m[🔒 " + route.AuthorizerName + "]\033[0m "
				}
				return ""
			}(),
			route.Method,
			pathInfo.Path,
		)
		e.Add(route.Method, pathInfo.Path, func(c echo.Context) error {
			var (
				req = events.APIGatewayV2HTTPRequest{
					Version:               "2.0",
					RouteKey:              route.Path,
					RawPath:               c.Path(),
					RawQueryString:        c.Request().URL.RawQuery,
					Cookies:               cookiesToStrings(c.Cookies()),
					Headers:               headersToString(c.Request().Header),
					QueryStringParameters: queryToString(c.QueryParams()),
					PathParameters:        paramsToString(c, pathInfo.Keys),
					RequestContext: events.APIGatewayV2HTTPRequestContext{
						AccountID:    "mockAccountID",
						APIID:        "api-id",
						DomainName:   "id.execute-api.us-east-1.amazonaws.com",
						DomainPrefix: "id",
						HTTP: events.APIGatewayV2HTTPRequestContextHTTPDescription{
							Method:    c.Request().Method,
							Path:      c.Path(),
							Protocol:  c.Request().Proto,
							SourceIP:  "127.0.0.1",
							UserAgent: c.Request().UserAgent(),
						},
						RequestID: "241b50ed-ffd9-4297-8a88-137b069815a5",
						RouteKey:  route.Path,
						Stage:     "local",
						Time:      time.Now().UTC().Format("02/Jan/2006:15:04:05 -0700"),
						TimeEpoch: time.Now().UnixNano() / int64(time.Millisecond),
						Authorizer: &events.APIGatewayV2HTTPRequestContextAuthorizerDescription{
							Lambda: c.Get("authContext").(map[string]interface{}),
						},
					},
					Body: parseRequestBody(c),
				}
				ret events.APIGatewayProxyResponse
			)

			ret, err = handler(req)

			for k, v := range ret.Headers {
				c.Response().Header().Set(k, v)
			}
			c.Response().WriteHeader(ret.StatusCode)

			if err != nil {
				_, err = fmt.Fprint(c.Response(), err.Error())
				return nil
			}
			_, err = fmt.Fprint(c.Response(), ret.Body)
			return err
		}, makeAuthorizeMiddleware(pathInfo, route, authorizers))
	}

	serverStarted := make(chan struct{})
	go func() {
		e.Listener, _ = net.Listen("tcp", "127.0.0.1:80")
		close(serverStarted) // Signal that the server has started
		e.Logger.Fatal(e.Start(""))
	}()

	<-serverStarted
	fmt.Printf("\n\n[Routers]\n")
	for _, route := range e.Routes() {
		fmt.Printf("%-7s %-25s\n", route.Method, "http://127.0.0.1"+route.Path)
	}
	select {}
}

func makeAuthorizeMiddleware(pathInfo PathInfo, route FunctionRoute, authorizers map[string]AuthFunc) func(next echo.HandlerFunc) echo.HandlerFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if _a, ok := authorizers[route.AuthorizerName]; ok {
				_ret, err := _a(events.APIGatewayV2CustomAuthorizerV2Request{
					Version:               "2.0",
					Type:                  "REQUEST",
					RouteArn:              "arn:aws:execute-api:us-east-1:123456789012:abcdef123/test/GET/request",
					IdentitySource:        []string{"todoAuthorizer"},
					RouteKey:              route.Path,
					RawPath:               c.Path(),
					RawQueryString:        c.Request().URL.RawQuery,
					Cookies:               cookiesToStrings(c.Cookies()),
					Headers:               headersToString(c.Request().Header),
					QueryStringParameters: queryToString(c.QueryParams()),
					PathParameters:        paramsToString(c, pathInfo.Keys),
					RequestContext: events.APIGatewayV2HTTPRequestContext{
						AccountID:    "mockAccountID",
						APIID:        "api-id",
						DomainName:   "id.execute-api.us-east-1.amazonaws.com",
						DomainPrefix: "id",
						HTTP: events.APIGatewayV2HTTPRequestContextHTTPDescription{
							Method:    c.Request().Method,
							Path:      c.Path(),
							Protocol:  c.Request().Proto,
							SourceIP:  "127.0.0.1",
							UserAgent: c.Request().UserAgent(),
						},
						RequestID: "241b50ed-ffd9-4297-8a88-137b069815a5",
						RouteKey:  route.Path,
						Stage:     "local",
						Time:      time.Now().UTC().Format("02/Jan/2006:15:04:05 -0700"),
						TimeEpoch: time.Now().UnixNano() / int64(time.Millisecond),
					},
				})
				if err != nil {
					return c.String(500, err.Error())
				}
				if !_ret.IsAuthorized {
					return c.String(403, "Forbidden")
				}
				c.Set("authContext", _ret.Context)
			} else if !ok && route.AuthorizerName != "" {
				// Authorizer가 지정되어있는데 인증 핸들러 함수를 못찾았을 때
				panic(fmt.Sprintf("Can't find authorizer function [%s]", route.AuthorizerName))
			} else {
				// Authorizer가 지정되어있지 않은 경우
				c.Set("authContext", make(map[string]interface{})) // Dummy
			}
			return next(c)
		}
	}
}
