module rp-order-information-api

go 1.20

require (
	github.com/aws/aws-lambda-go v1.46.0
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.5.0
	github.com/gorilla/schema v1.2.1
	github.com/jmoiron/sqlx v1.3.5
	github.com/relvacode/iso8601 v1.3.0
	golang.org/x/crypto v0.7.0
)

require (
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/leodido/go-urn v1.2.4 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
