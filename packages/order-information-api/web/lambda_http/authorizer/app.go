package main

import (
	"rp-order-information-api/modules/shared/auth"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

// 인가 처리
func Handler(request events.APIGatewayV2CustomAuthorizerV2Request) (events.APIGatewayV2CustomAuthorizerSimpleResponse, error) {

	path := request.RequestContext.RouteKey
	pathArr := strings.Split(path, "/")

	if len(pathArr) < 1 {
		return events.APIGatewayV2CustomAuthorizerSimpleResponse{
			IsAuthorized: false,
		}, nil
	}
	resource := pathArr[0]
	resourceID := pathArr[1]
	token := request.Headers["authorization"]

	var action auth.Action
	// when if request method GET, then action should be read
	if request.RequestContext.HTTP.Method == "GET" {
		action = auth.ACTION_READ
	} else {
		action = auth.ACTION_WRITE
	}

	// 인가
	if err := auth.AuthRequest(token, resource, resourceID, action); err != nil {
		return events.APIGatewayV2CustomAuthorizerSimpleResponse{
			IsAuthorized: false,
		}, nil
	}

	var authContext map[string]interface{}

	if am, err := auth.NewAuthMemberFromJwtToken(token); err != nil {
		return events.APIGatewayV2CustomAuthorizerSimpleResponse{
			IsAuthorized: false,
		}, nil
	} else {
		authContext = am.ToMap()
	}

	return events.APIGatewayV2CustomAuthorizerSimpleResponse{
		IsAuthorized: true,
		Context:      authContext,
	}, nil
}

func main() {
	lambda.Start(Handler)
}
