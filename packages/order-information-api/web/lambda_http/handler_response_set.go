package lambda_http

import "github.com/aws/aws-lambda-go/events"

type HandlerResponseSet struct {
	Response events.APIGatewayProxyResponse
	Err      error
}
