package main

import (
	"errors"
	"net/http"
	"rp-order-information-api/db/mysql"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/modules/view_model/view_order_information"
	"rp-order-information-api/web/lambda_http"

	"github.com/aws/aws-lambda-go/events"
)

var dbconfig = mysql.NewConfigFromTestDB("realpacking")
var viewOrderInformationRepository *mysql.ViewOrderInformationRepository

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	if viewOrderInformationRepository == nil {
		viewOrderInformationRepository, err = mysql.NewViewOrderInformationRepository(dbconfig)
		if err != nil {
			return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
		}
	}

	id, err := vo.NewOrderInformationIdFromString(request.PathParameters["order-information-id"])
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	service := view_order_information.NewViewOrderInformationService(viewOrderInformationRepository)
	orderInformation, err := service.FindById(*id)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	if orderInformation == nil {
		return lambda_http.ErrorResponse(http.StatusNotFound, errors.New(http.StatusText(http.StatusNotFound)))
	}

	return lambda_http.SuccessResponseJson(http.StatusOK, lambda_http.SuccessResponse{Data: orderInformation, Message: http.StatusText(http.StatusOK)}, err)
}
