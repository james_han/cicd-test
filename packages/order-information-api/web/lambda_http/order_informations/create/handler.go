package main

import (
	"net/http"
	"rp-order-information-api/db/mysql"
	"rp-order-information-api/modules/order_information"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/web/lambda_http"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

var dbconfig = mysql.NewConfigFromTestDB("realpacking")
var orderInformationRepository *mysql.OrderInformationRepository

type requestParamter struct {
	WorkspaceId           string                `json:"workspaceId" example:"018da094-a30b-7033-b27c-7a32e620958f" format:"uuid"` // 워크스페이스 고유 id
	MemberId              string                `json:"memberId" example:"018da094-a30b-7033-b27c-7a32e620958f" format:"uuid"`    // 멤버 고유 id
	OrderId               string                `json:"orderId" example:"202101010001"`                                           // 주문번호
	Code                  string                `json:"code" example:"1234567890"`                                                // 주문코드 (송장번호)
	Courier               string                `json:"courier" example:"logen"`                                                  // 택배사
	Timezone              string                `json:"timezone" example:"Asia/Seoul"`                                            // 주문시간대
	Items                 []requestItemParamter `json:"items"`                                                                    // 주문상품
	OrdererName           string                `json:"ordererName" example:"홍길동"`                                                // 주문자명
	OrdererEmail          string                `json:"ordererEmail" example:"rptest\u0040gmail.com" format:"email"`              // 주문자 이메일
	OrdererCellPhoneCode  string                `json:"ordererCellPhoneCode" example:"82"`                                        // 주문자 휴대폰 국가번호
	OrdererCellPhone      string                `json:"ordererCellPhone" example:"01012345678" format:"phone"`                    // 주문자 휴대폰
	OrdererPhoneCode      string                `json:"ordererPhoneCode" example:"82"`                                            // 주문자 전화번호 국가번호
	OrdererPhone          string                `json:"ordererPhone" example:"01012345678" format:"phone"`                        // 주문자 전화번호
	ReceiverName          string                `json:"receiverName" example:"홍길동"`                                               // 수취인명
	ReceiverCellPhoneCode string                `json:"receiverCellPhoneCode" example:"82"`                                       // 수취인 휴대폰 국가번호
	ReceiverCellPhone     string                `json:"receiverCellPhone" example:"01012345678" format:"phone"`                   // 수취인 휴대폰
	ReceiverPhoneCode     string                `json:"receiverPhoneCode" example:"82"`                                           // 수취인 전화번호 국가번호
	ReceiverPhone         string                `json:"receiverPhone" example:"01012345678" format:"phone"`                       // 수취인 전화번호
	ReceiverPostCode      string                `json:"receiverPostCode" example:"12345"`                                         // 수취인 우편번호
	ReceiverAddress       string                `json:"receiverAddress" example:"서울시 강남구 역삼동 123-456"`                            // 수취인 주소
	ShopName              string                `json:"shopName" example:"realpacking"`                                           // 매출처
	CustomerId            string                `json:"customerId" example:"202101010001"`                                        // 고객 id
	OrderedAt             string                `json:"orderedAt" example:"2021-01-01T00:00:00+09:00"`                            // 주문일시
}

type requestItemParamter struct {
	Price             int    `json:"price" example:"10000"`                                                      // 가격
	Amount            int    `json:"amount" example:"1"`                                                         // 수량
	ProductId         string `json:"productId" example:"202101010001"`                                           // 상품 고유 id
	ProductBarcode    string `json:"productBarcode" example:"1234567890"`                                        // 상품 바코드
	ProductCustomCode string `json:"productCustomCode" example:"1234567890"`                                     // 자체상품코드 / SKU 코드
	ProductName       string `json:"productName" example:"상품명"`                                                  // 상품명
	ProductLink       string `json:"productLink" example:"https://realpacking.com/product/202101010001"`         // 상품링크
	ProductImageUrl   string `json:"productImageUrl" example:"https://realpacking.com/product/202101010001.jpg"` // 상품이미지 url
	IsInspected       bool   `json:"isInspected" example:"false"`                                                // 검수여부
	InspectedAmount   int    `json:"inspectedAmount" example:"0"`                                                // 검수수량
}

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	if orderInformationRepository == nil {
		orderInformationRepository, err = mysql.NewOrderInformationRepository(dbconfig)
		if err != nil {
			return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
		}
	}

	requestParams, err := lambda_http.ParseJsonRequestBody[requestParamter](request)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	workspaceId, err := vo.NewWorkspaceIdFromString(requestParams.WorkspaceId)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	memberId, err := vo.NewMemberIdFromString(requestParams.MemberId)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	createItemParamter := make([]order_information.CreateItemParameter, len(requestParams.Items))
	for i, item := range requestParams.Items {
		createItemParamter[i] = order_information.CreateItemParameter{
			Price:             item.Price,
			Amount:            item.Amount,
			ProductId:         item.ProductId,
			ProductBarcode:    item.ProductBarcode,
			ProductCustomCode: item.ProductCustomCode,
			ProductName:       item.ProductName,
			ProductLink:       item.ProductLink,
			ProductImageUrl:   item.ProductImageUrl,
			IsInspected:       item.IsInspected,
			InspectedAmount:   item.InspectedAmount,
		}
	}

	createParameter := order_information.CreateParameter{
		WorkspaceId:           *workspaceId,
		MemberId:              *memberId,
		OrderId:               requestParams.OrderId,
		Code:                  requestParams.Code,
		Courier:               requestParams.Courier,
		Timezone:              requestParams.Timezone,
		Items:                 createItemParamter,
		OrdererName:           requestParams.OrdererName,
		OrdererEmail:          requestParams.OrdererEmail,
		OrderCellPhoneCode:    requestParams.OrdererCellPhoneCode,
		OrdererCellPhone:      requestParams.OrdererCellPhone,
		OrderPhoneCode:        requestParams.OrdererPhoneCode,
		OrdererPhone:          requestParams.OrdererPhone,
		ReceiverName:          requestParams.ReceiverName,
		ReceiverCellPhoneCode: requestParams.ReceiverCellPhoneCode,
		ReceiverCellPhone:     requestParams.ReceiverCellPhone,
		ReceiverPhoneCode:     requestParams.ReceiverPhoneCode,
		ReceiverPhone:         requestParams.ReceiverPhone,
		ReceiverPostCode:      requestParams.ReceiverPostCode,
		ReceiverAddress:       requestParams.ReceiverAddress,
		ShopName:              requestParams.ShopName,
		CustomerId:            requestParams.CustomerId,
		OrderedAt:             requestParams.OrderedAt,
	}

	service := order_information.NewOrderInformationService(orderInformationRepository)
	orderInformation, err := service.CreateOrderInformation(createParameter)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	return lambda_http.SuccessResponseJson(http.StatusCreated, lambda_http.SuccessResponse{Data: orderInformation, Message: http.StatusText(http.StatusCreated)}, err)
}

func main() {
	lambda.Start(Handler)
}
