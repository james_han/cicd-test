package main

import (
	"errors"
	"net/http"
	"rp-order-information-api/db/mysql"
	"rp-order-information-api/modules/order_information"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/web/lambda_http"

	"github.com/aws/aws-lambda-go/events"
)

type requestParameter struct {
	OrdererName           string `json:"ordererName" example:"홍길동"`                                   // 주문자명
	OrdererEmail          string `json:"ordererEmail" example:"rptest\u0040gmail.com" format:"email"` // 주문자 이메일
	OrdererCellPhoneCode  string `json:"ordererCellPhoneCode" example:"82"`                           // 주문자 휴대폰 국가번호
	OrdererCellPhone      string `json:"ordererCellPhone" example:"01012345678" format:"phone"`       // 주문자 휴대폰
	OrdererPhoneCode      string `json:"ordererPhoneCode" example:"82"`                               // 주문자 전화번호 국가번호
	OrdererPhone          string `json:"ordererPhone" example:"01012345678" format:"phone"`           // 주문자 전화번호
	ReceiverName          string `json:"receiverName" example:"홍길동"`                                  // 수취인명
	ReceiverCellPhoneCode string `json:"receiverCellPhoneCode" example:"82"`                          // 수취인 휴대폰 국가번호
	ReceiverCellPhone     string `json:"receiverCellPhone" example:"01012345678" format:"phone"`      // 수취인 휴대폰
	ReceiverPhoneCode     string `json:"receiverPhoneCode" example:"82"`                              // 수취인 전화번호 국가번호
	ReceiverPhone         string `json:"receiverPhone" example:"01012345678" format:"phone"`          // 수취인 전화번호
}

var dbconfig = mysql.NewConfigFromTestDB("realpacking")
var orderInformationRepository *mysql.OrderInformationRepository

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	if orderInformationRepository == nil {
		orderInformationRepository, err = mysql.NewOrderInformationRepository(dbconfig)
		if err != nil {
			return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
		}
	}

	requestParams, err := lambda_http.ParseJsonRequestBody[requestParameter](request)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	id, err := vo.NewOrderInformationIdFromString(request.PathParameters["order-information-id"])
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	updateParams := order_information.UpdateParameter{
		OrdererName:           requestParams.OrdererName,
		OrdererEmail:          requestParams.OrdererEmail,
		OrdererCellPhoneCode:  requestParams.OrdererCellPhoneCode,
		OrdererCellPhone:      requestParams.OrdererCellPhone,
		OrdererPhoneCode:      requestParams.OrdererPhoneCode,
		OrdererPhone:          requestParams.OrdererPhone,
		ReceiverName:          requestParams.ReceiverName,
		ReceiverCellPhoneCode: requestParams.ReceiverCellPhoneCode,
		ReceiverCellPhone:     requestParams.ReceiverCellPhone,
		ReceiverPhoneCode:     requestParams.ReceiverPhoneCode,
		ReceiverPhone:         requestParams.ReceiverPhone,
	}

	service := order_information.NewOrderInformationService(orderInformationRepository)

	orderInformation, err := service.UpdateById(*id, updateParams)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	if orderInformation == nil {
		return lambda_http.ErrorResponse(http.StatusNotFound, errors.New(http.StatusText(http.StatusNotFound)))
	}

	return lambda_http.SuccessResponseJson(http.StatusOK, lambda_http.SuccessResponse{Data: orderInformation, Message: http.StatusText(http.StatusOK)}, err)
}
