package main

import (
	"net/http"
	"rp-order-information-api/db/mysql"
	"rp-order-information-api/modules/order_information"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/web/lambda_http"

	"github.com/aws/aws-lambda-go/events"
)

var dbconfig = mysql.NewConfigFromTestDB("realpacking")
var orderInformationRepository *mysql.OrderInformationRepository

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	if orderInformationRepository == nil {
		orderInformationRepository, err = mysql.NewOrderInformationRepository(dbconfig)
		if err != nil {
			return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
		}
	}

	id, err := vo.NewOrderInformationIdFromString(request.PathParameters["order-information-id"])
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	service := order_information.NewOrderInformationService(orderInformationRepository)
	if err := service.DeleteById(*id); err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	return lambda_http.SuccessResponseNoContent()
}
