package main

import (
	"errors"
	"net/http"
	"rp-order-information-api/db/mysql"
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/view_model/view_order_information"
	"rp-order-information-api/web/lambda_http"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type requestParameter struct {
	WorkspaceId        string `query:"workspace-id" example:"123e4567-e89b-12d3-a456-426614174000" format:"uuid" validate:"required"`
	OrderId            string `query:"order-id" example:"202101010001"`
	OrdererName        string `query:"orderer-name"  example:"홍길동"`
	Code               string `query:"code"  example:"1234567890"`
	OrderCellPhoneCode string `query:"orderer-cellphone-code" example:"82"`
	OrdererCellPhone   string `query:"orderer-cellphone" example:"01012345678" format:"phone"`
	OrdererEmail       string `query:"orderer-email" example:"rptest\u0040gmail.com" format:"email"`
	CreatedAfter       string `query:"created-after" example:"2021-01-01T00:00:00+09:00" format:"date-time"`
	CreatedBefore      string `query:"created-before" example:"2021-01-01T00:00:00+09:00" format:"date-time"`
	Page               string `query:"page" example:"1"`
	Cursor             string `query:"cursor" example:"123e4567-e89b-12d3-a456-426614174000" format:"uuid"`
	Sort               string `query:"sort" example:"asc" enums:"asc,desc"`
	Size               string `query:"size" validate:"gte=1" example:"10"`
	SortTarget         string `query:"sort-target" example:"id" enums:"id,created_at"`
}

var dbconfig = mysql.NewConfigFromTestDB("realpacking")
var viewOrderInformationRepository *mysql.ViewOrderInformationRepository

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	if viewOrderInformationRepository == nil {
		viewOrderInformationRepository, err = mysql.NewViewOrderInformationRepository(dbconfig)
		if err != nil {
			return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
		}
	}

	requestParams, err := lambda_http.ParseQueryString[requestParameter](request)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	criteria, err := createCriteria(*requestParams)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	service := view_order_information.NewViewOrderInformationService(viewOrderInformationRepository)
	orderInformations, paging, err := service.FindByCriteria(criteria)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	if orderInformations == nil {
		return lambda_http.SuccessResponseNoContent()
	}

	return lambda_http.SuccessResponseJson(http.StatusOK, lambda_http.SuccessPagedResponse{Data: orderInformations, Paging: paging, Message: http.StatusText(http.StatusOK)}, err)
}

func createCriteria(requestParams requestParameter) (*search.Criteria, error) {
	filters, err := createFilters(requestParams)
	if err != nil {
		return nil, err
	}

	pagingParams, err := createPagingParams(requestParams)
	if err != nil {
		return nil, err
	}

	sortOption, err := createSortOption(requestParams)
	if err != nil {
		return nil, err
	}

	criteria := search.NewCriteria(filters, sortOption, pagingParams)

	return &criteria, nil
}

func createFilters(requestParams requestParameter) ([]search.Filter, error) {
	filters := []search.Filter{}

	if requestParams.WorkspaceId != "" {
		filters = append(filters, search.NewFilter("workspaceId", search.Equal, requestParams.WorkspaceId))
	}

	if requestParams.OrderId != "" {
		filters = append(filters, search.NewFilter("orderId", search.Like, requestParams.OrderId))
	}

	if requestParams.OrdererName != "" {
		filters = append(filters, search.NewFilter("ordererName", search.Like, requestParams.OrdererName))
	}

	if requestParams.Code != "" {
		filters = append(filters, search.NewFilter("code", search.Like, requestParams.Code))
	}

	if requestParams.OrderCellPhoneCode != "" && requestParams.OrdererCellPhone != "" {
		filters = append(filters, search.NewFilter("orderCellPhoneCode", search.Equal, requestParams.OrderCellPhoneCode))
		filters = append(filters, search.NewFilter("ordererCellPhone", search.Like, requestParams.OrdererCellPhone))
	}

	if requestParams.OrdererEmail != "" {
		filters = append(filters, search.NewFilter("ordererEmail", search.Like, requestParams.OrdererEmail))
	}

	if requestParams.CreatedAfter != "" {
		filters = append(filters, search.NewFilter("createdAt", search.GreaterOrEqual, requestParams.CreatedAfter))
	}

	if requestParams.CreatedBefore != "" {
		filters = append(filters, search.NewFilter("createdAt", search.LessOrEqual, requestParams.CreatedBefore))
	}

	return filters, nil
}

func createSortOption(requestParams requestParameter) (*search.SortOption, error) {
	if requestParams.Sort == "" {
		requestParams.Sort = search.Descending.String()
	}

	sortOption := &search.SortOption{
		Field: requestParams.SortTarget,
		Order: search.SortOrder(requestParams.Sort),
	}

	return sortOption, nil
}

func createPagingParams(requestParams requestParameter) (*search.PagingParams, error) {
	var pagingParams *search.PagingParams
	if requestParams.Size != "" || requestParams.Cursor != "" || requestParams.Page != "" {
		pagingParams = &search.PagingParams{}

		if requestParams.Page != "" {
			page, err := strconv.Atoi(requestParams.Page)
			if err != nil {
				return nil, errors.New("invalid page")
			}
			pagingParams.Page = page
		}

		if requestParams.Size != "" {
			size, err := strconv.Atoi(requestParams.Size)
			if err != nil {
				return nil, errors.New("invalid size")
			}
			pagingParams.PageSize = size
		}

		if requestParams.Cursor != "" {
			pagingParams.Cursor = requestParams.Cursor
		}
	}

	return pagingParams, nil
}

func main() {
	lambda.Start(Handler)
}
