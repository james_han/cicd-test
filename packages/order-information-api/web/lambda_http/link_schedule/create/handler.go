package main

import (
	"net/http"
	"rp-order-information-api/db/mysql"
	linkschedule "rp-order-information-api/modules/link_schedule"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/web/lambda_http"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/relvacode/iso8601"
)

var dbconfig = mysql.NewConfigFromTestDB("realpacking")
var linkScheduleRepository *mysql.LinkScheduleRepository

type requestParameter struct {
	WorkspaceId    string `json:"workspaceId" validate:"required" example:"018da094-a30b-7033-b27c-7a32e620958f" format:"uuid"` //워크스페이스 id
	Oms            string `json:"oms" validate:"required" example:"cafe24"`                                                     //솔루션(카페24: cafe24, 고도몰: godoMall, 메이크샵: makeshop, 이지어드민: ezadmin, 사방넷: sabangnet, 샵플링: shopling)
	LinkType       string `json:"linkType" validate:"required" example:"orderDate"`                                             //연동 기준(주문일: orderDate, 송장입력일: waybillInputDate)
	LinkStartedAt  string `json:"linkStartedAt" validate:"required" example:"2021-01-01T00:00:00+09:00"`                        //연동 시작일
	LinkFinishedAt string `json:"linkFinishedAt" validate:"required" example:"2021-01-01T00:00:00+09:00"`                       //연동 종료일
}

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	// db connection check
	var err error
	if linkScheduleRepository == nil {
		linkScheduleRepository, err = mysql.NewLinkScheduleRepository(dbconfig)
		if err != nil {
			return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
		}
	}

	// requestParameter check
	requestParams, err := lambda_http.ParseJsonRequestBody[requestParameter](request)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	// requestParameter validation check
	workspaceId, err := vo.NewWorkspaceIdFromString(requestParams.WorkspaceId)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	oms, err := linkschedule.ParseOms(requestParams.Oms)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	linkType, err := linkschedule.ParseLinkType(requestParams.LinkType)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	linkStartedAt, err := iso8601.ParseString(requestParams.LinkStartedAt)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	linkFinishedAt, err := iso8601.ParseString(requestParams.LinkFinishedAt)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	// service call
	service := linkschedule.NewLinkScheduleService(linkScheduleRepository)
	res, err := service.CreateLinkSchedule(*workspaceId, oms, linkType, linkStartedAt, linkFinishedAt)

	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	return lambda_http.SuccessResponseJson(http.StatusCreated, lambda_http.SuccessResponse{Data: res, Message: http.StatusText(http.StatusCreated)}, err)
}

func main() {
	lambda.Start(Handler)
}
