package main

import (
	"errors"
	"fmt"
	"net/http"
	"rp-order-information-api/db/mysql"
	linkschedule "rp-order-information-api/modules/link_schedule"
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/web/lambda_http"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

var dbconfig = mysql.NewConfigFromTestDB("realpacking")
var linkScheduleRepository *mysql.LinkScheduleRepository

type requestParameter struct {
	WorkspaceId   string `query:"workspace-id" validate:"required" example:"018da094-a30b-7033-b27c-7a32e620958f" format:"uuid"` // 워크스페이스 id
	CreatedAfter  string `query:"created-after" example:"2023-01-01T09:00:00+09:00" format:"date-time"`                          // 연동스케줄 등록 시작일
	CreatedBefore string `query:"created-before" example:"2023-01-01T09:00:00+09:00" format:"date-time"`                         // 연동스케줄 등록 종료일
	OrderStatus   string `query:"order-status" example:"pending"`                                                                // 주문 상태
	Oms           string `query:"oms" example:"cafe24"`                                                                          // 솔루션
	LinkStatus    string `query:"link-status" example:"waiting"`                                                                 // 진행 상태
	Page          string `query:"page" example:"1" format:"int"`                                                                 // 페이지
	Cursor        string `query:"cursor" example:"123e4567-e89b-12d3-a456-426614174000" format:"uuid"`                           // 다음 페이지를 위한 커서
	Sort          string `query:"sort" example:"asc" enums:"asc,desc"`                                                           // 정렬방식
	Size          string `query:"size" validate:"gte=1" example:"10" format:"int"`                                               // 페이지 사이즈
	SortTarget    string `query:"sort-target" example:"id" enums:"created_at,link_status"`                                       // 정렬 대상
}

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	// db connection check
	var err error
	if linkScheduleRepository == nil {
		linkScheduleRepository, err = mysql.NewLinkScheduleRepository(dbconfig)
		if err != nil {
			return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
		}
	}

	// requestParameter check
	requestParams, err := lambda_http.ParseQueryString[requestParameter](request)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	// search criteria
	criteria, err := createCriteria(*requestParams)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	// service call
	service := linkschedule.NewLinkScheduleService(linkScheduleRepository)
	res, paging, err := service.FindByCriteria(criteria)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	if res == nil {
		return lambda_http.SuccessResponseNoContent()
	}

	return lambda_http.SuccessResponseJson(http.StatusOK, lambda_http.SuccessPagedResponse{Data: res, Paging: paging, Message: http.StatusText(http.StatusOK)}, err)
}

func main() {
	lambda.Start(Handler)
}

func createCriteria(requestParams requestParameter) (*search.Criteria, error) {
	filters, err := createFilters(requestParams)
	if err != nil {
		return nil, err
	}

	pagingParams, err := createPagingParams(requestParams)
	if err != nil {
		return nil, err
	}

	sortOption, err := createSortOption(requestParams)
	if err != nil {
		return nil, err
	}

	criteria := search.NewCriteria(filters, sortOption, pagingParams)

	return &criteria, nil
}

func createFilters(requestParams requestParameter) ([]search.Filter, error) {
	filters := []search.Filter{}

	fmt.Printf("requestParams :: %v", requestParams)

	if requestParams.WorkspaceId != "" {
		filters = append(filters, search.NewFilter("workspaceId", search.Equal, requestParams.WorkspaceId))
	}

	if requestParams.CreatedAfter != "" {
		filters = append(filters, search.NewFilter("createdAt", search.GreaterOrEqual, requestParams.CreatedAfter))
	}

	if requestParams.CreatedBefore != "" {
		filters = append(filters, search.NewFilter("createdAt", search.LessOrEqual, requestParams.CreatedBefore))
	}

	if requestParams.OrderStatus != "" {
		orderStatus, err := linkschedule.ParseOrderStatus(requestParams.OrderStatus)
		if err != nil {
			return nil, err
		}

		filters = append(filters, search.NewFilter("orderStatus", search.Equal, orderStatus))
	}

	if requestParams.Oms != "" {
		oms, err := linkschedule.ParseOms(requestParams.Oms)
		if err != nil {
			return nil, err
		}

		filters = append(filters, search.NewFilter("oms", search.Equal, oms))
	}

	if requestParams.LinkStatus != "" {
		linkStatus, err := linkschedule.ParseLinkStatus(requestParams.LinkStatus)
		if err != nil {
			return nil, err
		}

		filters = append(filters, search.NewFilter("linkStatus", search.Equal, linkStatus))
	}

	return filters, nil
}

func createSortOption(requestParams requestParameter) (*search.SortOption, error) {
	var err error

	if requestParams.Sort == "" {
		requestParams.Sort = search.Descending.String()
	}

	if err != nil {
		return nil, err
	}

	sortOption := &search.SortOption{
		Field: requestParams.SortTarget,
		Order: search.SortOrder(requestParams.Sort),
	}

	return sortOption, nil
}

func createPagingParams(requestParams requestParameter) (*search.PagingParams, error) {
	var pagingParams *search.PagingParams
	if requestParams.Size != "" || requestParams.Cursor != "" || requestParams.Page != "" {
		pagingParams = &search.PagingParams{}

		if requestParams.Page != "" {
			page, err := strconv.Atoi(requestParams.Page)
			if err != nil {
				return nil, errors.New("invalid page")
			}
			pagingParams.Page = page
		}

		if requestParams.Size != "" {
			size, err := strconv.Atoi(requestParams.Size)
			if err != nil {
				return nil, errors.New("invalid size")
			}
			pagingParams.PageSize = size
		}

		if requestParams.Cursor != "" {
			pagingParams.Cursor = requestParams.Cursor
		}
	}

	return pagingParams, nil
}
