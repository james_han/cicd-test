package main

import (
	"net/http"
	"rp-order-information-api/db/mysql"
	excel_upload "rp-order-information-api/modules/excel_upload"
	"rp-order-information-api/web/lambda_http"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type requestParameter struct {
	WorkspaceId   string `validate:"required" example:"018dd4f9-8334-7ab5-8d5f-ace8a89c5f25"` // 워크스페이스 고유 아이디
	FileId        string `validate:"required" example:"018dd4f9-e421-7e30-8b47-599ff2020758"` // 파일 고유 아이디
	IsOverlap     bool   `validate:"required"`                                                // 덮어쓰기 여부
	IsPhoneCheck  bool   `validate:"required"`                                                // 번호체크 여부
	IsAllowHyphen bool   `validate:"required"`                                                // 송장번호 하이픈 허용 여부
}

var config = mysql.NewConfigFromTestDB("realpacking")
var repository *mysql.ExcelUploadRepository

// @Summary		엑셀 업로드 예약
// @Description	엑셀 업로드 예약
// @Tags			excel-uploads
// @Accept			json
// @Produce		json
// @Param			body	body		requestParameter	true	"body"
// @Success		201		{object}	SuccessSchema[DTO 생성 후 적용예정]
// @Failure		400		{object}	lambda_http.ErrorResponseType
// @Failure		500		{object}	lambda_http.ErrorResponseType
// @Router			/storages [post]
func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	if repository == nil {
		repository, err = mysql.NewExcelUploadRepository(*config)
		if err != nil {
			return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
		}
	}

	requestParams, err := lambda_http.ParseJsonRequestBody[requestParameter](request)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	service := excel_upload.NewExcelUploadService(repository)
	excelUploadDto, err := service.Save(
		requestParams.WorkspaceId,
		requestParams.FileId,
		requestParams.IsOverlap,
		requestParams.IsPhoneCheck,
		requestParams.IsAllowHyphen,
	)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	return lambda_http.SuccessResponseJson[lambda_http.SuccessResponse](http.StatusCreated, lambda_http.SuccessResponse{Data: *excelUploadDto, Message: http.StatusText(http.StatusCreated)}, nil)
}

func main() {
	lambda.Start(Handler)
}
