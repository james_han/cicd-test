package lambda_http

import "rp-order-information-api/modules/shared/search"

type SuccessResponse struct {
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

type SuccessPagedResponse struct {
	Data    interface{}        `json:"data"`
	Paging  *search.PagingMeta `json:"paging"`
	Message string             `json:"message"`
}
