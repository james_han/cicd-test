package main

import (
	"net/http"
	"os"
	"rp-order-information-api/web/lambda_http"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type SwaggerSpec struct {
	// Swagger JSON 구조체 정의
	// 필요한 필드들을 구조체에 추가
	// 예: Info, Paths, Components 등
}

var swaggerSpec string = ""

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	// if request.QueryStringParameters["key"] != "i#P7f*C[:E.c%,<{2iowfy=cjt7aEd" {
	// 	return lambda_http.ErrorResponse(401, errors.New("권한이 없는 접근입니다"))
	// }

	if swaggerSpec == "" {
		data, err := os.ReadFile("docs/swagger.json")
		if err != nil {
			lambda_http.ErrorResponse(http.StatusInternalServerError, err)
		}
		swaggerSpec = string(data)
	}

	// Swagger HTML 템플릿
	swaggerHTML := `
		<!DOCTYPE html>
		<html>
		<head>
			<meta charset="UTF-8">
			<title>Swagger UI</title>
			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.52.0/swagger-ui.css">
		</head>
		<body>
			<div id="swagger-ui"></div>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.52.0/swagger-ui-bundle.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.52.0/swagger-ui-standalone-preset.js"></script>
			<script>
				window.onload = function() {
					const ui = SwaggerUIBundle({
						spec: ` + swaggerSpec + `, // Swagger JSON 내용을 직접 전달
						dom_id: '#swagger-ui',
						deepLinking: true,
						presets: [
							SwaggerUIBundle.presets.apis,
							SwaggerUIStandalonePreset
						],
						layout: "StandaloneLayout"
					})
				}
			</script>
		</body>
		</html>
	`
	return events.APIGatewayProxyResponse{
		StatusCode:      http.StatusOK,
		IsBase64Encoded: false,
		Body:            swaggerHTML,
		Headers: map[string]string{
			"Content-Type":                 "text/html",
			"Access-Control-Allow-Origin":  "*",            // Allow all origins to access the resource (change this to a specific origin if needed)
			"Access-Control-Allow-Methods": "GET, OPTIONS", // Allow only GET and OPTIONS methods (change this to the allowed methods if needed)
		},
	}, nil
}

func main() {
	lambda.Start(Handler)
}
