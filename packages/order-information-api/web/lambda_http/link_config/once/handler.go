package main

import (
	"errors"
	"net/http"
	"rp-order-information-api/db/mysql"
	link_config "rp-order-information-api/modules/link_config"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/web/lambda_http"

	"github.com/aws/aws-lambda-go/events"
)

var dbconfig = mysql.NewConfigFromTestDB("realpacking")
var linkConfigRepository *mysql.LinkConfigRepository

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	linkConfigId, err := vo.NewLinkConfigIdFromString(request.PathParameters["link_config-id"])
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	service := link_config.NewLinkConfigService(linkConfigRepository)
	linkConfig, err := service.FindById(*linkConfigId)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	if linkConfig == nil {
		return lambda_http.ErrorResponse(http.StatusNotFound, errors.New("no data exists for the given id"))
	}

	return lambda_http.SuccessResponseJson(http.StatusOK, lambda_http.SuccessResponse{Data: linkConfig, Message: http.StatusText(http.StatusOK)}, err)
}
