package main

import (
	"net/http"
	"rp-order-information-api/db/mysql"
	link_config "rp-order-information-api/modules/link_config"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/web/lambda_http"

	"github.com/aws/aws-lambda-go/events"
)

var dbconfig = mysql.NewConfigFromTestDB("realpacking")
var linkConfigRepository *mysql.LinkConfigRepository

type requestParamter struct {
	WorkspaceId         string `json:"workspaceId"`
	Cafe24Id            string `json:"cafe24Id"`
	Cafe24Domain        string `json:"cafe24Domain"`
	GodoId              string `json:"godoId"`
	GodoDomain          string `json:"godoDomain"`
	GodoBusinessName    string `json:"godoBusinessName"`
	MakeshopIsAdmin     bool   `json:"makeshopIsAdmin"`
	MakeshopAdminDomain string `json:"makeshopAdminDomain"`
	MakeshopMallDomain  string `json:"makeshopMallDomain"`
	MakeshopId          string `json:"makeshopId"`
	MakeshopPassword    string `json:"makeshopPassword"`
	EzadminPartnerKey   string `json:"ezadminPartnerKey"`
	EzadminDomainKey    string `json:"ezadminDomainKey"`
	SabangnetApiKey     string `json:"sabangnetApiKey"`
	SabangnetId         string `json:"sabangnetId"`
	ShoplingId          string `json:"shoplingId"`
	ShoplingCompanyId   string `json:"shoplingCompanyId"`
	ShoplingApiKey      string `json:"shoplingApiKey"`
}

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error
	if linkConfigRepository == nil {
		linkConfigRepository, err = mysql.NewLinkConfigRepository(dbconfig)
		if err != nil {
			return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
		}
	}
	requestParams, err := lambda_http.ParseJsonRequestBody[requestParamter](request)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	workspaceId, err := vo.NewWorkspaceIdFromString(requestParams.WorkspaceId)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	createParamter := link_config.CreateParamter{
		WorkspaceId:         *workspaceId,
		Cafe24Id:            requestParams.Cafe24Id,
		Cafe24Domain:        requestParams.Cafe24Domain,
		GodoId:              requestParams.GodoId,
		GodoDomain:          requestParams.GodoDomain,
		GodoBusinessName:    requestParams.GodoBusinessName,
		MakeshopIsAdmin:     requestParams.MakeshopIsAdmin,
		MakeshopAdminDomain: requestParams.MakeshopAdminDomain,
		MakeshopMallDomain:  requestParams.MakeshopMallDomain,
		MakeshopId:          requestParams.MakeshopId,
		MakeshopPassword:    requestParams.MakeshopPassword,
		EzadminPartnerKey:   requestParams.EzadminPartnerKey,
		EzadminDomainKey:    requestParams.EzadminDomainKey,
		SabangnetApiKey:     requestParams.SabangnetApiKey,
		SabangnetId:         requestParams.SabangnetId,
		ShoplingId:          requestParams.ShoplingId,
		ShoplingCompanyId:   requestParams.ShoplingCompanyId,
		ShoplingApiKey:      requestParams.ShoplingApiKey,
	}

	service := link_config.NewLinkConfigService(linkConfigRepository)
	linkConfig, err := service.CreateLinkConfig(createParamter)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	return lambda_http.SuccessResponseJson(http.StatusCreated, lambda_http.SuccessResponse{Data: linkConfig, Message: http.StatusText(http.StatusCreated)}, err)
}
