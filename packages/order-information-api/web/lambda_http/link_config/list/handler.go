package main

import (
	"errors"
	"net/http"
	"rp-order-information-api/db/mysql"
	link_config "rp-order-information-api/modules/link_config"
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/web/lambda_http"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

var dbconfig = mysql.NewConfigFromTestDB("realpacking")
var linkConfigRepository *mysql.LinkConfigRepository

type statusOkResponse struct {
	Data    []*link_config.LinkConfigDto `json:"data"`
	Message string                       `json:"message"`
	Paging  interface{}                  `json:"paging"`
}
type requestParameter struct {
	WorkspaceId string `query:"workspace_id" example:"123e4567-e89b-12d3-a456-426614174000" format:"uuid" validate:"required"`
	Page        string `query:"page" example:"1"`
	Cursor      string `query:"cursor" example:"123e4567-e89b-12d3-a456-426614174000" format:"uuid"`
	Sort        string `query:"sort" example:"asc" enums:"asc,desc"`
	Size        string `query:"size" validate:"gte=1" example:"10"`
	SortTarget  string `query:"sort-target" example:"id" enums:"id,created_at"`
}

// @Summary		연동설정 리스트 조회
// @Description	연동설정 리스트 조회
// @Tags			link-config
// @Accept			json
// @Produce		json
// @Param			query	query		requestParameter	true	"query"
// @Success		200		{object}	statusOkResponse
// @Success		204
// @Failure		400	{object}	lambda_http.ErrorResponseType
// @Failure		500	{object}	lambda_http.ErrorResponseType
// @Router			/videos [get]
func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	if linkConfigRepository == nil {
		linkConfigRepository, err = mysql.NewLinkConfigRepository(dbconfig)
		if err != nil {
			return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
		}
	}

	requestParams, err := lambda_http.ParseQueryString[requestParameter](request)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	criteria, err := createCriteria(*requestParams)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	service := link_config.NewLinkConfigService(linkConfigRepository)
	linkConfigs, paging, err := service.FindByCriteria(criteria)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	if linkConfigs == nil {
		return lambda_http.SuccessResponseNoContent()
	}

	return lambda_http.SuccessResponseJson(http.StatusOK, lambda_http.SuccessPagedResponse{Data: linkConfigs, Message: http.StatusText(http.StatusOK), Paging: paging}, err)
}

func createCriteria(requestParams requestParameter) (*search.Criteria, error) {
	filters, err := createFilters(requestParams)
	if err != nil {
		return nil, err
	}

	pagingParams, err := createPagingParams(requestParams)
	if err != nil {
		return nil, err
	}

	sortOption, err := createSortOption(requestParams)
	if err != nil {
		return nil, err
	}

	criteria := search.NewCriteria(filters, sortOption, pagingParams)

	return &criteria, nil
}

func createFilters(requestParams requestParameter) ([]search.Filter, error) {
	filters := []search.Filter{}

	if requestParams.WorkspaceId != "" {
		filters = append(filters, search.NewFilter("workspaceId", search.Equal, requestParams.WorkspaceId))
	}

	return filters, nil
}

func createSortOption(requestParams requestParameter) (*search.SortOption, error) {
	var err error

	if requestParams.Sort == "" {
		requestParams.Sort = search.Descending.String()
	}

	if err != nil {
		return nil, err
	}

	sortOption := &search.SortOption{
		Field: requestParams.SortTarget,
		Order: search.SortOrder(requestParams.Sort),
	}

	return sortOption, nil
}

func createPagingParams(requestParams requestParameter) (*search.PagingParams, error) {
	var pagingParams *search.PagingParams
	if requestParams.Size != "" || requestParams.Cursor != "" || requestParams.Page != "" {
		pagingParams = &search.PagingParams{}

		if requestParams.Page != "" {
			page, err := strconv.Atoi(requestParams.Page)
			if err != nil {
				return nil, errors.New("invalid page")
			}
			pagingParams.Page = page
		}

		if requestParams.Size != "" {
			size, err := strconv.Atoi(requestParams.Size)
			if err != nil {
				return nil, errors.New("invalid size")
			}
			pagingParams.PageSize = size
		}

		if requestParams.Cursor != "" {
			pagingParams.Cursor = requestParams.Cursor
		}
	}

	return pagingParams, nil
}

func main() {
	lambda.Start(Handler)
}
