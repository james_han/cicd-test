package main

import (
	"net/http"
	"rp-order-information-api/db/mysql"
	link_config "rp-order-information-api/modules/link_config"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/web/lambda_http"

	"github.com/aws/aws-lambda-go/events"
)

var dbconfig = mysql.NewConfigFromTestDB("realpacking")
var linkConfigRepository *mysql.LinkConfigRepository

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	linkConfigId, err := vo.NewLinkConfigIdFromString(request.PathParameters["link_config-id"])
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	service := link_config.NewLinkConfigService(linkConfigRepository)

	if err := service.DeleteById(*linkConfigId); err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	return lambda_http.SuccessResponseNoContent()
}
