package lambda_http

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/url"

	"github.com/aws/aws-lambda-go/events"
	"github.com/go-playground/validator"
	"github.com/gorilla/schema"
	"golang.org/x/crypto/bcrypt"
)

type ErrorResponseType struct {
	Message string `json:"message"`
}

func ParseQueryString[T any](request events.APIGatewayV2HTTPRequest) (*T, error) {
	requestParams := new(T)

	decoder := schema.NewDecoder()
	decoder.SetAliasTag("query")

	values := url.Values{}
	for key, value := range request.QueryStringParameters {
		values.Add(key, value)
	}

	if err := decoder.Decode(requestParams, values); err != nil {
		return nil, err
	}

	validate := validator.New()
	if err := validate.Struct(requestParams); err != nil {
		return nil, err
	}

	return requestParams, nil
}

func ParseJsonRequestBody[T any](request events.APIGatewayV2HTTPRequest) (*T, error) {
	requestParams := new(T)
	if !json.Valid([]byte(request.Body)) {
		return nil, errors.New(`body is not in JSON format`)
	}

	if err := json.Unmarshal([]byte(request.Body), requestParams); err != nil {
		return nil, err
	}

	validate := validator.New()
	if err := validate.Struct(requestParams); err != nil {
		return nil, err
	}

	return requestParams, nil
}

func SuccessResponseJson[T SuccessPagedResponse | SuccessResponse](code int, resp T, err error) (events.APIGatewayProxyResponse, error) {
	var buf bytes.Buffer

	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 500}, err
	}

	body, err := json.Marshal(resp)
	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 500}, err
	}

	json.HTMLEscape(&buf, body)

	return events.APIGatewayProxyResponse{
		StatusCode:      code,
		IsBase64Encoded: false,
		Body:            buf.String(),
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
	}, nil
}

func SuccessResponseNoContent() (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		StatusCode:      204,
		IsBase64Encoded: false,
		Body:            "",
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
	}, nil
}

func ErrorResponse(code int, err error) (events.APIGatewayProxyResponse, error) {
	resp := ErrorResponseType{
		Message: err.Error(),
	}

	var jsonString string
	if jsonByte, err := json.Marshal(resp); err != nil {
		jsonString = `{"message":"unknown error"}`
	} else {
		jsonString = string(jsonByte)
	}

	return events.APIGatewayProxyResponse{
		StatusCode:      code,
		IsBase64Encoded: false,
		Body:            jsonString,
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
	}, nil
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
