package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/google/uuid"
)

type VideoDB struct {
	Id               uuid.UUID `db:"id"`
	WorkspaceId      uuid.UUID `db:"workspace_id"`
	MemberId         uuid.UUID `db:"member_id"`
	Code             string    `db:"code"`
	ObjectStorageId  string    `db:"object_storage_id"`
	ObjectStorageKey string    `db:"object_storage_key"`
	Claim            string    `db:"claim"`
	DurationSeconds  int       `db:"duration_seconds"`
	SizeKb           int       `db:"size_kb"`
	IsSended         bool      `db:"is_sended"`
	RecordingProgram string    `db:"recording_program"`
	RecordingIp      string    `db:"recording_ip"`
	RecordStartedAt  time.Time `db:"record_started_at"`
	RecordFinishedAt time.Time `db:"record_finished_at"`
	CreatedAt        time.Time `db:"created_at"`
	UpdatedAt        time.Time `db:"updated_at"`
}

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		StatusCode:      http.StatusOK,
		IsBase64Encoded: false,
		Body:            string(fmt.Sprintf(`{"status":true, region:"%s"}`, os.Getenv("AWS_REGION"))),
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
	}, nil
}

func main() {
	lambda.Start(Handler)
}
