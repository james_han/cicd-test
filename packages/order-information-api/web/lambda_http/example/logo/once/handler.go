package main

import (
	"errors"
	"net/http"
	"rp-order-information-api/db/mysql"
	"rp-order-information-api/modules/logo"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/web/lambda_http"

	"github.com/aws/aws-lambda-go/events"
)

var dbconfig = mysql.NewConfigFromTestDB("realpacking")

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	repo, err := mysql.NewLogoRepository(dbconfig)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	logoId, err := vo.NewLogoIdFromString(request.PathParameters["logo-id"])
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	service := logo.NewLogoService(repo)
	logo, err := service.FindById(*logoId)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	if logo == nil {
		return lambda_http.ErrorResponse(http.StatusNotFound, errors.New(http.StatusText(http.StatusNotFound)))
	}

	return lambda_http.SuccessResponseJson(http.StatusOK, lambda_http.SuccessResponse{Data: logo, Message: http.StatusText(http.StatusOK)}, err)
}
