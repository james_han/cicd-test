package main

import (
	"net/http"
	"rp-order-information-api/db/mysql"
	"rp-order-information-api/modules/logo"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/web/lambda_http"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
)

type requestParameter struct {
	WorkspaceId string `json:"workspaceId" validate:"required"`
	IsActive    string `json:"isActive" validate:"required"`
	FileId      string `json:"fileId" validate:"required"`
}

var dbconfig = mysql.NewConfigFromTestDB("realpacking")

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	requestParams, err := lambda_http.ParseJsonRequestBody[requestParameter](request)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	id, err := vo.NewLogoIdFromString(request.PathParameters["logo-id"])
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	wId, err := vo.NewWorkspaceIdFromString(requestParams.WorkspaceId)

	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	fId, err := vo.NewFileIdFromString(requestParams.FileId)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	isActive, err := strconv.ParseBool(requestParams.IsActive)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	repo, err := mysql.NewLogoRepository(dbconfig)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	service := logo.NewLogoService(repo)
	logoDto, err := service.Update(*id, *wId, isActive, *fId)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	return lambda_http.SuccessResponseJson(http.StatusCreated, lambda_http.SuccessResponse{Data: logoDto, Message: http.StatusText(http.StatusOK)}, err)
}
