package main

import (
	"errors"
	"net/http"
	"rp-order-information-api/db/mysql"
	"rp-order-information-api/modules/logo"
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/web/lambda_http"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
)

type requestParameter struct {
	WorkspaceId   string `json:"workspaceId" query:"workspace-id"`
	IsActive      string `json:"isActive" query:"is-active"`
	CreatedAfter  string `json:"createdAfter" query:"created-after"`
	CreatedBefore string `json:"createdBefore" query:"created-before"`
	Page          string `json:"page" query:"page"`
	Cursor        string `json:"cursor" query:"cursor"`
	Sort          string `json:"sort" query:"sort"`
	Size          string `json:"size" query:"size" validate:"gte=1"`
	SortTarget    string `json:"sort-target" query:"sort-target"`
}

var dbconfig = mysql.NewConfigFromTestDB("realpacking")

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	requestParams, err := lambda_http.ParseQueryString[requestParameter](request)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	criteria, err := createCriteria(*requestParams)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	repo, err := mysql.NewLogoRepository(dbconfig)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}
	service := logo.NewLogoService(repo)
	logos, paging, err := service.FindByCriteria(criteria)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	if logos == nil {
		return lambda_http.SuccessResponseNoContent()
	}

	return lambda_http.SuccessResponseJson(http.StatusOK, lambda_http.SuccessPagedResponse{Data: logos, Paging: paging, Message: http.StatusText(http.StatusOK)}, err)
}

func createCriteria(requestParams requestParameter) (*search.Criteria, error) {
	filters, err := createFilters(requestParams)
	if err != nil {
		return nil, err
	}

	pagingParams, err := createPagingParams(requestParams)
	if err != nil {
		return nil, err
	}

	sortOption, err := createSortOption(requestParams)
	if err != nil {
		return nil, err
	}

	criteria := search.NewCriteria(filters, sortOption, pagingParams)

	return &criteria, nil
}

func createFilters(requestParams requestParameter) ([]search.Filter, error) {
	filters := []search.Filter{}
	if requestParams.WorkspaceId != "" {
		filters = append(filters, search.NewFilter("workspaceId", search.Equal, requestParams.WorkspaceId))
	}

	if requestParams.IsActive != "" {
		isActive, err := strconv.ParseBool(requestParams.IsActive)
		if err != nil {
			return nil, errors.New("invalid is-active")
		}

		filters = append(filters, search.NewFilter("isActive", search.Equal, isActive))
	}

	if requestParams.CreatedAfter != "" {
		filters = append(filters, search.NewFilter("createdAt", search.GreaterOrEqual, requestParams.CreatedAfter))
	}

	if requestParams.CreatedBefore != "" {
		filters = append(filters, search.NewFilter("createdAt", search.LessOrEqual, requestParams.CreatedBefore))
	}

	return filters, nil
}

func createSortOption(requestParams requestParameter) (*search.SortOption, error) {
	var err error

	if requestParams.Sort == "" {
		requestParams.Sort = search.Descending.String()
	}

	if err != nil {
		return nil, err
	}

	sortOption := &search.SortOption{
		Field: requestParams.SortTarget,
		Order: search.SortOrder(requestParams.Sort),
	}

	return sortOption, nil
}

func createPagingParams(requestParams requestParameter) (*search.PagingParams, error) {
	var pagingParams *search.PagingParams
	if requestParams.Size != "" || requestParams.Cursor != "" || requestParams.Page != "" {
		pagingParams = &search.PagingParams{}

		if requestParams.Page != "" {
			page, err := strconv.Atoi(requestParams.Page)
			if err != nil {
				return nil, errors.New("invalid page")
			}
			pagingParams.Page = page
		}

		if requestParams.Size != "" {
			size, err := strconv.Atoi(requestParams.Size)
			if err != nil {
				return nil, errors.New("invalid size")
			}
			pagingParams.PageSize = size
		}

		if requestParams.Cursor != "" {
			pagingParams.Cursor = requestParams.Cursor
		}
	}

	return pagingParams, nil
}
