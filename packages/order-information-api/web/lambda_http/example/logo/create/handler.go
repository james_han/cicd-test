package main

import (
	"net/http"
	"rp-order-information-api/db/mysql"
	"rp-order-information-api/modules/logo"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/web/lambda_http"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type requestParameter struct {
	WorkspaceId string `validate:"required"`
	FileId      string `validate:"required"`
	IsActive    *bool  `validate:"required"`
}

func Handler(request events.APIGatewayV2HTTPRequest) (events.APIGatewayProxyResponse, error) {
	var err error

	requestParams, err := lambda_http.ParseJsonRequestBody[requestParameter](request)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	config := mysql.NewConfigFromTestDB("realpacking")
	repo, err := mysql.NewLogoRepository(config)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}
	service := logo.NewLogoService(repo)

	wId, err := vo.NewWorkspaceIdFromString(requestParams.WorkspaceId)

	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	fId, err := vo.NewFileIdFromString(requestParams.FileId)
	if err != nil {
		return lambda_http.ErrorResponse(http.StatusBadRequest, err)
	}

	logoDto, err := service.Create(
		*wId,
		*fId,
		*requestParams.IsActive,
	)

	if err != nil {
		return lambda_http.ErrorResponse(http.StatusInternalServerError, err)
	}

	return lambda_http.SuccessResponseJson[lambda_http.SuccessResponse](http.StatusCreated, lambda_http.SuccessResponse{Data: *logoDto, Message: http.StatusText(http.StatusOK)}, nil)
}

func main() {
	lambda.Start(Handler)
}
