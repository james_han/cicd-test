package order_information

import "rp-order-information-api/modules/shared/vo"

type OrderInfomationRepository interface {
	Save(orderInformation OrderInformation) error
	DeleteById(id vo.OrderInformationId) error
	FindById(id vo.OrderInformationId) (*OrderInformation, error)
}
