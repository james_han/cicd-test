package order_information

import (
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type OrderInformationDto struct {
	Id                    vo.OrderInformationId     `json:"id" example:"018da094-a30b-7033-b27c-7a32e620958f" format:"uuid"`          // 주문정보 고유 id
	WorkspaceId           vo.WorkspaceId            `json:"workspaceId" example:"018da094-a30b-7033-b27c-7a32e620958f" format:"uuid"` // 워크스페이스 고유 id
	MemberId              vo.MemberId               `json:"memberId" example:"018da094-a30b-7033-b27c-7a32e620958f" format:"uuid"`    // 멤버 고유 id
	OrderId               string                    `json:"orderId" example:"1190895862"`                                             // 주문번호
	Code                  string                    `json:"code" example:"1512993161"`                                                // 주문코드 (송장번호)
	Courier               string                    `json:"courier" example:"CJ대한통운"`                                                 // 택배사
	Timezone              string                    `json:"timezone" example:"Asia/Seoul"`                                            // 주문시간대
	Items                 []OrderInformationItemDto `json:"items"`                                                                    // 주문상품 목록
	OrdererName           string                    `json:"ordererName" example:"홍길동"`                                                // 주문자명
	OrdererEmail          string                    `json:"ordererEmail" example:"rptest@gmail.com" format:"email"`                   // 주문자 이메일
	OrderCellPhoneCode    string                    `json:"orderCellPhoneCode" example:"82"`                                          // 주문자 휴대폰 국가번호
	OrdererCellPhone      string                    `json:"ordererCellPhone" example:"821012345678" format:"phone"`                   // 주문자 휴대폰
	OrderPhoneCode        string                    `json:"orderPhoneCode" example:"82"`                                              // 주문자 전화번호 국가번호
	OrdererPhone          string                    `json:"ordererPhone" example:"821012345678" format:"phone"`                       // 주문자 전화번호
	ReceiverName          string                    `json:"receiverName" example:"홍길동"`                                               // 수취인명
	ReceiverCellPhoneCode string                    `json:"receiverCellPhoneCode" example:"82"`                                       // 수취인 휴대폰 국가번호
	ReceiverCellPhone     string                    `json:"receiverCellPhone" example:"821012345678" format:"phone"`                  // 수취인 휴대폰
	ReceiverPhoneCode     string                    `json:"receiverPhoneCode" example:"82"`                                           // 수취인 전화번호 국가번호
	ReceiverPhone         string                    `json:"receiverPhone" example:"821012345678" format:"phone"`                      // 수취인 전화번호
	ReceiverPostCode      string                    `json:"receiverPostCode" example:"12345"`                                         // 수취인 우편번호
	ReceiverAddress       string                    `json:"receiverAddress" example:"서울시 강남구 역삼동 123-45"`                             // 수취인 주소
	ShopName              string                    `json:"shopName" example:"테스트스토어"`                                                // 매출처
	CustomerId            string                    `json:"customerId" example:"nayh74"`                                              // 고객 id
	OrderedAt             string                    `json:"orderedAt" example:"2024-02-21" format:"date"`                             // 주문일시
	CreatedAt             time.Time                 `json:"createdAt" example:"2024-02-21T09:00:00+09:00" format:"date-time"`         // 생성일시
	UpdatedAt             time.Time                 `json:"updatedAt" example:"2024-02-21T09:00:00+09:00" format:"date-time"`         // 수정일시
}

type OrderInformationItemDto struct {
	Id                vo.OrderInformationItemId `json:"id" example:"018da094-a30b-7033-b27c-7a32e620958f" format:"uuid"`  // 주문상품 고유 id
	Price             int                       `json:"price" example:"10000"`                                            // 가격
	Amount            int                       `json:"amount" example:"2"`                                               // 수량
	ProductId         string                    `json:"productId" example:"1129286"`                                      // 상품 고유 id
	ProductBarcode    string                    `json:"productBarcode" example:"productcode1"`                            // 상품 바코드
	ProductCustomCode string                    `json:"productCustomCode" example:"productcustom"`                        // 자체상품코드 / SKU 코드
	ProductName       string                    `json:"productName" example:"상품명"`                                        // 		상품명
	ProductLink       string                    `json:"productLink" example:"http://productlink.com" format:"uri"`        // 상품링크
	ProductImageUrl   string                    `json:"productImageUrl" example:"http://productimage.com" format:"uri"`   // 상품이미지 url
	IsInspected       bool                      `json:"isInspected" example:"true"`                                       // 검수여부
	InspectedAmount   int                       `json:"inspectedAmount" example:"2"`                                      // 검수수량
	CreatedAt         time.Time                 `json:"createdAt" example:"2024-02-21T09:00:00+09:00" format:"date-time"` // 생성일시
	UpdatedAt         time.Time                 `json:"updatedAt" example:"2024-02-21T09:00:00+09:00" format:"date-time"` // 수정일시
}

type CreateParameter struct {
	WorkspaceId           vo.WorkspaceId
	MemberId              vo.MemberId
	OrderId               string
	Code                  string
	Courier               string
	Timezone              string
	Items                 []CreateItemParameter
	OrdererName           string
	OrdererEmail          string
	OrderCellPhoneCode    string
	OrdererCellPhone      string
	OrderPhoneCode        string
	OrdererPhone          string
	ReceiverName          string
	ReceiverCellPhoneCode string
	ReceiverCellPhone     string
	ReceiverPhoneCode     string
	ReceiverPhone         string
	ReceiverPostCode      string
	ReceiverAddress       string
	ShopName              string
	CustomerId            string
	OrderedAt             string
}

type CreateItemParameter struct {
	Price             int
	Amount            int
	ProductId         string
	ProductBarcode    string
	ProductCustomCode string
	ProductName       string
	ProductLink       string
	ProductImageUrl   string
	IsInspected       bool
	InspectedAmount   int
}

func domainToDto(OrderInformation *OrderInformation) *OrderInformationDto {
	orderInformationItemDto := make([]OrderInformationItemDto, len(OrderInformation.Items))
	for i, item := range OrderInformation.Items {
		orderInformationItemDto[i] = OrderInformationItemDto{
			Id:                item.Id,
			Price:             item.Price,
			Amount:            item.Amount,
			ProductId:         item.ProductId,
			ProductBarcode:    item.ProductBarcode,
			ProductCustomCode: item.ProductCustomCode,
			ProductName:       item.ProductName,
			ProductLink:       item.ProductLink,
			ProductImageUrl:   item.ProductImageUrl,
			IsInspected:       item.IsInspected,
			InspectedAmount:   item.InspectedAmount,
			CreatedAt:         item.CreatedAt,
			UpdatedAt:         item.UpdatedAt,
		}
	}

	var ordererEmail string
	var ordererCellPhoneCode string
	var ordererCellPhone string
	var orderPhoneCode string
	var ordererPhone string
	var receiverCellPhoneCode string
	var receiverCellPhone string
	var receiverPhoneCode string
	var receiverPhone string

	if OrderInformation.OrdererEmail != nil {
		ordererEmail = OrderInformation.OrdererEmail.String()
	}

	if OrderInformation.OrdererCellPhone != nil {
		ordererCellPhoneCode = OrderInformation.OrdererCellPhone.Code()
		ordererCellPhone = OrderInformation.OrdererCellPhone.Number()
	}

	if OrderInformation.OrdererPhone != nil {
		orderPhoneCode = OrderInformation.OrdererPhone.Code()
		ordererPhone = OrderInformation.OrdererPhone.Number()
	}

	if OrderInformation.ReceiverCellPhone != nil {
		receiverCellPhoneCode = OrderInformation.ReceiverCellPhone.Code()
		receiverCellPhone = OrderInformation.ReceiverCellPhone.Number()
	}

	if OrderInformation.ReceiverPhone != nil {
		receiverPhoneCode = OrderInformation.ReceiverPhone.Code()
		receiverPhone = OrderInformation.ReceiverPhone.Number()
	}

	return &OrderInformationDto{
		Id:                    OrderInformation.Id,
		WorkspaceId:           OrderInformation.WorkspaceId,
		MemberId:              OrderInformation.MemberId,
		OrderId:               OrderInformation.OrderId,
		Code:                  OrderInformation.Code,
		Courier:               OrderInformation.Courier,
		Timezone:              OrderInformation.Timezone,
		Items:                 orderInformationItemDto,
		OrdererName:           OrderInformation.OrdererName,
		OrdererEmail:          ordererEmail,
		OrderCellPhoneCode:    ordererCellPhoneCode,
		OrdererCellPhone:      ordererCellPhone,
		OrderPhoneCode:        orderPhoneCode,
		OrdererPhone:          ordererPhone,
		ReceiverName:          OrderInformation.ReceiverName,
		ReceiverCellPhoneCode: receiverCellPhoneCode,
		ReceiverCellPhone:     receiverCellPhone,
		ReceiverPhoneCode:     receiverPhoneCode,
		ReceiverPhone:         receiverPhone,
		ReceiverPostCode:      OrderInformation.ReceiverPostCode,
		ReceiverAddress:       OrderInformation.ReceiverAddress,
		ShopName:              OrderInformation.ShopName,
		CustomerId:            OrderInformation.CustomerId,
		OrderedAt:             OrderInformation.OrderedAt,
		CreatedAt:             OrderInformation.CreatedAt,
		UpdatedAt:             OrderInformation.UpdatedAt,
	}
}

func createParamterToEntity(createParameter *CreateParameter) (*OrderInformation, error) {
	nowDt := time.Now()
	orderInformationItems := make([]OrderInformationItem, len(createParameter.Items))
	for i, item := range createParameter.Items {
		itemId, err := vo.NewOrderInformationItemId()
		if err != nil {
			return nil, err
		}
		orderInformationItems[i] = OrderInformationItem{
			Id:                *itemId,
			Price:             item.Price,
			Amount:            item.Amount,
			ProductId:         item.ProductId,
			ProductBarcode:    item.ProductBarcode,
			ProductCustomCode: item.ProductCustomCode,
			ProductName:       item.ProductName,
			ProductLink:       item.ProductLink,
			ProductImageUrl:   item.ProductImageUrl,
			IsInspected:       item.IsInspected,
			InspectedAmount:   item.InspectedAmount,
			CreatedAt:         nowDt,
			UpdatedAt:         nowDt,
		}
	}

	id, err := vo.NewOrderInformationId()
	if err != nil {
		return nil, err
	}
	var email *vo.Email
	var ordererCellPhone *vo.Phone
	var ordererPhone *vo.Phone
	var receiverCellPhone *vo.Phone
	var receiverPhone *vo.Phone

	if createParameter.OrdererEmail != "" {
		email, err = vo.NewEmail(createParameter.OrdererEmail)
		if err != nil {
			return nil, err
		}
	}

	if createParameter.OrdererCellPhone != "" {
		tmpPhone := vo.NewPhone(createParameter.OrderCellPhoneCode, createParameter.OrdererCellPhone)
		ordererCellPhone = &tmpPhone
	}

	if createParameter.OrdererPhone != "" {
		tmpPhone := vo.NewPhone(createParameter.OrderPhoneCode, createParameter.OrdererPhone)
		ordererPhone = &tmpPhone
	}

	if createParameter.ReceiverCellPhone != "" {
		tmpPhone := vo.NewPhone(createParameter.ReceiverCellPhoneCode, createParameter.ReceiverCellPhone)
		receiverCellPhone = &tmpPhone
	}

	if createParameter.ReceiverPhone != "" {
		tmpPhone := vo.NewPhone(createParameter.ReceiverPhoneCode, createParameter.ReceiverPhone)
		receiverPhone = &tmpPhone
	}

	return &OrderInformation{
		Id:                *id,
		WorkspaceId:       createParameter.WorkspaceId,
		MemberId:          createParameter.MemberId,
		OrderId:           createParameter.OrderId,
		Code:              createParameter.Code,
		Courier:           createParameter.Courier,
		Timezone:          createParameter.Timezone,
		Items:             orderInformationItems,
		OrdererName:       createParameter.OrdererName,
		OrdererEmail:      email,
		OrdererCellPhone:  ordererCellPhone,
		OrdererPhone:      ordererPhone,
		ReceiverName:      createParameter.ReceiverName,
		ReceiverCellPhone: receiverCellPhone,
		ReceiverPhone:     receiverPhone,
		ReceiverPostCode:  createParameter.ReceiverPostCode,
		ReceiverAddress:   createParameter.ReceiverAddress,
		ShopName:          createParameter.ShopName,
		CustomerId:        createParameter.CustomerId,
		OrderedAt:         createParameter.OrderedAt,
		CreatedAt:         nowDt,
		UpdatedAt:         nowDt,
	}, nil

}

type UpdateParameter struct {
	OrdererName           string
	OrdererEmail          string
	OrdererCellPhoneCode  string
	OrdererCellPhone      string
	OrdererPhoneCode      string
	OrdererPhone          string
	ReceiverName          string
	ReceiverCellPhoneCode string
	ReceiverCellPhone     string
	ReceiverPhoneCode     string
	ReceiverPhone         string
}
