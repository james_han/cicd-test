package order_information

import (
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type OrderInformation struct {
	Id                vo.OrderInformationId
	WorkspaceId       vo.WorkspaceId
	MemberId          vo.MemberId
	OrderId           string                 // 주문번호
	Code              string                 // 주문코드 (송장번호)
	Courier           string                 // 택배사
	Timezone          string                 // 주문시간대
	Items             []OrderInformationItem // 주문상품
	OrdererName       string                 // 주문자명
	OrdererEmail      *vo.Email              // 주문자 이메일
	OrdererCellPhone  *vo.Phone              // 주문자 휴대폰
	OrdererPhone      *vo.Phone              // 주문자 전화번호
	ReceiverName      string                 // 수취인명
	ReceiverCellPhone *vo.Phone              // 수취인 휴대폰
	ReceiverPhone     *vo.Phone              // 수취인 전화번호
	ReceiverPostCode  string                 // 수취인 우편번호
	ReceiverAddress   string                 // 수취인 주소
	ShopName          string                 // 매출처
	CustomerId        string                 // 고객 id
	OrderedAt         string                 // 주문일시
	CreatedAt         time.Time              // 생성일시
	UpdatedAt         time.Time              // 수정일시
}

type OrderInformationItem struct {
	Id                vo.OrderInformationItemId
	Price             int       // 가격
	Amount            int       // 수량
	ProductId         string    // 상품 고유 id
	ProductBarcode    string    // 상품 바코드
	ProductCustomCode string    // 자체상품코드 / SKU 코드
	ProductName       string    // 상품명
	ProductLink       string    // 상품링크
	ProductImageUrl   string    // 상품이미지 url
	IsInspected       bool      // 검수여부
	InspectedAmount   int       // 검수수량
	CreatedAt         time.Time // 생성일시
	UpdatedAt         time.Time // 수정일시
}

func (o *OrderInformation) Update(ordererName string, ordererEmail *vo.Email, ordererCellPhone *vo.Phone, ordererPhone *vo.Phone, receiverName string, receiverCellPhone *vo.Phone, receiverPhone *vo.Phone) {
	o.OrdererName = ordererName
	o.OrdererEmail = ordererEmail
	o.OrdererCellPhone = ordererCellPhone
	o.OrdererPhone = ordererPhone
	o.ReceiverName = receiverName
	o.ReceiverCellPhone = receiverCellPhone
	o.ReceiverPhone = receiverPhone
	o.UpdatedAt = time.Now()
}
