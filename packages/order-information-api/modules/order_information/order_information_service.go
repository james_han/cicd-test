package order_information

import "rp-order-information-api/modules/shared/vo"

type OrderInformationService struct {
	OrderInformationRepos OrderInfomationRepository
}

func NewOrderInformationService(orderInformationRepos OrderInfomationRepository) *OrderInformationService {
	return &OrderInformationService{OrderInformationRepos: orderInformationRepos}
}

func (s *OrderInformationService) CreateOrderInformation(createParams CreateParameter) (*OrderInformationDto, error) {
	var err error

	orderInformation, err := createParamterToEntity(&createParams)
	if err != nil {
		return nil, err
	}

	if err := s.OrderInformationRepos.Save(*orderInformation); err != nil {
		return nil, err
	}

	return domainToDto(orderInformation), nil
}

func (s *OrderInformationService) DeleteById(id vo.OrderInformationId) error {
	return s.OrderInformationRepos.DeleteById(id)
}

func (s *OrderInformationService) UpdateById(id vo.OrderInformationId, updateParams UpdateParameter) (*OrderInformationDto, error) {
	orderInformation, err := s.OrderInformationRepos.FindById(id)
	if err != nil {
		return nil, err
	}

	if orderInformation == nil {
		return nil, nil
	}

	var ordererEmail *vo.Email
	var ordererCellPhone *vo.Phone
	var ordererPhone *vo.Phone
	var receiverCellPhone *vo.Phone
	var receiverPhone *vo.Phone

	if updateParams.OrdererEmail != "" {
		ordererEmail, err = vo.NewEmail(updateParams.OrdererEmail)
		if err != nil {
			return nil, err
		}
	}

	if updateParams.OrdererCellPhone != "" {
		phone := vo.NewPhone(updateParams.OrdererCellPhoneCode, updateParams.OrdererCellPhone)
		ordererCellPhone = &phone
	}

	if updateParams.OrdererPhone != "" {
		phone := vo.NewPhone(updateParams.OrdererPhoneCode, updateParams.OrdererPhone)
		ordererPhone = &phone
	}

	if updateParams.ReceiverCellPhone != "" {
		phone := vo.NewPhone(updateParams.ReceiverCellPhoneCode, updateParams.ReceiverCellPhone)
		receiverCellPhone = &phone
	}

	if updateParams.ReceiverPhone != "" {
		phone := vo.NewPhone(updateParams.ReceiverPhoneCode, updateParams.ReceiverPhone)
		receiverPhone = &phone
	}

	orderInformation.Update(updateParams.OrdererName, ordererEmail, ordererCellPhone, ordererPhone, updateParams.ReceiverName, receiverCellPhone, receiverPhone)

	if err := s.OrderInformationRepos.Save(*orderInformation); err != nil {
		return nil, err
	}

	return domainToDto(orderInformation), nil
}
