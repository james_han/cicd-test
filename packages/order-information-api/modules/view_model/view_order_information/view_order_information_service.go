package view_order_information

import (
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
)

type ViewOrderInformationService struct {
	repository OrderInformationRepository
}

func NewViewOrderInformationService(repository OrderInformationRepository) *ViewOrderInformationService {
	return &ViewOrderInformationService{repository: repository}
}

func (s *ViewOrderInformationService) FindById(id vo.OrderInformationId) (*ViewOrderInformationDto, error) {
	orderInformation, err := s.repository.FindById(id)
	if err != nil {
		return nil, err
	}

	return viewToDto(*orderInformation), nil
}

func (s *ViewOrderInformationService) FindByCriteria(criteria *search.Criteria) ([]*ViewOrderInformationDto, *search.PagingMeta, error) {
	var err error

	orderInformations, paging, err := s.repository.FindByCriteria(criteria)
	if err != nil {
		return nil, nil, err
	}

	var dtos []*ViewOrderInformationDto
	for _, orderInformation := range orderInformations {
		dtos = append(dtos, viewToDto(*orderInformation))
	}

	return dtos, paging, nil
}
