package view_order_information

import (
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
)

type OrderInformationRepository interface {
	FindById(id vo.OrderInformationId) (*OrderInformation, error)
	FindByCriteria(criteria *search.Criteria) ([]*OrderInformation, *search.PagingMeta, error)
}
