package view_order_information

import (
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type ViewOrderInformationDto struct {
	Id                    vo.OrderInformationId         `json:"id" example:"018da094-a30b-7033-b27c-7a32e620958f" format:"uuid"`          // 주문정보 고유 id
	WorkspaceId           vo.WorkspaceId                `json:"workspaceId" example:"018da094-a30b-7033-b27c-7a32e620958f" format:"uuid"` // 워크스페이스 고유 id
	MemberId              vo.MemberId                   `json:"memberId" example:"018da094-a30b-7033-b27c-7a32e620958f" format:"uuid"`    // 멤버 고유 id
	OrderId               string                        `json:"orderId" example:"1190895862"`                                             // 주문번호
	Code                  string                        `json:"code" example:"1512993161"`                                                // 주문코드 (송장번호)
	Courier               string                        `json:"courier" example:"CJ대한통운"`                                                 // 택배사
	Timezone              string                        `json:"timezone" example:"Asia/Seoul"`                                            // 주문시간대
	Items                 []ViewOrderInformationItemDto `json:"items"`                                                                    // 주문상품 목록
	OrdererName           string                        `json:"ordererName" example:"홍길동"`                                                // 주문자명
	OrdererEmail          string                        `json:"ordererEmail" example:"rptest@gmail.com" format:"email"`                   // 주문자 이메일
	OrderCellPhoneCode    string                        `json:"orderCellPhoneCode" example:"82"`                                          // 주문자 휴대폰 국가번호
	OrdererCellPhone      string                        `json:"ordererCellPhone" example:"821012345678" format:"phone"`                   // 주문자 휴대폰
	OrderPhoneCode        string                        `json:"orderPhoneCode" example:"82"`                                              // 주문자 전화번호 국가번호
	OrdererPhone          string                        `json:"ordererPhone" example:"821012345678" format:"phone"`                       // 주문자 전화번호
	ReceiverName          string                        `json:"receiverName" example:"홍길동"`                                               // 수취인명
	ReceiverCellPhoneCode string                        `json:"receiverCellPhoneCode" example:"82"`                                       // 수취인 휴대폰 국가번호
	ReceiverCellPhone     string                        `json:"receiverCellPhone" example:"821012345678" format:"phone"`                  // 수취인 휴대폰
	ReceiverPhoneCode     string                        `json:"receiverPhoneCode" example:"82"`                                           // 수취인 전화번호 국가번호
	ReceiverPhone         string                        `json:"receiverPhone" example:"821012345678" format:"phone"`                      // 수취인 전화번호
	ReceiverPostCode      string                        `json:"receiverPostCode" example:"12345"`                                         // 수취인 우편번호
	ReceiverAddress       string                        `json:"receiverAddress" example:"서울시 강남구 역삼동 123-45"`                             // 수취인 주소
	ShopName              string                        `json:"shopName" example:"테스트스토어"`                                                // 매출처
	CustomerId            string                        `json:"customerId" example:"nayh74"`                                              // 고객 id
	OrderedAt             string                        `json:"orderedAt" example:"2024-02-21" format:"date"`                             // 주문일시
	CreatedAt             time.Time                     `json:"createdAt" example:"2024-02-21T09:00:00+09:00" format:"date-time"`         // 생성일시
	UpdatedAt             time.Time                     `json:"updatedAt" example:"2024-02-21T09:00:00+09:00" format:"date-time"`         // 수정일시
}

type ViewOrderInformationItemDto struct {
	Id                vo.OrderInformationItemId `json:"id" example:"018da094-a30b-7033-b27c-7a32e620958f" format:"uuid"`  // 주문상품 고유 id
	Price             int                       `json:"price" example:"10000"`                                            // 가격
	Amount            int                       `json:"amount" example:"2"`                                               // 수량
	ProductId         string                    `json:"productId" example:"1129286"`                                      // 상품 고유 id
	ProductBarcode    string                    `json:"productBarcode" example:"productcode1"`                            // 상품 바코드
	ProductCustomCode string                    `json:"productCustomCode" example:"productcustom"`                        // 자체상품코드 / SKU 코드
	ProductName       string                    `json:"productName" example:"상품명"`                                        // 		상품명
	ProductLink       string                    `json:"productLink" example:"http://productlink.com" format:"uri"`        // 상품링크
	ProductImageUrl   string                    `json:"productImageUrl" example:"http://productimage.com" format:"uri"`   // 상품이미지 url
	IsInspected       bool                      `json:"isInspected" example:"true"`                                       // 검수여부
	InspectedAmount   int                       `json:"inspectedAmount" example:"2"`                                      // 검수수량
	CreatedAt         time.Time                 `json:"createdAt" example:"2024-02-21T09:00:00+09:00" format:"date-time"` // 생성일시
	UpdatedAt         time.Time                 `json:"updatedAt" example:"2024-02-21T09:00:00+09:00" format:"date-time"` // 수정일시
}

func viewToDto(view OrderInformation) *ViewOrderInformationDto {
	return &ViewOrderInformationDto{
		Id:                    view.Id,
		WorkspaceId:           view.WorkspaceId,
		MemberId:              view.MemberId,
		OrderId:               view.OrderId,
		Code:                  view.Code,
		Courier:               view.Courier,
		Timezone:              view.Timezone,
		Items:                 viewItemsToDto(view.Items),
		OrdererName:           view.OrdererName,
		OrdererEmail:          view.OrdererEmail.String(),
		OrderCellPhoneCode:    view.OrdererCellPhone.Code(),
		OrdererCellPhone:      view.OrdererCellPhone.Number(),
		OrderPhoneCode:        view.OrdererPhone.Code(),
		OrdererPhone:          view.OrdererPhone.Number(),
		ReceiverName:          view.ReceiverName,
		ReceiverCellPhoneCode: view.ReceiverCellPhone.Code(),
		ReceiverCellPhone:     view.ReceiverCellPhone.Number(),
		ReceiverPhoneCode:     view.ReceiverPhone.Code(),
		ReceiverPhone:         view.ReceiverPhone.Number(),
		ReceiverPostCode:      view.ReceiverPostCode,
		ReceiverAddress:       view.ReceiverAddress,
		ShopName:              view.ShopName,
		CustomerId:            view.CustomerId,
		OrderedAt:             view.OrderedAt,
		CreatedAt:             view.CreatedAt,
		UpdatedAt:             view.UpdatedAt,
	}
}

func viewItemsToDto(items []OrderInformationItem) []ViewOrderInformationItemDto {
	var itemDtos []ViewOrderInformationItemDto
	for _, item := range items {
		itemDtos = append(itemDtos, ViewOrderInformationItemDto{
			Id:                item.Id,
			Price:             item.Price,
			Amount:            item.Amount,
			ProductId:         item.ProductId,
			ProductBarcode:    item.ProductBarcode,
			ProductCustomCode: item.ProductCustomCode,
			ProductName:       item.ProductName,
			ProductLink:       item.ProductLink,
			ProductImageUrl:   item.ProductImageUrl,
			IsInspected:       item.IsInspected,
			InspectedAmount:   item.InspectedAmount,
			CreatedAt:         item.CreatedAt,
			UpdatedAt:         item.UpdatedAt,
		})
	}
	return itemDtos
}
