package vo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

type VideoId uuid.UUID

var _ IdInterface = (*VideoId)(nil)

func NewVideoId() (*VideoId, error) {
	id, err := uuid.NewV7()
	if err != nil {
		return nil, err
	}

	cid := VideoId(id)
	return &cid, nil
}

func NewVideoIdString(id string) (*VideoId, error) {
	cid, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}

	return (*VideoId)(&cid), nil
}

func (v VideoId) String() string {
	return uuid.UUID(v).String()
}

func (v VideoId) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.String())
}

// Scan implements IdInterface.
func (v *VideoId) Scan(src interface{}) error {
	var source []byte

	switch src := src.(type) {
	case string:
		source = []byte(src)
	case []byte:
		source = src
	default:
		return errors.New("incompatible type for video id")
	}

	id, err := uuid.ParseBytes(source)
	if err != nil {
		return err
	}

	*v = VideoId(id)
	return nil
}

func (v VideoId) Value() (driver.Value, error) {
	return uuid.UUID(v).String(), nil
}
