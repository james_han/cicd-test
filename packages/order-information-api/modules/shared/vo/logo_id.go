package vo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

type LogoId uuid.UUID

var _ IdInterface = (*LogoId)(nil)

func NewLogoId() (*LogoId, error) {
	id, err := uuid.NewV7()
	if err != nil {
		return nil, err
	}

	lId := LogoId(id)
	return &lId, nil
}

func NewLogoIdFromString(id string) (*LogoId, error) {
	lId, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}

	return (*LogoId)(&lId), nil
}

func (id LogoId) String() string {
	return uuid.UUID(id).String()
}

// Scan implements IdInterface.
func (id *LogoId) Scan(src interface{}) error {
	var source []byte

	switch src := src.(type) {
	case string:
		source = []byte(src)
	case []byte:
		source = src
	default:
		return errors.New("incompatible type for logo id")
	}

	uid, err := uuid.ParseBytes(source)
	if err != nil {
		return err
	}

	*id = LogoId(uid)
	return nil
}

// Value implements IdInterface.
func (id LogoId) Value() (driver.Value, error) {
	return uuid.UUID(id).String(), nil
}

// DTO -> client response (byte -> string)
func (id LogoId) MarshalJSON() ([]byte, error) {
	return json.Marshal(id.String())
}
