package vo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

type ObjectStorageId uuid.UUID

var _ IdInterface = (*ObjectStorageId)(nil)

func NewObjectStorageId() (*ObjectStorageId, error) {
	id, err := uuid.NewV7()
	if err != nil {
		return nil, err
	}

	objectStorageId := ObjectStorageId(id)
	return &objectStorageId, nil
}

func NewObjectStorageIdFromString(id string) (*ObjectStorageId, error) {
	objectStorageId, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}

	return (*ObjectStorageId)(&objectStorageId), nil
}

func (s ObjectStorageId) String() string {
	return uuid.UUID(s).String()
}

// Scan implements IdInterface.
func (o *ObjectStorageId) Scan(src interface{}) error {
	var source []byte

	switch src := src.(type) {
	case string:
		source = []byte(src)
	case []byte:
		source = src
	default:
		return errors.New("incompatible type for object storage id")
	}

	id, err := uuid.ParseBytes(source)
	if err != nil {
		return err
	}

	*o = ObjectStorageId(id)
	return nil
}

// Value implements IdInterface.
func (o ObjectStorageId) Value() (driver.Value, error) {
	return uuid.UUID(o).String(), nil
}

// DTO -> client response (byte -> string)
func (o ObjectStorageId) MarshalJSON() ([]byte, error) {
	return json.Marshal(o.String())
}
