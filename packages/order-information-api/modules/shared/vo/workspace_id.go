package vo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

type WorkspaceId uuid.UUID

var _ IdInterface = (*WorkspaceId)(nil)

func NewWorkspaceId() (*WorkspaceId, error) {
	id, err := uuid.NewV7()
	if err != nil {
		return nil, err
	}

	cid := WorkspaceId(id)
	return &cid, nil
}

func NewWorkspaceIdFromString(id string) (*WorkspaceId, error) {
	cid, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}

	return (*WorkspaceId)(&cid), nil
}

func (s WorkspaceId) String() string {
	return uuid.UUID(s).String()
}

func (w WorkspaceId) MarshalJSON() ([]byte, error) {
	return json.Marshal(w.String())
}

// Scan implements IdInterface.
func (w *WorkspaceId) Scan(src interface{}) error {
	var source []byte

	switch src := src.(type) {
	case string:
		source = []byte(src)
	case []byte:
		source = src
	default:
		return errors.New("incompatible type for workspace id")
	}

	id, err := uuid.ParseBytes(source)
	if err != nil {
		return err
	}

	*w = WorkspaceId(id)
	return nil
}

// Value implements IdInterface.
func (w WorkspaceId) Value() (driver.Value, error) {
	return uuid.UUID(w).String(), nil
}
