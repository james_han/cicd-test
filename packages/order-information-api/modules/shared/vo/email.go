package vo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"regexp"
)

type Email string

func NewEmail(email string) (*Email, error) {
	if !regexp.MustCompile(`^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$`).MatchString(email) {
		return nil, errors.New("invalid email format")
	}
	ret := Email(email)
	return &ret, nil
}

func (e *Email) String() string {
	return string(*e)
}

func (e Email) MarshalJSON() ([]byte, error) {
	return json.Marshal(e.String())
}

// Scan implements IdInterface.
func (e *Email) Scan(src interface{}) error {
	var source []byte

	switch src := src.(type) {
	case string:
		source = []byte(src)
	case []byte:
		source = src
	default:
		return errors.New("incompatible type for email")
	}

	*e = Email(string(source))

	return nil
}

// Value implements IdInterface.
func (e Email) Value() (driver.Value, error) {
	return e.String(), nil
}
