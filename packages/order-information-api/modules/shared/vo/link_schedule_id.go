package vo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

type LinkScheduleId uuid.UUID

var _ IdInterface = (*LinkScheduleId)(nil)

func NewLinkScheduleId() (*LinkScheduleId, error) {
	id, err := uuid.NewV7()
	if err != nil {
		return nil, err
	}

	cid := LinkScheduleId(id)
	return &cid, nil
}

func NewLinkScheduleIdFromString(id string) (*LinkScheduleId, error) {
	cid, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}

	return (*LinkScheduleId)(&cid), nil
}

func (s LinkScheduleId) String() string {
	return uuid.UUID(s).String()
}

func (m LinkScheduleId) MarshalJSON() ([]byte, error) {
	return json.Marshal(m.String())
}

// Scan implements IdInterface.
func (m *LinkScheduleId) Scan(src interface{}) error {
	var source []byte

	switch src := src.(type) {
	case string:
		source = []byte(src)
	case []byte:
		source = src
	default:
		return errors.New("incompatible type for link schedule id")
	}

	id, err := uuid.ParseBytes(source)
	if err != nil {
		return err
	}

	*m = LinkScheduleId(id)
	return nil
}

// Value implements IdInterface.
func (m LinkScheduleId) Value() (driver.Value, error) {
	return uuid.UUID(m).String(), nil
}
