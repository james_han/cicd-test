package vo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

type OrderInformationId uuid.UUID

var _ IdInterface = (*OrderInformationId)(nil)

func NewOrderInformationId() (*OrderInformationId, error) {
	id, err := uuid.NewV7()
	if err != nil {
		return nil, err
	}

	lId := OrderInformationId(id)
	return &lId, nil
}

func NewOrderInformationIdFromString(id string) (*OrderInformationId, error) {
	lId, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}

	return (*OrderInformationId)(&lId), nil
}

func (id OrderInformationId) String() string {
	return uuid.UUID(id).String()
}

// Scan implements IdInterface.
func (id *OrderInformationId) Scan(src interface{}) error {
	var source []byte

	switch src := src.(type) {
	case string:
		source = []byte(src)
	case []byte:
		source = src
	default:
		return errors.New("incompatible type for order information id")
	}

	uid, err := uuid.ParseBytes(source)
	if err != nil {
		return err
	}

	*id = OrderInformationId(uid)
	return nil
}

// Value implements IdInterface.
func (id OrderInformationId) Value() (driver.Value, error) {
	return uuid.UUID(id).String(), nil
}

func (id OrderInformationId) MarshalJSON() ([]byte, error) {
	return json.Marshal(id.String())
}
