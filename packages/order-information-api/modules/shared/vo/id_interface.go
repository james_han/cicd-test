package vo

import "database/sql/driver"

type IdInterface interface {
	String() string
	Value() (driver.Value, error) // implements sql Built in interfcaes ref.http://jmoiron.net/blog/built-in-interfaces
	Scan(src interface{}) error   // implements sql Built in interfcaes ref.http://jmoiron.net/blog/built-in-interfaces
	MarshalJSON() ([]byte, error) // implementes json Marshaler interface ref.https://pkg.go.dev/encoding/json#Marshaler
}
