package vo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

type MemberId uuid.UUID

var _ IdInterface = (*MemberId)(nil)

func NewMemberId() (*MemberId, error) {
	id, err := uuid.NewV7()
	if err != nil {
		return nil, err
	}

	cid := MemberId(id)
	return &cid, nil
}

func NewMemberIdFromString(id string) (*MemberId, error) {
	cid, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}

	return (*MemberId)(&cid), nil
}

func (s MemberId) String() string {
	return uuid.UUID(s).String()
}

func (m MemberId) MarshalJSON() ([]byte, error) {
	return json.Marshal(m.String())
}

// Scan implements IdInterface.
func (m *MemberId) Scan(src interface{}) error {
	var source []byte

	switch src := src.(type) {
	case string:
		source = []byte(src)
	case []byte:
		source = src
	default:
		return errors.New("incompatible type for member id")
	}

	id, err := uuid.ParseBytes(source)
	if err != nil {
		return err
	}

	*m = MemberId(id)
	return nil
}

// Validate implements IdInterface.
func (m *MemberId) Validate() error {
	if uuid.UUID(*m).Version() != 7 {
		return errors.New("member id is not v7")
	}

	return nil
}

// Value implements IdInterface.
func (m MemberId) Value() (driver.Value, error) {
	return uuid.UUID(m).String(), nil
}
