package vo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

type ExcelUploadId uuid.UUID

var _ IdInterface = (*ExcelUploadId)(nil)

func NewExcelUploadId() (*ExcelUploadId, error) {
	id, err := uuid.NewV7()
	if err != nil {
		return nil, err
	}
	uid := ExcelUploadId(id)
	return &uid, nil
}

func NewExcelUploadIdFromString(id string) (*ExcelUploadId, error) {
	uid, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}
	return (*ExcelUploadId)(&uid), nil
}

func (e ExcelUploadId) String() string {
	return uuid.UUID(e).String()
}

// Scan implements IdInterface.
func (e *ExcelUploadId) Scan(src interface{}) error {
	var source []byte

	switch src := src.(type) {
	case string:
		source = []byte(src)
	case []byte:
		source = src
	default:
		return errors.New("incompatible type for excel upload id")
	}

	id, err := uuid.ParseBytes(source)
	if err != nil {
		return err
	}

	*e = ExcelUploadId(id)
	return nil
}

// Value implements IdInterface.
func (e ExcelUploadId) Value() (driver.Value, error) {
	return uuid.UUID(e).String(), nil
}

// DTO -> client response (byte -> string)
func (e ExcelUploadId) MarshalJSON() ([]byte, error) {
	return json.Marshal(e.String())
}
