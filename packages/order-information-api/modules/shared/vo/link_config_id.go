package vo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

type LinkConfigId uuid.UUID

var _ IdInterface = (*LinkConfigId)(nil)

func NewLinkConfigId() (*LinkConfigId, error) {
	id, err := uuid.NewV7()
	if err != nil {
		return nil, err
	}

	lId := LinkConfigId(id)
	return &lId, nil
}

func NewLinkConfigIdFromString(id string) (*LinkConfigId, error) {
	lId, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}

	return (*LinkConfigId)(&lId), nil
}

func (id LinkConfigId) String() string {
	return uuid.UUID(id).String()
}

// Scan implements IdInterface.
func (id *LinkConfigId) Scan(src interface{}) error {
	var source []byte

	switch src := src.(type) {
	case string:
		source = []byte(src)
	case []byte:
		source = src
	default:
		return errors.New("incompatible type for link config id")
	}

	uid, err := uuid.ParseBytes(source)
	if err != nil {
		return err
	}

	*id = LinkConfigId(uid)
	return nil
}

// Value implements IdInterface.
func (id LinkConfigId) Value() (driver.Value, error) {
	return uuid.UUID(id).String(), nil
}

func (id LinkConfigId) MarshalJSON() ([]byte, error) {
	return json.Marshal(id.String())
}
