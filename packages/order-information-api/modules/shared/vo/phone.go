package vo

type Phone struct {
	code   string
	number string
}

func NewPhone(code, number string) Phone {
	return Phone{
		code:   code,
		number: number,
	}
}

func (p Phone) Code() string {
	return p.code
}

func (p Phone) Number() string {
	return p.number
}

func (p Phone) String() string {
	return `+` + p.code + p.number[1:]
}
