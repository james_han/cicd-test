package vo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

type FileId uuid.UUID

var _ IdInterface = (*FileId)(nil)

func NewFileId() (*FileId, error) {
	id, err := uuid.NewV7()
	if err != nil {
		return nil, err
	}

	lId := FileId(id)
	return &lId, nil
}

func NewFileIdFromString(id string) (*FileId, error) {
	lId, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}

	return (*FileId)(&lId), nil
}

func (id FileId) String() string {
	return uuid.UUID(id).String()
}

// Scan implements IdInterface.
func (id *FileId) Scan(src interface{}) error {
	var source []byte

	switch src := src.(type) {
	case string:
		source = []byte(src)
	case []byte:
		source = src
	default:
		return errors.New("incompatible type for file id")
	}

	uid, err := uuid.ParseBytes(source)
	if err != nil {
		return err
	}

	*id = FileId(uid)
	return nil
}

// Value implements IdInterface.
func (id FileId) Value() (driver.Value, error) {
	return uuid.UUID(id).String(), nil
}

// DTO -> client response (byte -> string)
func (id FileId) MarshalJSON() ([]byte, error) {
	return json.Marshal(id.String())
}
