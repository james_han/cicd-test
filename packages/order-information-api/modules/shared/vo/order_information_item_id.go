package vo

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

type OrderInformationItemId uuid.UUID

var _ IdInterface = (*OrderInformationItemId)(nil)

func NewOrderInformationItemId() (*OrderInformationItemId, error) {
	id, err := uuid.NewV7()
	if err != nil {
		return nil, err
	}

	lId := OrderInformationItemId(id)
	return &lId, nil
}

func NewOrderInformationItemIdFromString(id string) (*OrderInformationItemId, error) {
	lId, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}

	return (*OrderInformationItemId)(&lId), nil
}

func (id OrderInformationItemId) String() string {
	return uuid.UUID(id).String()
}

// Scan implements IdInterface.
func (id *OrderInformationItemId) Scan(src interface{}) error {
	var source []byte

	switch src := src.(type) {
	case string:
		source = []byte(src)
	case []byte:
		source = src
	default:
		return errors.New("incompatible type for order information id")
	}

	uid, err := uuid.ParseBytes(source)
	if err != nil {
		return err
	}

	*id = OrderInformationItemId(uid)
	return nil
}

// Value implements IdInterface.
func (id OrderInformationItemId) Value() (driver.Value, error) {
	return uuid.UUID(id).String(), nil
}

func (id OrderInformationItemId) MarshalJSON() ([]byte, error) {
	return json.Marshal(id.String())
}
