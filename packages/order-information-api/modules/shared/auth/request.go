package auth

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

const ACTION_READ = "read"
const ACTION_WRITE = "write"

type Action string

const AUTH_SERVICE_URL = "https://auth-api.realpacking.com/v1/auth"

func AuthRequest(token, resource, resourceID string, action Action) error {

	// 타임아웃이 설정된 HTTP 클라이언트 생성
	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	// HTTP 요청 생성
	req, err := http.NewRequest("GET", AUTH_SERVICE_URL, nil)
	if err != nil {
		fmt.Println(err)
		return err
	}

	// 헤더에 사용자 정의 값 추가
	req.Header.Add("Authorization", token)
	req.Header.Add("Resource", resource)
	req.Header.Add("Resource-Id", resourceID)
	req.Header.Add("Action", string(action))

	// 요청 실행
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer resp.Body.Close() // 응답 본문 닫기는 항상 보장되어야 함

	// 응답 본문 읽기
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println(string(body))
	if resp.StatusCode == 401 {
		return fmt.Errorf("Unauthorized")
	}
	return nil
}
