package auth

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strings"
)

// organizationId
// workspaceId
// groupId
// memberId
// accountId
// sessionId

type AuthMember struct {
	OrganizationId string `json:"organizationId"`
	WorkspaceId    string `json:"workspaceId"`
	GroupId        string `json:"groupId"`
	MemberId       string `json:"memberId"`
	AccountId      string `json:"accountId"`
	SessionId      string `json:"sessionId"`
}

func (m *AuthMember) ToMap() map[string]interface{} {
	return map[string]interface{}{
		"organizationId": m.OrganizationId,
		"workspaceId":    m.WorkspaceId,
		"groupId":        m.GroupId,
		"memberId":       m.MemberId,
		"accountId":      m.AccountId,
		"sessionId":      m.SessionId,
	}
}

func FromMap(data map[string]interface{}) *AuthMember {
	return &AuthMember{
		OrganizationId: data["organizationId"].(string),
		WorkspaceId:    data["workspaceId"].(string),
		GroupId:        data["groupId"].(string),
		MemberId:       data["memberId"].(string),
		AccountId:      data["accountId"].(string),
		SessionId:      data["sessionId"].(string),
	}
}

func NewAuthMemberFromJwtToken(token string) (*AuthMember, error) {
	// 토큰을 '.'을 기준으로 분할
	parts := strings.Split(token, ".")
	if len(parts) != 3 {
		return nil, fmt.Errorf("Invalid JWT token format")
	}

	// 페이로드 부분 디코딩
	payload, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		fmt.Printf("Error decoding payload: %s\n", err)
		return nil, err
	}

	// JSON 파싱
	var data map[string]interface{}
	if err := json.Unmarshal(payload, &data); err != nil {
		fmt.Printf("Error parsing payload: %s\n", err)
		return nil, err
	}

	// AuthMember 구조체로 변환
	member := FromMap(data)
	return member, nil
}
