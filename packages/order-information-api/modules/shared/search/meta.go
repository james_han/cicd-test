package search

// PagingMeta 구조체는 페이징에 필요한 메타데이터를 정의합니다.
type PagingMeta struct {
	NextCursor  string `json:"nextCursor,omitempty"`  // 다음 페이지를 위한 커서 (선택적, CURSOR PAGING)
	TotalCount  int    `json:"totalCount,omitempty"`  // 전체 아이템 수 (선택적)
	TotalPages  int    `json:"totalPages,omitempty"`  // 전체 페이지 수 (선택적, OFFSET PAGING)
	CurrentPage int    `json:"currentPage,omitempty"` // 현재 페이지 (선택적, OFFSET PAGING)
	Size        int    `json:"size,omitempty"`        // 페이지 사이즈 (선택적, OFFSET PAGING)
}

func (p *PagingMeta) SetTotalPages() {
	p.TotalPages = p.TotalCount / p.Size
	if p.TotalCount%p.Size != 0 {
		p.TotalPages += 1
	}
}
