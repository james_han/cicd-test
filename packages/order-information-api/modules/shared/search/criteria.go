package search

type Filter struct {
	Field    string
	Operator OperatorType
	Value    interface{}
}

type Criteria struct {
	Filters []Filter
	Sort    *SortOption
	Page    *PagingParams
}

type SortOption struct {
	Field string
	Order SortOrder
}

type SortOrder string

func (s SortOrder) String() string {
	return string(s)
}

const (
	Ascending  SortOrder = "asc"
	Descending SortOrder = "desc"
)

type OperatorType string

const (
	Equal          OperatorType = "="
	NotEqual       OperatorType = "!="
	GreaterThan    OperatorType = ">"
	LessThan       OperatorType = "<"
	GreaterOrEqual OperatorType = ">="
	LessOrEqual    OperatorType = "<="
	Like           OperatorType = "LIKE"
	NotLike        OperatorType = "NOT LIKE"
	In             OperatorType = "IN"
)

// NewCriteria는 새로운 Criteria 인스턴스를 생성합니다.
func NewCriteria(filters []Filter, sort *SortOption, page *PagingParams) Criteria {
	return Criteria{
		Filters: filters,
		Sort:    sort,
		Page:    page,
	}
}

func NewFilter(field string, operator OperatorType, value interface{}) Filter {
	return Filter{
		Field:    field,
		Operator: operator,
		Value:    value,
	}
}
