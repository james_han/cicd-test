package search

// PagingParams 구조체는 페이징을 위한 파라미터를 정의합니다.
// 클라이언트는 페이지 번호나 커서 중 하나를 선택적으로 사용할 수 있습니다.
type PagingParams struct {
	Page     int    // 페이지 기반 페이징을 위한 페이지 번호 (0이면 무시됨)
	Cursor   string // 커서 기반 페이징을 위한 커서 (""이면 무시됨)
	PageSize int    // 페이지당 아이템 수
}
