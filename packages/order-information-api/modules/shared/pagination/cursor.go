package pagination

type Cursor struct {
	Size      int    `json:"size"`
	Pointer   string `json:"pointer"`
	Direction string `json:"direction"`
}

func NewCursor(size int, pointer string, direction string) (*Cursor, error) {
	cursor := new(Cursor)
	cursor.Direction = direction
	cursor.Size = size
	cursor.Pointer = pointer

	return cursor, nil
}
