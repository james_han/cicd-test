package pagination

type Offset struct {
	Size       int `json:"pageSize"`
	Page       int `json:"currentPage"`
	Total      int `json:"totalPages"`
	TotalItems int `json:"totalItems"`
}

func NewOffset(page, size, totalItems int) *Offset {
	pagination := newOffsetRequest(page, size, totalItems)
	pagination.setTotal(totalItems)

	return &pagination
}

func newOffsetRequest(page, size, totalItems int) Offset {
	// Check if page or size is less than 1 and set them to default values
	if page < 1 {
		page = 1
	}

	if size < 1 {
		size = 10
	}

	return Offset{
		Page:       page,
		Size:       size,
		TotalItems: totalItems,
	}
}

// 총 로우 개수를 입력하면 알아서 최대 페이지 수를 설정하는 메소드
func (p *Offset) setTotal(totalRows int) {
	p.Total = totalRows / p.Size
	if totalRows%p.Size != 0 {
		p.Total += 1
	}
}
