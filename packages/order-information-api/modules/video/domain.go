package video

import (
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type Video struct {
	Id               vo.VideoId         `db:"id"`
	WorkspaceId      vo.WorkspaceId     `db:"workspace_id"`
	MemberId         vo.MemberId        `db:"member_id"`
	Code             string             `db:"code"`
	ObjectStorageId  vo.ObjectStorageId `db:"object_storage_id"`
	ObjectStorageKey string             `db:"object_storage_key"`
	Claim            VideoClaim         `db:"claim" json:"claim"`
	DurationSeconds  int                `db:"duration_seconds"`
	SizeKb           int                `db:"size_kb"`
	IsSended         bool               `db:"is_sended"`
	RecordingProgram string             `db:"recording_program"`
	RecordingIp      string             `db:"recording_ip"`
	RecordStartedAt  time.Time          `db:"record_started_at"`
	RecordFinishedAt time.Time          `db:"record_finished_at"`
	CreatedAt        time.Time          `db:"created_at"`
	UpdatedAt        time.Time          `db:"updated_at"`
}

type VideoClaim struct {
	Status videoClaimStatus `db:"status" json:"status"`
	Logs   []VideoClaimLog  `db:"logs" json:"logs"`
}

type VideoClaimLog struct {
	ManagerName string    `db:"managerName" json:"managerName"`
	Contents    string    `db:"contents" json:"contents"`
	CreatedAt   time.Time `db:"createdAt" json:"createdAt"`
	UpdatedAt   time.Time `db:"updatedAt" json:"updatedAt"`
}

func (v *Video) ClaimSolved() {
	v.Claim.Status = claimStatusSolved
	v.UpdatedAt = time.Now()
}

func (v *Video) NoClaimExist() {
	v.Claim.Status = claimStatusNone
	v.UpdatedAt = time.Now()
}

func (v *Video) ClaimAnswering() {
	v.Claim.Status = claimStatusAnswering
	v.UpdatedAt = time.Now()
}

func (v *Video) UpdateClaim(status videoClaimStatus, logs []VideoClaimLog) {
	v.Claim.Status = status
	v.Claim.Logs = logs
	v.UpdatedAt = time.Now()
}

func (v *Video) AddClaim(managerName, contents string) {
	log := VideoClaimLog{
		ManagerName: managerName,
		Contents:    contents,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	if v.Claim.Logs == nil {
		v.Claim.Logs = make([]VideoClaimLog, 0)
	}
	v.Claim.Logs = append(v.Claim.Logs, log)
	v.UpdatedAt = time.Now()
}

func (v *Video) UpdateCode(code string) {
	v.Code = code
	v.UpdatedAt = time.Now()
}

func (v *Video) Sended() {
	v.IsSended = true
	v.UpdatedAt = time.Now()
}
