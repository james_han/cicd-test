package video

import "errors"

type videoClaimStatus string

const (
	claimStatusNone      = "none"
	claimStatusAnswering = "answering"
	claimStatusSolved    = "solved"
)

func newVideoClaimStatus(status string) (videoClaimStatus, error) {
	if status != claimStatusNone && status != claimStatusAnswering && status != claimStatusSolved {
		return "", errors.New("invalid claim status")
	}

	return videoClaimStatus(status), nil
}

func (s videoClaimStatus) String() string {
	return string(s)
}
