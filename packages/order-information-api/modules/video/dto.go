package video

import (
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type VideoDto struct {
	Id               vo.VideoId         `json:"id"`
	WorkspaceId      vo.WorkspaceId     `json:"workspaceId"`
	MemberId         vo.MemberId        `json:"memberId"`
	Code             string             `json:"code"`
	ObjectStorageId  vo.ObjectStorageId `json:"objectStorageId"`
	ObjectStorageKey string             `json:"objectStorageKey"`
	Claim            VideoClaimDto      `json:"claim"`
	DurationSeconds  int                `json:"durationSeconds"`
	SizeKb           int                `json:"sizeKb"`
	IsSended         bool               `json:"isSended"`
	RecordingProgram string             `json:"recordingProgram"`
	RecordingIp      string             `json:"recordingIp"`
	RecordStartedAt  time.Time          `json:"recordStartedAt"`
	RecordFinishedAt time.Time          `json:"recordFinishedAt"`
	CreatedAt        time.Time          `json:"createdAt"`
	UpdatedAt        time.Time          `json:"updatedAt"`
}

type VideoClaimDto struct {
	Status string             `json:"status"`
	Logs   []VideoClaimLogDto `json:"logs"`
}

type VideoClaimLogDto struct {
	ManagerName string    `json:"managerName"`
	Contents    string    `json:"contents"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"updatedAt"`
}

type VideoCreateParamter struct {
	WorkspaceId      vo.WorkspaceId           `json:"workspaceId"`
	MemberId         vo.MemberId              `json:"memberId"`
	Code             string                   `json:"code"`
	ObjectStorageId  vo.ObjectStorageId       `json:"objectStorageId"`
	ObjectStorageKey string                   `json:"objectStorageKey"`
	DurationSeconds  int                      `json:"durationSeconds"`
	SizeKb           int                      `json:"sizeKb"`
	IsSended         bool                     `json:"isSended"`
	RecordingProgram string                   `json:"recordingProgram"`
	RecordingIp      string                   `json:"recordingIp"`
	RecordStartedAt  time.Time                `json:"recordStartedAt"`
	RecordFinishedAt time.Time                `json:"recordFinishedAt"`
	Claims           VideoCreateClaimParamter `json:"claims"`
}

type VideoCreateClaimParamter struct {
	Status string                        `json:"status"`
	Logs   []VideoCreateClaimLogParamter `json:"logs"`
}

type VideoCreateClaimLogParamter struct {
	ManagerName string    `json:"managerName"`
	Contents    string    `json:"contents"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"updatedAt"`
}

func VideoClaimCreateParamterToEntity(param *VideoCreateClaimParamter) (*VideoClaim, error) {
	claimLogs := make([]VideoClaimLog, len(param.Logs))
	for idx, log := range param.Logs {

		claimLogs[idx] = VideoClaimLog{
			ManagerName: log.ManagerName,
			Contents:    log.Contents,
			CreatedAt:   log.CreatedAt,
			UpdatedAt:   log.UpdatedAt,
		}
	}

	status, err := newVideoClaimStatus(param.Status)
	if err != nil {
		return nil, err
	}
	return &VideoClaim{
		Status: status,
		Logs:   claimLogs,
	}, nil
}

func VideoCreateParamterToEntity(param *VideoCreateParamter) (*Video, error) {
	id, err := vo.NewVideoId()
	if err != nil {
		return nil, err
	}

	claim, err := VideoClaimCreateParamterToEntity(&param.Claims)
	if err != nil {
		return nil, err
	}

	return &Video{
		Id:               *id,
		WorkspaceId:      param.WorkspaceId,
		MemberId:         param.MemberId,
		Code:             param.Code,
		ObjectStorageId:  param.ObjectStorageId,
		ObjectStorageKey: param.ObjectStorageKey,
		Claim:            *claim,
		DurationSeconds:  param.DurationSeconds,
		SizeKb:           param.SizeKb,
		IsSended:         false,
		RecordingProgram: param.RecordingProgram,
		RecordingIp:      param.RecordingIp,
		RecordStartedAt:  param.RecordStartedAt,
		RecordFinishedAt: param.RecordFinishedAt,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}, nil
}

func domainToDto(video *Video) *VideoDto {
	return &VideoDto{
		Id:               video.Id,
		WorkspaceId:      video.WorkspaceId,
		MemberId:         video.MemberId,
		Code:             video.Code,
		ObjectStorageId:  video.ObjectStorageId,
		ObjectStorageKey: video.ObjectStorageKey,
		Claim:            videoClaimToDto(video.Claim),
		DurationSeconds:  video.DurationSeconds,
		SizeKb:           video.SizeKb,
		IsSended:         video.IsSended,
		RecordingProgram: video.RecordingProgram,
		RecordingIp:      video.RecordingIp,
		RecordStartedAt:  video.RecordStartedAt,
		RecordFinishedAt: video.RecordFinishedAt,
		CreatedAt:        video.CreatedAt,
		UpdatedAt:        video.UpdatedAt,
	}
}

func videoClaimToDto(claim VideoClaim) VideoClaimDto {
	videoLog := make([]VideoClaimLogDto, len(claim.Logs))

	for idx, log := range claim.Logs {
		videoLog[idx] = VideoClaimLogDto{
			ManagerName: log.ManagerName,
			Contents:    log.Contents,
			CreatedAt:   log.CreatedAt,
			UpdatedAt:   log.UpdatedAt,
		}
	}

	return VideoClaimDto{
		Status: claim.Status.String(),
		Logs:   videoLog,
	}
}
