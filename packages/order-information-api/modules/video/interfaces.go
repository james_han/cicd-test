package video

import (
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
)

type IVideoRepository interface {
	FindById(id vo.VideoId) (*Video, error)
	FindByCriteria(criteria *search.Criteria) ([]*Video, *search.PagingMeta, error)
	DeleteById(id vo.VideoId) error
	Save(video *Video) (*vo.VideoId, error)
}
