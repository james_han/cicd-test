package video

import (
	"errors"
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
)

var (
	ErrNoData = errors.New("no data exists for the given id")
)

type VideoService struct {
	VideoRepos IVideoRepository
}

func NewVideoService(videoRepos IVideoRepository) *VideoService {
	return &VideoService{
		VideoRepos: videoRepos,
	}
}

func (s *VideoService) CreateVideo(createParams VideoCreateParamter) (*VideoDto, error) {
	var err error
	video, err := VideoCreateParamterToEntity(&createParams)
	if err != nil {
		return nil, err
	}

	if _, err = s.VideoRepos.Save(video); err != nil {
		return nil, err
	}

	return domainToDto(video), err
}

func (s *VideoService) FindByCriteria(criteria *search.Criteria) ([]*VideoDto, *search.PagingMeta, error) {
	var err error
	videos, paging, err := s.VideoRepos.FindByCriteria(criteria)
	if err != nil {
		return nil, nil, err
	}

	if len(videos) == 0 {
		return nil, nil, nil
	}

	videoDtos := make([]*VideoDto, len(videos))
	for i, video := range videos {
		videoDtos[i] = domainToDto(video)
	}

	return videoDtos, paging, err
}

func (s *VideoService) FindById(id vo.VideoId) (*VideoDto, error) {
	var err error

	video, err := s.VideoRepos.FindById(id)
	if err != nil {
		return nil, err
	}

	return domainToDto(video), err
}

func (s *VideoService) DeleteById(id vo.VideoId) error {
	var err error

	if err = s.VideoRepos.DeleteById(id); err != nil {
		return err
	}

	return nil
}

func (s *VideoService) UpdateIsSendedById(id vo.VideoId) (*VideoDto, error) {
	var err error

	video, err := s.VideoRepos.FindById(id)
	if err != nil {
		return nil, err
	}

	if video == nil {
		return nil, ErrNoData
	}

	video.Sended()

	if _, err = s.VideoRepos.Save(video); err != nil {
		return nil, err
	}

	return domainToDto(video), err
}

func (s *VideoService) UpdateVideoCode(id vo.VideoId, code string) (*VideoDto, error) {
	var err error

	video, err := s.VideoRepos.FindById(id)
	if err != nil {
		return nil, err
	}

	if video == nil {
		return nil, ErrNoData
	}

	video.UpdateCode(code)

	if _, err = s.VideoRepos.Save(video); err != nil {
		return nil, err
	}

	return domainToDto(video), err
}

func (s *VideoService) VideoSended() {}

func (s *VideoService) UpdateVideoClaim(id vo.VideoId, claimData VideoCreateClaimParamter) (*VideoDto, error) {
	var err error

	video, err := s.VideoRepos.FindById(id)
	if err != nil {
		return nil, err
	}

	if video == nil {
		return nil, errors.New("video not found")
	}

	claim, err := VideoClaimCreateParamterToEntity(&claimData)
	if err != nil {
		return nil, err
	}

	video.UpdateClaim(claim.Status, claim.Logs)

	if _, err = s.VideoRepos.Save(video); err != nil {
		return nil, err
	}

	return domainToDto(video), err
}
func (s *VideoService) CreateVideoClaim(id vo.VideoId, status string, managerName string, contents string) (*VideoDto, error) {
	var err error

	video, err := s.VideoRepos.FindById(id)
	if err != nil {
		return nil, err
	}

	switch status {
	case claimStatusSolved:
		video.ClaimSolved()
	case claimStatusNone:
		video.NoClaimExist()
	case claimStatusAnswering:
		video.ClaimAnswering()
	default:
		return nil, errors.New("invalid claim status")
	}

	video.AddClaim(managerName, contents)

	if _, err = s.VideoRepos.Save(video); err != nil {
		return nil, err
	}

	return domainToDto(video), err
}
