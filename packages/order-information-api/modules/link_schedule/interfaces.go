package linkschedule

import (
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
)

type LinkScheduleRepository interface {
	FindByCriteria(criteria *search.Criteria) ([]*LinkSchedule, *search.PagingMeta, error)
	FindById(id vo.LinkScheduleId) (*LinkSchedule, error)
	Save(linkSchedule *LinkSchedule) error
}
