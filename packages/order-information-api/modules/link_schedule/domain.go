package linkschedule

import (
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type LinkSchedule struct {
	Id                         vo.LinkScheduleId //id
	WorkspaceId                vo.WorkspaceId    //workspaceid
	Oms                        Oms               //솔루션(카페24,고도몰,메이크샵,이지어드민,사방넷,샵플링)
	OrderStatus                OrderStatus       //주문 상태(출고대기,출고완료,상품준비중,배송중,배송완료,구매확정)
	LinkStatus                 LinkStatus        //진행 상태(대기,진행중,실패,성공)
	LinkStartedAt              time.Time         //연동시작일
	LinkFinishedAt             time.Time         //연동종료일
	LinkType                   LinkType          //연동 기준(주문일,송장입력일)
	TotalOrderCount            int               //총 주문 수
	TotalOrderLinkSuccessCount int               //연동완료 주문수
	OrderLinkTryCount          int               //연동시도횟수
	Message                    string            //실패사유
	CreatedAt                  time.Time         //연동등록일
	UpdatedAt                  time.Time         //연동작업업데이트일
}

func newLinkSchedule(workspaceId vo.WorkspaceId, oms Oms, linkType LinkType, linkStartedAt, linkFinishedAt time.Time) (*LinkSchedule, error) {
	id, err := vo.NewLinkScheduleId()
	if err != nil {
		return nil, err
	}

	return &LinkSchedule{
		Id:                         *id,
		WorkspaceId:                workspaceId,
		Oms:                        oms,
		OrderStatus:                Uninitialized,
		LinkStatus:                 Waiting,
		LinkStartedAt:              linkStartedAt,
		LinkFinishedAt:             linkFinishedAt,
		LinkType:                   linkType,
		TotalOrderCount:            0,
		TotalOrderLinkSuccessCount: 0,
		OrderLinkTryCount:          0,
		Message:                    "",
		CreatedAt:                  time.Now(),
		UpdatedAt:                  time.Now(),
	}, nil
}
