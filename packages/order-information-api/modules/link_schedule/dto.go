package linkschedule

import (
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type LinkScheduleDto struct {
	Id                         vo.LinkScheduleId
	WorkspaceId                vo.WorkspaceId
	Oms                        Oms         //솔루션(cafe24,godoMall,makeshop,ezdamin,sabangnet,shopling)
	OrderStatus                OrderStatus //주문 상태(pending,dispatched,preparing,shipping,delivered,confirmed)
	LinkStatus                 LinkStatus  //진행 상태(waiting,progress,success,failure)
	LinkStartedAt              time.Time   //연동시작일
	LinkFinishedAt             time.Time   //연동종료일
	LinkType                   LinkType    //연동 기준(orderDate,waybillInputDate)
	TotalOrderCount            int         //총 주문 수
	TotalOrderLinkSuccessCount int         //연동완료 주문수
	OrderLinkTryCount          int         //연동시도횟수
	Message                    string      //실패사유
	CreatedAt                  time.Time   //연동등록일
	UpdatedAt                  time.Time   //연동작업업데이트일
}

func domainToDto(d *LinkSchedule) *LinkScheduleDto {
	return &LinkScheduleDto{
		Id:                         d.Id,
		WorkspaceId:                d.WorkspaceId,
		Oms:                        d.Oms,
		OrderStatus:                d.OrderStatus,
		LinkStatus:                 d.LinkStatus,
		LinkStartedAt:              d.LinkStartedAt,
		LinkFinishedAt:             d.LinkFinishedAt,
		LinkType:                   d.LinkType,
		TotalOrderCount:            d.TotalOrderCount,
		TotalOrderLinkSuccessCount: d.TotalOrderLinkSuccessCount,
		OrderLinkTryCount:          d.OrderLinkTryCount,
		Message:                    d.Message,
		CreatedAt:                  d.CreatedAt,
		UpdatedAt:                  d.UpdatedAt,
	}
}

func domainsToDtos(schedules []*LinkSchedule) []*LinkScheduleDto {
	var linkScheduleDtos []*LinkScheduleDto

	for _, schedule := range schedules {
		linkScheduleDtos = append(linkScheduleDtos, domainToDto(schedule))
	}
	
	return linkScheduleDtos
}

func dtoToDomain(d *LinkScheduleDto) *LinkSchedule {
	if d.CreatedAt.IsZero() {
		d.CreatedAt = time.Now()
	}

	if d.UpdatedAt.IsZero() {
		d.UpdatedAt = time.Now()
	}

	return &LinkSchedule{
		Id:                         d.Id,
		WorkspaceId:                d.WorkspaceId,
		Oms:                        d.Oms,
		OrderStatus:                d.OrderStatus,
		LinkStatus:                 d.LinkStatus,
		LinkStartedAt:              d.LinkStartedAt,
		LinkFinishedAt:             d.LinkFinishedAt,
		LinkType:                   d.LinkType,
		TotalOrderCount:            d.TotalOrderCount,
		TotalOrderLinkSuccessCount: d.TotalOrderLinkSuccessCount,
		OrderLinkTryCount:          d.OrderLinkTryCount,
		Message:                    d.Message,
		CreatedAt:                  d.CreatedAt,
		UpdatedAt:                  d.UpdatedAt,
	}
}
