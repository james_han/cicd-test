package linkschedule

import "fmt"

type Oms string
type OrderStatus string
type LinkStatus string
type LinkType string

const (
	Undefined Oms = "undefined" //초기값
	Cafe24    Oms = "cafe24"
	Godo      Oms = "godoMall"
	Makeshop  Oms = "makeshop"
	Ezadmin   Oms = "ezadmin"
	Sabangnet Oms = "sabangnet"
	Shopling  Oms = "shopling"
)

const (
	Uninitialized OrderStatus = "uninitialized" //초기값
	Pending       OrderStatus = "pending"       //출고대기
	Dispatched    OrderStatus = "dispatched"    //출고완료
	Preparing     OrderStatus = "preparing"     //상품준비중
	Shipping      OrderStatus = "shipping"      //배송중
	Delivered     OrderStatus = "delivered"     //배송완료
	Confirmed     OrderStatus = "confirmed"     //구매확정
)

const (
	Waiting  LinkStatus = "waiting"  //대기
	Progress LinkStatus = "progress" //진행중
	Success  LinkStatus = "success"  //성공
	Failure  LinkStatus = "failure"  //실패
)

const (
	OrderDate        LinkType = "orderDate"        //주문일
	WaybillInputDate LinkType = "waybillInputDate" //송장입력일
)

func isValidOms(omsName string) bool {
	switch Oms(omsName) {
	case Undefined, Cafe24, Godo, Makeshop, Ezadmin, Sabangnet, Shopling:
		return true
	default:
		return false
	}
}

func ParseOms(omsName string) (Oms, error) {
	if isValidOms(omsName) {
		return Oms(omsName), nil
	}
	return "", fmt.Errorf("invalid Oms: %s", omsName)
}

func (o Oms) String() string {
	return string(o)
}

func isValidLinkStatus(status string) bool {
	switch LinkStatus(status) {
	case Waiting, Progress, Success, Failure:
		return true
	default:
		return false
	}
}

func ParseLinkStatus(status string) (LinkStatus, error) {
	if isValidLinkStatus(status) {
		return LinkStatus(status), nil
	}
	return "", fmt.Errorf("invalid link status: %s", status)
}

func (o OrderStatus) String() string {
	return string(o)
}

func isValidOrderStatus(status string) bool {
	switch OrderStatus(status) {
	case Uninitialized, Pending, Dispatched, Preparing, Shipping, Delivered, Confirmed:
		return true
	default:
		return false
	}
}

func ParseOrderStatus(status string) (OrderStatus, error) {
	if isValidOrderStatus(status) {
		return OrderStatus(status), nil
	}
	return "", fmt.Errorf("invalid order status: %s", status)
}

func (l LinkStatus) String() string {
	return string(l)
}

func isValidLinkType(linkType string) bool {
	switch LinkType(linkType) {
	case OrderDate, WaybillInputDate:
		return true
	default:
		return false
	}
}

func ParseLinkType(linkType string) (LinkType, error) {
	if isValidLinkType(linkType) {
		return LinkType(linkType), nil
	}
	return "", fmt.Errorf("invalid link type: %s", linkType)
}

func (l LinkType) String() string {
	return string(l)
}
