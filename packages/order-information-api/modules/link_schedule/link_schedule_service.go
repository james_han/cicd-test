package linkschedule

import (
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type LinkScheduleService struct {
	repos LinkScheduleRepository
}

func NewLinkScheduleService(repos LinkScheduleRepository) *LinkScheduleService {
	return &LinkScheduleService{
		repos: repos,
	}
}

func (s *LinkScheduleService) FindByCriteria(criteria *search.Criteria) ([]*LinkScheduleDto, *search.PagingMeta, error) {
	schedules, pagingMeta, err := s.repos.FindByCriteria(criteria)
	if err != nil {
		return nil, nil, err
	}

	return domainsToDtos(schedules), pagingMeta, nil
}

func (s *LinkScheduleService) CreateLinkSchedule(workspaceId vo.WorkspaceId, oms Oms, linkType LinkType, linkStartedAt, linkFinishedAt time.Time) (*LinkScheduleDto, error) {
	newLinkSchedule, err := newLinkSchedule(workspaceId, oms, linkType, linkStartedAt, linkFinishedAt)
	if err != nil {
		return nil, err
	}

	err = s.repos.Save(newLinkSchedule)
	if err != nil {
		return nil, err
	}

	return domainToDto(newLinkSchedule), nil
}

// @Todo: Parameter
func (s *LinkScheduleService) UpdateLinkSchedule() (*LinkScheduleDto, error) {
	return nil, nil
}
