package excel_upload

import (
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type ExcelUpload struct {
	Id            vo.ExcelUploadId
	WorkspaceId   vo.WorkspaceId
	FileId        vo.FileId
	ResultFileId  vo.FileId
	IsOverlap     bool
	IsPhoneCheck  bool
	IsAllowHyphen bool
	Status        UploadStatus
	CreatedAt     time.Time
	UpdatedAt     time.Time
}
