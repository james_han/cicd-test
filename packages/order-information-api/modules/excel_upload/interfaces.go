package excel_upload

import "rp-order-information-api/modules/shared/search"

type ExcelUploadRepository interface {
	Save(excelUpload ExcelUpload) error
	FindByCriteria(criteria search.Criteria) ([]ExcelUpload, *search.PagingMeta, error)
}
