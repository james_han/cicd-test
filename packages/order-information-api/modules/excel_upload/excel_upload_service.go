package excel_upload

type ExcelUploadService struct {
	repo ExcelUploadRepository
}

func NewExcelUploadService(repo ExcelUploadRepository) ExcelUploadService {
	return ExcelUploadService{
		repo: repo,
	}
}

func (service *ExcelUploadService) Save(workspaceId, fileId string, isOverlap, isPhoneCheck, isAllowHyphen bool) (*ExcelUpload, error) {
	return nil, nil
}

func (service *ExcelUploadService) FindByCriteria() ([]ExcelUpload, error) {
	return nil, nil
}
