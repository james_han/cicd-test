package excel_upload

import "errors"

type UploadStatus string

const (
	WAITING  UploadStatus = "waiting"
	PROGRESS UploadStatus = "progress"
	SUCCESS  UploadStatus = "success"
	FAILURE  UploadStatus = "failure"
)

func NewUploadStatus(status string) (UploadStatus, error) {
	validUploadStatusArray := []UploadStatus{WAITING, PROGRESS, SUCCESS, FAILURE}
	for _, validUploadStatus := range validUploadStatusArray {
		if UploadStatus(status) == validUploadStatus {
			return UploadStatus(status), nil
		}
	}
	return "", errors.New("invalid file type: " + status)
}
