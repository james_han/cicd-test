package link_config

import (
	"database/sql"
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
)

type LinkConfigService struct {
	LinkConfigRepos LinkConfigRepository
}

func NewLinkConfigService(linkConfigRepos LinkConfigRepository) *LinkConfigService {
	return &LinkConfigService{
		LinkConfigRepos: linkConfigRepos,
	}
}

func (s *LinkConfigService) FindById(id vo.LinkConfigId) (*LinkConfigDto, error) {
	linkConfig, err := s.LinkConfigRepos.FindById(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	return domainToDto(linkConfig), nil
}

func (s *LinkConfigService) CreateLinkConfig(params CreateParamter) (*LinkConfigDto, error) {
	var err error

	linkConfig, err := CreateParamterToEntity(&params)
	if err != nil {
		return nil, err
	}

	if _, err := s.LinkConfigRepos.Save(linkConfig); err != nil {
		return nil, err
	}

	return domainToDto(linkConfig), nil
}

func (s *LinkConfigService) UpdateLinkConfig(id vo.LinkConfigId, params CreateParamter) (*LinkConfigDto, error) {
	linkConfig, err := s.LinkConfigRepos.FindById(id)
	if err != nil {
		return nil, err
	}

	newLinkConfig, err := CreateParamterToEntity(&params)
	if err != nil {
		return nil, err
	}

	newLinkConfig.Id = linkConfig.Id
	newLinkConfig.CreatedAt = linkConfig.CreatedAt

	if _, err := s.LinkConfigRepos.Save(newLinkConfig); err != nil {
		return nil, err
	}

	return domainToDto(newLinkConfig), nil
}

func (s *LinkConfigService) DeleteById(id vo.LinkConfigId) error {
	return s.LinkConfigRepos.DeleteById(id)
}

func (s *LinkConfigService) FindByCriteria(criteria *search.Criteria) ([]*LinkConfigDto, *search.PagingMeta, error) {
	var err error

	linkConfigs, paging, err := s.LinkConfigRepos.FindByCriteria(criteria)
	if err != nil {
		return nil, nil, err
	}

	if len(linkConfigs) == 0 {
		return nil, nil, nil
	}

	linkConfigDtos := make([]*LinkConfigDto, 0, len(linkConfigs))
	for _, linkConfig := range linkConfigs {
		linkConfigDtos = append(linkConfigDtos, domainToDto(linkConfig))
	}

	return linkConfigDtos, paging, nil
}
