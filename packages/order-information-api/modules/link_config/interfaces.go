package link_config

import (
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
)

type LinkConfigRepository interface {
	FindById(id vo.LinkConfigId) (*LinkConfig, error)
	Save(linkConfig *LinkConfig) (*vo.LinkConfigId, error)
	DeleteById(id vo.LinkConfigId) error
	FindByCriteria(criteria *search.Criteria) ([]*LinkConfig, *search.PagingMeta, error)
}
