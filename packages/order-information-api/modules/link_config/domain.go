package link_config

import (
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type LinkConfig struct {
	Id                  vo.LinkConfigId
	WorkspaceId         vo.WorkspaceId
	Cafe24Id            string
	Cafe24Domain        string
	GodoId              string
	GodoDomain          string
	GodoBusinessName    string
	MakeshopIsAdmin     bool
	MakeshopAdminDomain string
	MakeshopMallDomain  string
	MakeshopId          string
	MakeshopPassword    string
	EzadminPartnerKey   string
	EzadminDomainKey    string
	SabangnetApiKey     string
	SabangnetId         string
	ShoplingId          string
	ShoplingCompanyId   string
	ShoplingApiKey      string
	CreatedAt           time.Time
	UpdatedAt           time.Time
}
