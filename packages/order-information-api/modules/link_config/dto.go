package link_config

import (
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type LinkConfigDto struct {
	Id                  vo.LinkConfigId `json:"id"`
	WorkspaceId         vo.WorkspaceId  `json:"workspaceId"`
	Cafe24Id            string          `json:"cafe24Id"`
	Cafe24Domain        string          `json:"cafe24Domain"`
	GodoId              string          `json:"godoId"`
	GodoDomain          string          `json:"godoDomain"`
	GodoBusinessName    string          `json:"godoBusinessName"`
	MakeshopIsAdmin     bool            `json:"makeshopIsAdmin"`
	MakeshopAdminDomain string          `json:"makeshopAdminDomain"`
	MakeshopMallDomain  string          `json:"makeshopMallDomain"`
	MakeshopId          string          `json:"makeshopId"`
	MakeshopPassword    string          `json:"makeshopPassword"`
	EzadminPartnerKey   string          `json:"ezadminPartnerKey"`
	EzadminDomainKey    string          `json:"ezadminDomainKey"`
	SabangnetApiKey     string          `json:"sabangnetApiKey"`
	SabangnetId         string          `json:"sabangnetId"`
	ShoplingId          string          `json:"shoplingId"`
	ShoplingCompanyId   string          `json:"shoplingCompanyId"`
	ShoplingApiKey      string          `json:"shoplingApiKey"`
	CreatedAt           time.Time       `json:"createdAt"`
	UpdatedAt           time.Time       `json:"updatedAt"`
}

func domainToDto(linkConfig *LinkConfig) *LinkConfigDto {
	return &LinkConfigDto{
		Id:                  linkConfig.Id,
		WorkspaceId:         linkConfig.WorkspaceId,
		Cafe24Id:            linkConfig.Cafe24Id,
		Cafe24Domain:        linkConfig.Cafe24Domain,
		GodoId:              linkConfig.GodoId,
		GodoDomain:          linkConfig.GodoDomain,
		GodoBusinessName:    linkConfig.GodoBusinessName,
		MakeshopIsAdmin:     linkConfig.MakeshopIsAdmin,
		MakeshopAdminDomain: linkConfig.MakeshopAdminDomain,
		MakeshopMallDomain:  linkConfig.MakeshopMallDomain,
		MakeshopId:          linkConfig.MakeshopId,
		MakeshopPassword:    linkConfig.MakeshopPassword,
		EzadminPartnerKey:   linkConfig.EzadminPartnerKey,
		EzadminDomainKey:    linkConfig.EzadminDomainKey,
		SabangnetApiKey:     linkConfig.SabangnetApiKey,
		SabangnetId:         linkConfig.SabangnetId,
		ShoplingId:          linkConfig.ShoplingId,
		ShoplingCompanyId:   linkConfig.ShoplingCompanyId,
		ShoplingApiKey:      linkConfig.ShoplingApiKey,
		CreatedAt:           linkConfig.CreatedAt,
		UpdatedAt:           linkConfig.UpdatedAt,
	}
}

type CreateParamter struct {
	WorkspaceId         vo.WorkspaceId `json:"workspaceId"`
	Cafe24Id            string         `json:"cafe24Id"`
	Cafe24Domain        string         `json:"cafe24Domain"`
	GodoId              string         `json:"godoId"`
	GodoDomain          string         `json:"godoDomain"`
	GodoBusinessName    string         `json:"godoBusinessName"`
	MakeshopIsAdmin     bool           `json:"makeshopIsAdmin"`
	MakeshopAdminDomain string         `json:"makeshopAdminDomain"`
	MakeshopMallDomain  string         `json:"makeshopMallDomain"`
	MakeshopId          string         `json:"makeshopId"`
	MakeshopPassword    string         `json:"makeshopPassword"`
	EzadminPartnerKey   string         `json:"ezadminPartnerKey"`
	EzadminDomainKey    string         `json:"ezadminDomainKey"`
	SabangnetApiKey     string         `json:"sabangnetApiKey"`
	SabangnetId         string         `json:"sabangnetId"`
	ShoplingId          string         `json:"shoplingId"`
	ShoplingCompanyId   string         `json:"shoplingCompanyId"`
	ShoplingApiKey      string         `json:"shoplingApiKey"`
}

func CreateParamterToEntity(param *CreateParamter) (*LinkConfig, error) {
	id, err := vo.NewLinkConfigId()
	if err != nil {
		return nil, err
	}
	nowDt := time.Now()
	return &LinkConfig{
		Id:                  *id,
		WorkspaceId:         param.WorkspaceId,
		Cafe24Id:            param.Cafe24Id,
		Cafe24Domain:        param.Cafe24Domain,
		GodoId:              param.GodoId,
		GodoDomain:          param.GodoDomain,
		GodoBusinessName:    param.GodoBusinessName,
		MakeshopIsAdmin:     param.MakeshopIsAdmin,
		MakeshopAdminDomain: param.MakeshopAdminDomain,
		MakeshopMallDomain:  param.MakeshopMallDomain,
		MakeshopId:          param.MakeshopId,
		MakeshopPassword:    param.MakeshopPassword,
		EzadminPartnerKey:   param.EzadminPartnerKey,
		EzadminDomainKey:    param.EzadminDomainKey,
		SabangnetApiKey:     param.SabangnetApiKey,
		SabangnetId:         param.SabangnetId,
		ShoplingId:          param.ShoplingId,
		ShoplingCompanyId:   param.ShoplingCompanyId,
		ShoplingApiKey:      param.ShoplingApiKey,
		CreatedAt:           nowDt,
		UpdatedAt:           nowDt,
	}, nil
}
