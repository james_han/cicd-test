package logo

import (
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
)

type LogoRepository interface {
	FindById(vo.LogoId) (*Logo, error)
	FindByWorkspaceId(workspaceId vo.WorkspaceId) ([]*Logo, error)
	FindByCriteria(criteria *search.Criteria) ([]*Logo, *search.PagingMeta, error)
	Save(logo *Logo) error
	SaveMultiple(logo []*Logo) error
	DeleteById(logo *Logo) error
}
