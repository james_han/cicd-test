package logo

import (
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type LogoDto struct {
	Id          vo.LogoId      `json:"id"`
	WorkspaceId vo.WorkspaceId `json:"workspaceId"`
	IsActive    bool           `json:"isActive"`
	FileId      vo.FileId      `json:"fileId"`
	CreatedAt   time.Time      `json:"createdAt"`
	UpdatedAt   time.Time      `json:"updatedAt"`
}

func domainToDto(logo *Logo) *LogoDto {
	if logo == nil {
		return nil
	}
	return &LogoDto{
		Id:          logo.Id,
		WorkspaceId: logo.WorkspaceId,
		IsActive:    logo.IsActive,
		FileId:      logo.FileId,
		CreatedAt:   logo.CreatedAt,
		UpdatedAt:   logo.UpdatedAt,
	}
}

func domainsToDtos(logos []*Logo) []*LogoDto {
	var logoDtos []*LogoDto
	for _, logo := range logos {
		logoDtos = append(logoDtos, domainToDto(logo))
	}
	return logoDtos
}
