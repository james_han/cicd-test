package logo

import (
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
)

type LogoService struct {
	repo LogoRepository
}

func NewLogoService(logoRepository LogoRepository) *LogoService {
	return &LogoService{
		repo: logoRepository,
	}
}

func (s *LogoService) FindById(id vo.LogoId) (*LogoDto, error) {
	logo, err := s.repo.FindById(id)
	if err != nil {
		return nil, err
	}
	return domainToDto(logo), nil
}

func (s *LogoService) FindByCriteria(criteria *search.Criteria) ([]*LogoDto, *search.PagingMeta, error) {
	logos, pagingMeta, err := s.repo.FindByCriteria(criteria)
	if err != nil {
		return nil, nil, err
	}
	return domainsToDtos(logos), pagingMeta, nil
}

func (s *LogoService) Enable(id vo.LogoId) error {
	logo, err := s.repo.FindById(id)
	if err != nil {
		return err
	}
	logo.Enable()
	return s.repo.Save(logo)
}

func (s *LogoService) Disable(id vo.LogoId) error {
	logo, err := s.repo.FindById(id)
	if err != nil {
		return err
	}
	logo.Disable()
	return s.repo.Save(logo)
}

func (s *LogoService) Create(wid vo.WorkspaceId, fid vo.FileId, isActive bool) (*LogoDto, error) {
	logo, err := NewLogo(wid, fid)
	if err != nil {
		return nil, err
	}

	if domains, err := s.repo.FindByWorkspaceId(wid); err != nil {
		return nil, err
	} else if isActive {
		// 새로 생성하는 로고가 활성화 상태인 경우, 기존 로고들을 비활성화 시킨다.
		for _, domain := range domains {
			if domain.IsActive {
				domain.Disable()
			}
		}
		if err := s.repo.SaveMultiple(domains); err != nil {
			return nil, err
		}
		logo.Enable()
	}

	if err := s.repo.Save(logo); err != nil {
		return nil, err
	}

	return domainToDto(logo), nil
}

func (s *LogoService) GetActiveOnce(wid vo.WorkspaceId) (*LogoDto, error) {
	logos, err := s.repo.FindByWorkspaceId(wid)
	if err != nil {
		return nil, err
	}
	for _, logo := range logos {
		if logo.IsActive {
			return domainToDto(logo), nil
		}
	}
	return nil, nil
}

func (s *LogoService) Update(id vo.LogoId, wId vo.WorkspaceId, isActive bool, fId vo.FileId) (*LogoDto, error) {
	uLogo, err := s.repo.FindById(id)
	if err != nil {
		return nil, err
	}
	uLogo.Update(fId)

	logos, err := s.repo.FindByWorkspaceId(wId)
	if err != nil {
		return nil, err
	}

	if isActive {
		for _, logo := range logos {
			logo.Disable()
		}
		uLogo.Enable()
	}

	if err := s.repo.SaveMultiple(logos); err != nil {
		return nil, err
	}

	if err := s.repo.Save(uLogo); err != nil {
		return nil, err
	}
	return domainToDto(uLogo), nil
}
