package logo

import (
	"rp-order-information-api/modules/shared/vo"
	"time"
)

type Logo struct {
	Id          vo.LogoId      `db:"id"`
	WorkspaceId vo.WorkspaceId `db:"workspace_id"`
	IsActive    bool           `db:"is_active"`
	FileId      vo.FileId      `db:"file_id"`
	CreatedAt   time.Time      `db:"created_at"`
	UpdatedAt   time.Time      `db:"updated_at"`
}

func NewLogo(wid vo.WorkspaceId, fid vo.FileId) (*Logo, error) {
	id, err := vo.NewLogoId()
	return &Logo{
		Id:          *id,
		WorkspaceId: wid,
		IsActive:    false,
		FileId:      fid,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}, err
}

func (l *Logo) Disable() {
	if !l.IsActive {
		return
	}
	l.IsActive = false
	l.UpdatedAt = time.Now()
}

func (l *Logo) Enable() {
	l.IsActive = true
	l.UpdatedAt = time.Now()
}

func (l *Logo) Update(fid vo.FileId) {
	l.FileId = fid
	l.UpdatedAt = time.Now()
}
