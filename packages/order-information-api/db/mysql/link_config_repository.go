package mysql

import (
	"rp-order-information-api/modules/link_config"
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
	"strconv"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
)

var _ link_config.LinkConfigRepository = (*LinkConfigRepository)(nil)

type LinkConfigRepository struct {
	db     *sqlx.DB
	config *Config
}

const linkConfigTable = "ORDER_INFORMATION_link_config"

type linkConfigRow struct {
	Id                  vo.LinkConfigId `db:"id"`
	WorkspaceId         vo.WorkspaceId  `db:"workspace_id"`
	Cafe24Id            string          `db:"cafe24_id"`
	Cafe24Domain        string          `db:"cafe24_domain"`
	GodoId              string          `db:"godo_id"`
	GodoDomain          string          `db:"godo_domain"`
	GodoBusinessName    string          `db:"godo_business_name"`
	MakeshopIsAdmin     bool            `db:"makeshop_is_admin"`
	MakeshopAdminDomain string          `db:"makeshop_admin_domain"`
	MakeshopMallDomain  string          `db:"makeshop_mall_domain"`
	MakeshopId          string          `db:"makeshop_id"`
	MakeshopPassword    string          `db:"makeshop_password"`
	EzadminPartnerKey   string          `db:"ezadmin_partner_key"`
	EzadminDomainKey    string          `db:"ezadmin_domain_key"`
	SabangnetApiKey     string          `db:"sabangnet_api_key"`
	SabangnetId         string          `db:"sabangnet_id"`
	ShoplingId          string          `db:"shopling_id"`
	ShoplingCompanyId   string          `db:"shopling_company_id"`
	ShoplingApiKey      string          `db:"shopling_api_key"`
	CreatedAt           time.Time       `db:"created_at"`
	UpdatedAt           time.Time       `db:"updated_at"`
}

func NewLinkConfigRepository(config *Config) (*LinkConfigRepository, error) {
	db, err := sqlx.Connect("mysql", config.GetDSN())
	if err != nil {
		return nil, err
	}
	return &LinkConfigRepository{
		db:     db,
		config: config,
	}, nil
}

func (r *LinkConfigRepository) checkAndReconnect() error {
	if err := r.db.Ping(); err != nil {
		db, err := sqlx.Connect("mysql", r.config.GetDSN())
		if err != nil {
			return err
		}
		r.db = db
	}
	return nil
}

func (r *LinkConfigRepository) FindById(id vo.LinkConfigId) (*link_config.LinkConfig, error) {
	var err error

	if err = r.checkAndReconnect(); err != nil {
		return nil, err
	}

	var linkConfigRow linkConfigRow

	if err = r.db.Get(&linkConfigRow, "SELECT * FROM "+linkConfigTable+" WHERE id = ?", id); err != nil {
		return nil, err
	}

	return r.rowToEntity(&linkConfigRow), nil
}

func (r *LinkConfigRepository) Save(linkConfig *link_config.LinkConfig) (*vo.LinkConfigId, error) {
	var err error

	if err = r.checkAndReconnect(); err != nil {
		return nil, err
	}

	linkConfigRow := r.entityToRow(linkConfig)

	query := `
		INSERT INTO
			` + linkConfigTable + `
		(
			id,
			workspace_id,
			cafe24_id,
			cafe24_domain,
			godo_id,
			godo_domain,
			godo_business_name,
			makeshop_is_admin,
			makeshop_admin_domain,
			makeshop_mall_domain,
			makeshop_id,
			makeshop_password,
			ezadmin_partner_key,
			ezadmin_domain_key,
			sabangnet_api_key,
			sabangnet_id,
			shopling_id,
			shopling_company_id,
			shopling_api_key,
			created_at,
			updated_at
		)
		VALUES (
			:id,
			:workspace_id,
			:cafe24_id,
			:cafe24_domain,
			:godo_id,
			:godo_domain,
			:godo_business_name,
			:makeshop_is_admin,
			:makeshop_admin_domain,
			:makeshop_mall_domain,
			:makeshop_id,
			:makeshop_password,
			:ezadmin_partner_key,
			:ezadmin_domain_key,
			:sabangnet_api_key,
			:sabangnet_id,
			:shopling_id,
			:shopling_company_id,
			:shopling_api_key,
			:created_at,
			:updated_at
		)
		ON DUPLICATE KEY UPDATE
			workspace_id = VALUES(workspace_id),
			cafe24_id = VALUES(cafe24_id),
			cafe24_domain = VALUES(cafe24_domain),
			godo_id = VALUES(godo_id),
			godo_domain = VALUES(godo_domain),
			godo_business_name = VALUES(godo_business_name),
			makeshop_is_admin = VALUES(makeshop_is_admin),
			makeshop_admin_domain = VALUES(makeshop_admin_domain),
			makeshop_mall_domain = VALUES(makeshop_mall_domain),
			makeshop_id = VALUES(makeshop_id),
			makeshop_password = VALUES(makeshop_password),
			ezadmin_partner_key = VALUES(ezadmin_partner_key),
			ezadmin_domain_key = VALUES(ezadmin_domain_key),
			sabangnet_api_key = VALUES(sabangnet_api_key),
			sabangnet_id = VALUES(sabangnet_id),
			shopling_id = VALUES(shopling_id),
			shopling_company_id = VALUES(shopling_company_id),
			shopling_api_key = VALUES(shopling_api_key),
			updated_at = VALUES(updated_at)
	`

	if _, err = r.db.NamedExec(query, linkConfigRow); err != nil {
		return nil, err
	}

	return &linkConfigRow.Id, nil
}

func (r *LinkConfigRepository) entityToRow(linkConfig *link_config.LinkConfig) *linkConfigRow {
	return &linkConfigRow{
		Id:                  linkConfig.Id,
		WorkspaceId:         linkConfig.WorkspaceId,
		Cafe24Id:            linkConfig.Cafe24Id,
		Cafe24Domain:        linkConfig.Cafe24Domain,
		GodoId:              linkConfig.GodoId,
		GodoDomain:          linkConfig.GodoDomain,
		GodoBusinessName:    linkConfig.GodoBusinessName,
		MakeshopIsAdmin:     linkConfig.MakeshopIsAdmin,
		MakeshopAdminDomain: linkConfig.MakeshopAdminDomain,
		MakeshopMallDomain:  linkConfig.MakeshopMallDomain,
		MakeshopId:          linkConfig.MakeshopId,
		MakeshopPassword:    linkConfig.MakeshopPassword,
		EzadminPartnerKey:   linkConfig.EzadminPartnerKey,
		EzadminDomainKey:    linkConfig.EzadminDomainKey,
		SabangnetApiKey:     linkConfig.SabangnetApiKey,
		SabangnetId:         linkConfig.SabangnetId,
		ShoplingId:          linkConfig.ShoplingId,
		ShoplingCompanyId:   linkConfig.ShoplingCompanyId,
		ShoplingApiKey:      linkConfig.ShoplingApiKey,
		CreatedAt:           linkConfig.CreatedAt,
		UpdatedAt:           linkConfig.UpdatedAt,
	}
}

func (r *LinkConfigRepository) rowToEntity(row *linkConfigRow) *link_config.LinkConfig {
	return &link_config.LinkConfig{
		Id:                  row.Id,
		WorkspaceId:         row.WorkspaceId,
		Cafe24Id:            row.Cafe24Id,
		Cafe24Domain:        row.Cafe24Domain,
		GodoId:              row.GodoId,
		GodoDomain:          row.GodoDomain,
		GodoBusinessName:    row.GodoBusinessName,
		MakeshopIsAdmin:     row.MakeshopIsAdmin,
		MakeshopAdminDomain: row.MakeshopAdminDomain,
		MakeshopMallDomain:  row.MakeshopMallDomain,
		MakeshopId:          row.MakeshopId,
		MakeshopPassword:    row.MakeshopPassword,
		EzadminPartnerKey:   row.EzadminPartnerKey,
		EzadminDomainKey:    row.EzadminDomainKey,
		SabangnetApiKey:     row.SabangnetApiKey,
		SabangnetId:         row.SabangnetId,
		ShoplingId:          row.ShoplingId,
		ShoplingCompanyId:   row.ShoplingCompanyId,
		ShoplingApiKey:      row.ShoplingApiKey,
		CreatedAt:           row.CreatedAt,
		UpdatedAt:           row.UpdatedAt,
	}
}

func (r *LinkConfigRepository) DeleteById(id vo.LinkConfigId) error {
	var err error

	if err = r.checkAndReconnect(); err != nil {
		return err
	}

	if _, err = r.db.Exec("DELETE FROM "+linkConfigTable+" WHERE id = ?", id); err != nil {
		return err
	}

	return nil
}

func (r *LinkConfigRepository) FindByCriteria(criteria *search.Criteria) ([]*link_config.LinkConfig, *search.PagingMeta, error) {
	var err error

	if err = r.checkAndReconnect(); err != nil {
		return nil, nil, err
	}

	query := `SELECT * FROM ` + linkConfigTable + ` WHERE 1 = 1`

	whereClause, args := buildWhereClause(criteria.Filters)

	if whereClause != nil {
		query += ` AND ` + strings.Join(whereClause, ` AND `)
	}

	if criteria.Page != nil && criteria.Page.Cursor != "" {
		if criteria.Sort.Order == search.Ascending {
			query += ` AND id > '` + criteria.Page.Cursor + `'`
		} else {
			query += ` AND id < '` + criteria.Page.Cursor + `'`
		}
	}

	if criteria.Sort != nil {
		if criteria.Sort.Order == "" {
			criteria.Sort.Order = search.Descending
		}

		if criteria.Sort.Field == "" {
			criteria.Sort.Field = "id"
		}

		query += ` ORDER BY ` + criteria.Sort.Field + ` ` + criteria.Sort.Order.String() + `, id ` + criteria.Sort.Order.String()
	}

	if criteria.Page != nil && criteria.Page.PageSize > 0 {
		pageSize := strconv.Itoa(criteria.Page.PageSize)
		if criteria.Page.Page > 0 {
			offset := strconv.Itoa(criteria.Page.PageSize * (criteria.Page.Page - 1))
			query += ` LIMIT ` + pageSize + ` OFFSET ` + offset
		} else {
			query += ` LIMIT ` + pageSize
		}
	}

	linkConfigRows := []*linkConfigRow{}
	if err = r.db.Select(&linkConfigRows, query, args...); err != nil {
		return nil, nil, err
	}

	var pagingCursor string
	linkConfigs := []*link_config.LinkConfig{}
	for _, row := range linkConfigRows {
		linkConfigs = append(linkConfigs, r.rowToEntity(row))
		pagingCursor = row.Id.String()
	}

	if len(linkConfigs) == 0 {
		return nil, nil, nil
	}

	totalCount, err := countTotalRecords(r.db, linkConfigTable, whereClause, args)
	if err != nil {
		return nil, nil, err
	}

	pagingMeta := &search.PagingMeta{
		TotalCount: totalCount,
	}

	if criteria.Page != nil {
		if criteria.Page.Page > 0 {
			pagingMeta.CurrentPage = criteria.Page.Page
			pagingMeta.TotalPages = calculateTotalPage(totalCount, criteria.Page.PageSize)
		} else {
			pagingMeta.NextCursor = pagingCursor
		}

		if criteria.Page.PageSize > 0 {
			pagingMeta.Size = criteria.Page.PageSize
		}
	}

	return linkConfigs, pagingMeta, nil
}
