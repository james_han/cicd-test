package mysql

import (
	"fmt"
	"math"
	"rp-order-information-api/modules/shared/search"
	"strings"
	"unicode"

	"github.com/jmoiron/sqlx"
)

func buildInQueryParam(list []string) (string, []interface{}) {
	var params []interface{}
	var placeholders []string

	for _, str := range list {
		params = append(params, str)
		placeholders = append(placeholders, "?")
	}

	return strings.Join(placeholders, ","), params
}

// buildWhereClause는 주어진 필터를 바탕으로 WHERE 절을 생성합니다.
func buildWhereClause(filters []search.Filter) ([]string, []interface{}) {
	var clauses []string
	var args []interface{}

	for _, filter := range filters {

		if filter.Operator == search.In {
			placeholderstr, params := buildInQueryParam(filter.Value.([]string))
			clauses = append(clauses, fmt.Sprintf("%s %s (%s)", camelToSnake(filter.Field), filter.Operator, placeholderstr))
			args = append(args, params...)
			continue
		}

		clauses = append(clauses, fmt.Sprintf("%s %s ?", camelToSnake(filter.Field), filter.Operator))
		args = append(args, func() interface{} {
			if filter.Operator == search.Like || filter.Operator == search.NotLike {
				if value, ok := filter.Value.(string); ok {
					return "%" + value + "%"
				}
			}
			return filter.Value
		}())
	}
	fmt.Printf("clauses: %+v\n", clauses)
	fmt.Printf("args: %+v\n", args)

	return clauses, args
}

// countTotalRecords는 주어진 조건에 맞는 레코드의 총 수를 계산합니다.
func countTotalRecords(db *sqlx.DB, tableName string, whereClauses []string, args []interface{}) (int, error) {
	query := fmt.Sprintf("SELECT COUNT(*) FROM %s", tableName)
	if len(whereClauses) > 0 {
		query += " WHERE " + strings.Join(whereClauses, " AND ")
	}

	var count int
	err := db.QueryRow(query, args...).Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func calculateTotalPage(totalCount, pageSize int) int {
	return int(math.Ceil(float64(totalCount) / float64(pageSize)))
}

func camelToSnake(s string) string {
	var result []rune
	for i, r := range s {
		if unicode.IsUpper(r) && i > 0 {
			result = append(result, '_')
		}
		result = append(result, unicode.ToLower(r))
	}
	return string(result)
}
