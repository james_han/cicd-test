package mysql

type Config struct {
	Host     string
	Port     string
	Username string
	Password string
	Database string
}

func NewConfig(host, port, username, password, database string) *Config {
	return &Config{
		Host:     host,
		Port:     port,
		Username: username,
		Password: password,
		Database: database,
	}
}

func NewConfigFromTestDB(database string) *Config {
	return &Config{
		Host:     "118.67.135.151",
		Port:     "3306",
		Username: "server_admin",
		Password: "Inbet!@34%^",
		Database: database,
	}
}

func (c *Config) GetHost() string {
	return c.Host
}

func (c *Config) GetPort() string {
	return c.Port
}

func (c *Config) GetUsername() string {
	return c.Username
}

func (c *Config) GetPassword() string {
	return c.Password
}

func (c *Config) GetDatabase() string {
	return c.Database
}

func (c *Config) GetDSN() string {
	return c.GetUsername() + ":" + c.GetPassword() + "@tcp(" + c.GetHost() + ":" + c.GetPort() + ")/" + c.GetDatabase() + "?charset=utf8&parseTime=True&loc=Local"
}

func (c *Config) GetDSNWithoutDatabase() string {
	return c.GetUsername() + ":" + c.GetPassword() + "@tcp(" + c.GetHost() + ":" + c.GetPort() + ")/?charset=utf8&parseTime=True&loc=Local"
}
