package mysql

import (
	"database/sql"
	"encoding/json"
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/modules/video"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type videoRow struct {
	Id               vo.VideoId     `db:"id"`
	WorkspaceId      vo.WorkspaceId `db:"workspace_id"`
	MemberId         vo.MemberId    `db:"member_id"`
	Code             string         `db:"code"`
	ObjectStorageId  string         `db:"object_storage_id"`
	ObjectStorageKey string         `db:"object_storage_key"`
	Claim            string         `db:"claim"`
	DurationSeconds  int            `db:"duration_seconds"`
	SizeKb           int            `db:"size_kb"`
	IsSended         bool           `db:"is_sended"`
	RecordingProgram string         `db:"recording_program"`
	RecordingIp      string         `db:"recording_ip"`
	RecordStartedAt  time.Time      `db:"record_started_at"`
	RecordFinishedAt time.Time      `db:"record_finished_at"`
	CreatedAt        time.Time      `db:"created_at"`
	UpdatedAt        time.Time      `db:"updated_at"`
}

type VideoRepository struct {
	db *sqlx.DB
}

const videoTable = "VIDEO_video"

func NewVideoRepository(config *Config) (*VideoRepository, error) {
	dsn := config.GetDSN()
	db, err := sqlx.Connect("mysql", dsn)
	if err != nil {
		return nil, err
	}

	return &VideoRepository{
		db: db,
	}, nil
}

func (r *VideoRepository) FindById(id vo.VideoId) (*video.Video, error) {
	query := `SELECT * FROM ` + videoTable + ` WHERE id = ?`
	var videoRow videoRow

	if err := r.db.Get(&videoRow, query, id); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	video, err := r.rowToEntity(&videoRow)
	if err != nil {
		return nil, err
	}

	if video == nil {
		return nil, nil
	}

	return video, nil
}

func (r *VideoRepository) FindByCriteria(criteria *search.Criteria) ([]*video.Video, *search.PagingMeta, error) {

	query := `SELECT * FROM ` + videoTable + ` WHERE 1=1`

	whereClause, args := buildWhereClause(criteria.Filters)

	if whereClause != nil {
		query += ` AND ` + strings.Join(whereClause, ` AND `)
	}

	if criteria.Page != nil && criteria.Page.Cursor != "" {
		if criteria.Sort.Order == search.Ascending {
			query += ` AND id > '` + criteria.Page.Cursor + `'`
		} else {
			query += ` AND id < '` + criteria.Page.Cursor + `'`
		}
	}

	if criteria.Sort != nil {
		if criteria.Sort.Order == "" {
			criteria.Sort.Order = search.Descending
		}

		if criteria.Sort.Field == "" {
			criteria.Sort.Field = "id"
		}

		query += ` ORDER BY ` + criteria.Sort.Field + ` ` + criteria.Sort.Order.String() + `, id ` + criteria.Sort.Order.String()
	}

	if criteria.Page != nil && criteria.Page.PageSize > 0 {
		pageSize := strconv.Itoa(criteria.Page.PageSize)
		if criteria.Page.Page > 0 {
			offset := strconv.Itoa(criteria.Page.PageSize * (criteria.Page.Page - 1))
			query += ` LIMIT ` + pageSize + ` OFFSET ` + offset
		} else {
			query += ` LIMIT ` + pageSize
		}
	}

	videoRows := []*videoRow{}
	if err := r.db.Select(&videoRows, query, args...); err != nil {
		return nil, nil, err
	}

	var pagingCursor string
	videos := []*video.Video{}
	for _, row := range videoRows {
		video, err := r.rowToEntity(row)
		if err != nil {
			return nil, nil, err
		}

		videos = append(videos, video)
		pagingCursor = row.Id.String()
	}

	totalCount, err := countTotalRecords(r.db, videoTable, whereClause, args)
	if err != nil {
		return nil, nil, err
	}

	pagingMeta := &search.PagingMeta{
		TotalCount: totalCount,
	}

	if criteria.Page != nil {
		if criteria.Page.Page > 0 {
			pagingMeta.CurrentPage = criteria.Page.Page
			pagingMeta.TotalPages = calculateTotalPage(totalCount, criteria.Page.PageSize)
		} else {
			pagingMeta.NextCursor = pagingCursor
		}

		if criteria.Page.PageSize > 0 {
			pagingMeta.Size = criteria.Page.PageSize
		}
	}

	return videos, pagingMeta, nil
}

func (r *VideoRepository) DeleteById(id vo.VideoId) error {
	if _, err := r.db.Exec("DELETE FROM "+videoTable+" WHERE id = ?", id); err != nil {
		return err
	}

	return nil
}
func (r *VideoRepository) Save(video *video.Video) (*vo.VideoId, error) {
	var err error

	claim, err := json.Marshal(video.Claim)
	if err != nil {
		return nil, err
	}

	videoRow := videoRow{
		Id:               video.Id,
		WorkspaceId:      video.WorkspaceId,
		MemberId:         video.MemberId,
		Code:             video.Code,
		ObjectStorageId:  video.ObjectStorageId.String(),
		ObjectStorageKey: video.ObjectStorageKey,
		Claim:            string(claim),
		DurationSeconds:  video.DurationSeconds,
		SizeKb:           video.SizeKb,
		IsSended:         video.IsSended,
		RecordingProgram: video.RecordingProgram,
		RecordingIp:      video.RecordingIp,
		RecordStartedAt:  video.RecordStartedAt,
		RecordFinishedAt: video.RecordFinishedAt,
		CreatedAt:        video.CreatedAt,
		UpdatedAt:        video.UpdatedAt,
	}
	query := `
		INSERT INTO ` + videoTable + `
		(
			id, workspace_id, member_id, code, object_storage_id, object_storage_key, claim, duration_seconds, size_kb, is_sended, recording_program, recording_ip, record_started_at, record_finished_at, created_at, updated_at
		)
		VALUES
		(
			:id, :workspace_id, :member_id, :code, :object_storage_id, :object_storage_key, :claim, :duration_seconds, :size_kb, :is_sended, :recording_program, :recording_ip, :record_started_at, :record_finished_at, :created_at, :updated_at
		)
		ON DUPLICATE KEY UPDATE
			id = VALUES(id),
			workspace_id = VALUES(workspace_id),
			member_id = VALUES(member_id),
			code = VALUES(code),
			object_storage_id = VALUES(object_storage_id),
			object_storage_key = VALUES(object_storage_key),
			claim = VALUES(claim),
			duration_seconds = VALUES(duration_seconds),
			size_kb = VALUES(size_kb),
			is_sended = VALUES(is_sended),
			recording_program = VALUES(recording_program),
			recording_ip = VALUES(recording_ip),
			record_started_at = VALUES(record_started_at),
			record_finished_at = VALUES(record_finished_at),
			updated_at = VALUES(updated_at);
	`

	if _, err := r.db.NamedExec(query, videoRow); err != nil {
		return nil, err
	}

	return &video.Id, nil
}

func (r *VideoRepository) rowToEntity(row *videoRow) (*video.Video, error) {
	var err error

	objectStorageId, err := vo.NewObjectStorageIdFromString(row.ObjectStorageId)
	if err != nil {
		return nil, err
	}

	claim := video.VideoClaim{}
	if err = json.Unmarshal([]byte(row.Claim), &claim); err != nil {
		return nil, err
	}

	workspaceId, err := vo.NewWorkspaceIdFromString(row.WorkspaceId.String())
	if err != nil {
		return nil, err
	}

	memberId, err := vo.NewMemberIdFromString(row.MemberId.String())
	if err != nil {
		return nil, err
	}

	return &video.Video{
		Id:               row.Id,
		WorkspaceId:      *workspaceId,
		MemberId:         *memberId,
		Code:             row.Code,
		ObjectStorageId:  *objectStorageId,
		ObjectStorageKey: row.ObjectStorageKey,
		Claim:            claim,
		DurationSeconds:  row.DurationSeconds,
		SizeKb:           row.SizeKb,
		IsSended:         row.IsSended,
		RecordingProgram: row.RecordingProgram,
		RecordingIp:      row.RecordingIp,
		RecordStartedAt:  row.RecordStartedAt,
		RecordFinishedAt: row.RecordFinishedAt,
		CreatedAt:        row.CreatedAt,
		UpdatedAt:        row.UpdatedAt,
	}, nil
}
