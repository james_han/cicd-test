package mysql

import (
	linkschedule "rp-order-information-api/modules/link_schedule"
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
	"strconv"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
)

type LinkScheduleRepository struct {
	db     *sqlx.DB
	config *Config
}

type linkScheduleRow struct {
	Id                         vo.LinkScheduleId `db:"id"`
	WorkspaceId                vo.WorkspaceId    `db:"workspace_id"`
	Oms                        string            `db:"oms"`
	OrderStatus                string            `db:"order_status"`
	LinkStatus                 string            `db:"link_status"`
	LinkStartedAt              time.Time         `db:"link_started_at"`
	LinkFinishedAt             time.Time         `db:"link_finished_at"`
	LinkType                   string            `db:"link_type"`
	TotalOrderCount            int               `db:"total_order_count"`
	TotalOrderLinkSuccessCount int               `db:"total_order_link_success_count"`
	OrderLinkTryCount          int               `db:"order_link_try_count"`
	Message                    string            `db:"message"`
	CreatedAt                  time.Time         `db:"created_at"`
	UpdatedAt                  time.Time         `db:"updated_at"`
}

const linkScheduleTable = "ORDER_INFORMATION_link_schedule"

func NewLinkScheduleRepository(config *Config) (*LinkScheduleRepository, error) {
	db, err := sqlx.Connect("mysql", config.GetDSN())
	if err != nil {
		return nil, err
	}
	return &LinkScheduleRepository{
		db:     db,
		config: config,
	}, nil
}

func (r *LinkScheduleRepository) checkAndReconnect() error {
	if err := r.db.Ping(); err != nil {
		db, err := sqlx.Connect("mysql", r.config.GetDSN())
		if err != nil {
			return err
		}
		r.db = db
	}
	return nil
}

func (r *LinkScheduleRepository) FindByCriteria(criteria *search.Criteria) ([]*linkschedule.LinkSchedule, *search.PagingMeta, error) {
	if err := r.checkAndReconnect(); err != nil {
		return nil, nil, err
	}

	query := `SELECT * FROM ` + linkScheduleTable + ` WHERE 1=1`

	whereClause, args := buildWhereClause(criteria.Filters)
	if whereClause != nil {
		query += ` AND ` + strings.Join(whereClause, ` AND `)
	}

	if criteria.Page != nil && criteria.Page.Cursor != "" {
		if criteria.Sort.Order == search.Ascending {
			query += ` AND id > '` + criteria.Page.Cursor + `'`
		} else {
			query += ` AND id < '` + criteria.Page.Cursor + `'`
		}
	}

	if criteria.Sort != nil {
		if criteria.Sort.Order == "" {
			criteria.Sort.Order = search.Descending
		}

		if criteria.Sort.Field == "" {
			criteria.Sort.Field = "id"
		}

		query += ` ORDER BY ` + criteria.Sort.Field + ` ` + criteria.Sort.Order.String() + `, id ` + criteria.Sort.Order.String()
	}

	if criteria.Page != nil && criteria.Page.PageSize > 0 {
		pageSize := strconv.Itoa(criteria.Page.PageSize)
		if criteria.Page.Page > 0 {
			offset := strconv.Itoa(criteria.Page.PageSize * (criteria.Page.Page - 1))
			query += ` LIMIT ` + pageSize + ` OFFSET ` + offset
		} else {
			query += ` LIMIT ` + pageSize
		}
	}

	linkScheduleRows := []*linkScheduleRow{}
	if err := r.db.Select(&linkScheduleRows, query, args...); err != nil {
		return nil, nil, err
	}

	var pagingCursor string
	linkSchedules := make([]*linkschedule.LinkSchedule, 0, len(linkScheduleRows))
	for _, row := range linkScheduleRows {
		linkSchedule := r.rowToEntity(row)
		linkSchedules = append(linkSchedules, linkSchedule)

		pagingCursor = row.Id.String()
	}

	totalCount, err := countTotalRecords(r.db, linkScheduleTable, whereClause, args)
	if err != nil {
		return nil, nil, err
	}

	pagingMeta := &search.PagingMeta{
		TotalCount: totalCount,
	}

	if criteria.Page != nil {
		if criteria.Page.Page > 0 {
			pagingMeta.CurrentPage = criteria.Page.Page
			pagingMeta.TotalPages = calculateTotalPage(totalCount, criteria.Page.PageSize)
		} else {
			pagingMeta.NextCursor = pagingCursor
		}

		if criteria.Page.PageSize > 0 {
			pagingMeta.Size = criteria.Page.PageSize
		}
	}

	return linkSchedules, pagingMeta, nil
}

func (r *LinkScheduleRepository) FindById(id vo.LinkScheduleId) (*linkschedule.LinkSchedule, error) {
	return nil, nil
}

func (r *LinkScheduleRepository) Save(linkSchedule *linkschedule.LinkSchedule) error {
	if err := r.checkAndReconnect(); err != nil {
		return err
	}

	linkScheduleRow := r.entityToRow(linkSchedule)

	query := `INSERT INTO ` + linkScheduleTable + `(id, workspace_id, oms, order_status, link_status, link_started_at, link_finished_at, link_type, 
				total_order_count, total_order_link_success_count, order_link_try_count, message, created_at, updated_at) 
			VALUES(
				:id, :workspace_id, :oms, :order_status, :link_status, :link_started_at, :link_finished_at, :link_type, 
				:total_order_count, :total_order_link_success_count, :order_link_try_count, :message, :created_at, :updated_at)`

	_, err := r.db.NamedExec(query, linkScheduleRow)

	if err != nil {
		return err
	}

	return nil
}

func (r *LinkScheduleRepository) entityToRow(entity *linkschedule.LinkSchedule) *linkScheduleRow {
	return &linkScheduleRow{
		Id:                         entity.Id,
		WorkspaceId:                entity.WorkspaceId,
		Oms:                        entity.Oms.String(),
		OrderStatus:                entity.OrderStatus.String(),
		LinkStatus:                 entity.LinkStatus.String(),
		LinkStartedAt:              entity.LinkStartedAt,
		LinkFinishedAt:             entity.LinkFinishedAt,
		LinkType:                   entity.LinkType.String(),
		TotalOrderCount:            entity.TotalOrderCount,
		TotalOrderLinkSuccessCount: entity.TotalOrderLinkSuccessCount,
		OrderLinkTryCount:          entity.OrderLinkTryCount,
		Message:                    entity.Message,
		CreatedAt:                  entity.CreatedAt,
		UpdatedAt:                  entity.UpdatedAt,
	}
}

func (r *LinkScheduleRepository) rowToEntity(row *linkScheduleRow) *linkschedule.LinkSchedule {
	oms, _ := linkschedule.ParseOms(row.Oms)
	orderStatus, _ := linkschedule.ParseOrderStatus(row.OrderStatus)
	linkStatus, _ := linkschedule.ParseLinkStatus(row.LinkStatus)
	linkType, _ := linkschedule.ParseLinkType(row.LinkType)

	return &linkschedule.LinkSchedule{
		Id:                         row.Id,
		WorkspaceId:                row.WorkspaceId,
		Oms:                        oms,
		OrderStatus:                orderStatus,
		LinkStatus:                 linkStatus,
		LinkStartedAt:              row.LinkStartedAt,
		LinkFinishedAt:             row.LinkFinishedAt,
		LinkType:                   linkType,
		TotalOrderCount:            row.TotalOrderCount,
		TotalOrderLinkSuccessCount: row.TotalOrderLinkSuccessCount,
		OrderLinkTryCount:          row.OrderLinkTryCount,
		Message:                    row.Message,
		CreatedAt:                  row.CreatedAt,
		UpdatedAt:                  row.UpdatedAt,
	}
}
