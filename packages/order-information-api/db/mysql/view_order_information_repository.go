package mysql

import (
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
	"rp-order-information-api/modules/view_model/view_order_information"
	"strconv"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
)

var _ view_order_information.OrderInformationRepository = (*ViewOrderInformationRepository)(nil)

type ViewOrderInformationRepository struct {
	db     *sqlx.DB
	config *Config
}

type viewOrderInformationRow struct {
	Id                    vo.OrderInformationId     `db:"id"`
	WorkspaceId           vo.WorkspaceId            `db:"workspace_id"`
	MemberId              vo.MemberId               `db:"member_id"`
	OrderId               string                    `db:"order_id"`
	Code                  string                    `db:"code"`
	Courier               string                    `db:"courier"`
	Timezone              string                    `db:"timezone"`
	OrdererName           string                    `db:"orderer_name"`
	OrdererEmail          string                    `db:"orderer_email"`
	OrdererCellPhoneCode  string                    `db:"orderer_cellphone_code"`
	OrdererCellPhone      string                    `db:"orderer_cellphone"`
	OrdererPhoneCode      string                    `db:"orderer_phone_code"`
	OrdererPhone          string                    `db:"orderer_phone"`
	ReceiverName          string                    `db:"receiver_name"`
	ReceiverCellPhoneCode string                    `db:"receiver_cellphone_code"`
	ReceiverCellPhone     string                    `db:"receiver_cellphone"`
	ReceiverPhoneCode     string                    `db:"receiver_phone_code"`
	ReceiverPhone         string                    `db:"receiver_phone"`
	ReceiverPostCode      string                    `db:"receiver_postcode"`
	ReceiverAddress       string                    `db:"receiver_address"`
	ShopName              string                    `db:"shop_name"`
	CustomerId            string                    `db:"customer_id"`
	OrderedAt             string                    `db:"ordered_at"`
	CreatedAt             time.Time                 `db:"created_at"`
	UpdatedAt             time.Time                 `db:"updated_at"`
	ItemId                vo.OrderInformationItemId `db:"item_id"`
	Price                 int                       `db:"price"`
	Amount                int                       `db:"amount"`
	ProductId             string                    `db:"product_id"`
	ProductBarcode        string                    `db:"product_barcode"`
	ProductCustomCode     string                    `db:"product_custom_code"`
	ProductName           string                    `db:"product_name"`
	ProductLink           string                    `db:"product_link"`
	ProductImageUrl       string                    `db:"product_image_url"`
	IsInspected           bool                      `db:"is_inspected"`
	InspectedAmount       int                       `db:"inspected_amount"`
	ItemCreatedAt         time.Time                 `db:"item_created_at"`
	ItemUpdatedAt         time.Time                 `db:"item_updated_at"`
}

const orderInformationView = "ORDER_INFORMATION_view_order_information"

func NewViewOrderInformationRepository(config *Config) (*ViewOrderInformationRepository, error) {
	db, err := sqlx.Connect("mysql", config.GetDSN())
	if err != nil {
		return nil, err
	}
	return &ViewOrderInformationRepository{
		db:     db,
		config: config,
	}, nil
}

func (r *ViewOrderInformationRepository) checkAndReconnect() error {
	if err := r.db.Ping(); err != nil {
		db, err := sqlx.Connect("mysql", r.config.GetDSN())
		if err != nil {
			return err
		}
		r.db = db
	}
	return nil
}

func (r *ViewOrderInformationRepository) FindById(id vo.OrderInformationId) (*view_order_information.OrderInformation, error) {
	var err error

	if err := r.checkAndReconnect(); err != nil {
		return nil, err
	}

	rows := []*viewOrderInformationRow{}
	query := `SELECT * FROM ` + orderInformationView + ` WHERE id = ?`
	if err := r.db.Select(&rows, query, id); err != nil {
		return nil, err
	}

	views, err := r.rowsToViews(rows)
	if err != nil {
		return nil, err
	}

	if len(views) == 0 {
		return nil, nil
	}

	return views[0], nil
}

func (r *ViewOrderInformationRepository) FindByCriteria(criteria *search.Criteria) ([]*view_order_information.OrderInformation, *search.PagingMeta, error) {
	var err error

	if err := r.checkAndReconnect(); err != nil {
		return nil, nil, err
	}

	query := `SELECT * FROM ` + orderInformationView + ` WHERE 1=1`

	whereClause, args := buildWhereClause(criteria.Filters)
	if whereClause != nil {
		query += ` AND ` + strings.Join(whereClause, ` AND `)
	}

	if criteria.Page != nil && criteria.Page.Cursor != "" {
		if criteria.Sort.Order == search.Ascending {
			query += ` AND id > '` + criteria.Page.Cursor + `'`
		} else {
			query += ` AND id < '` + criteria.Page.Cursor + `'`
		}
	}

	if criteria.Sort != nil {
		if criteria.Sort.Order == "" {
			criteria.Sort.Order = search.Descending
		}

		if criteria.Sort.Field == "" {
			criteria.Sort.Field = "id"
		}

		query += ` ORDER BY ` + criteria.Sort.Field + ` ` + criteria.Sort.Order.String() + `, id ` + criteria.Sort.Order.String()
	}

	if criteria.Page != nil && criteria.Page.PageSize > 0 {
		if criteria.Page.Page > 0 {
			offset := strconv.Itoa(criteria.Page.PageSize * (criteria.Page.Page - 1))
			query += ` LIMIT ` + strconv.Itoa(criteria.Page.PageSize) + ` OFFSET ` + offset
		} else {
			query += ` LIMIT ` + strconv.Itoa(criteria.Page.PageSize)
		}
	}

	rows := []*viewOrderInformationRow{}
	if err := r.db.Select(&rows, query, args...); err != nil {
		return nil, nil, err
	}

	orderInformations, err := r.rowsToViews(rows)
	if err != nil {
		return nil, nil, err
	}

	if len(orderInformations) == 0 {
		return nil, nil, nil
	}

	var totalCount int
	totalCountQuery := `SELECT COUNT(*) FROM ` + orderInformationView + ` WHERE 1=1 GROUP BY id`
	if len(whereClause) > 0 {
		totalCountQuery += ` AND ` + strings.Join(whereClause, ` AND `)
	}

	if err := r.db.Get(&totalCount, totalCountQuery, args...); err != nil {
		return nil, nil, err
	}

	pagingMeta := &search.PagingMeta{
		TotalCount: totalCount,
	}

	if criteria.Page != nil {
		if criteria.Page.Page > 0 {
			pagingMeta.CurrentPage = criteria.Page.Page
			pagingMeta.TotalPages = calculateTotalPage(totalCount, criteria.Page.PageSize)
		} else {
			pagingMeta.NextCursor = orderInformations[len(orderInformations)-1].Id.String()
		}

		if criteria.Page.PageSize > 0 {
			pagingMeta.Size = criteria.Page.PageSize
		}
	}

	return orderInformations, pagingMeta, nil
}

func (r *ViewOrderInformationRepository) rowsToViews(rows []*viewOrderInformationRow) ([]*view_order_information.OrderInformation, error) {
	var err error

	orders := make(map[vo.OrderInformationId]*view_order_information.OrderInformation)

	for _, row := range rows {
		if _, ok := orders[row.Id]; !ok {
			var ordererEmail *vo.Email
			var ordererCellPhone *vo.Phone
			var ordererPhone *vo.Phone
			var receiverCellPhone *vo.Phone
			var receiverPhone *vo.Phone

			if row.OrdererEmail != "" {
				ordererEmail, err = vo.NewEmail(row.OrdererEmail)
				if err != nil {
					return nil, err
				}
			}

			if row.OrdererCellPhone != "" {
				phone := vo.NewPhone(row.OrdererCellPhoneCode, row.OrdererCellPhone)
				ordererCellPhone = &phone
			}

			if row.OrdererPhone != "" {
				phone := vo.NewPhone(row.OrdererPhoneCode, row.OrdererPhone)
				ordererPhone = &phone
			}

			if row.ReceiverCellPhone != "" {
				phone := vo.NewPhone(row.ReceiverCellPhoneCode, row.ReceiverCellPhone)
				receiverCellPhone = &phone
			}

			if row.ReceiverPhone != "" {
				phone := vo.NewPhone(row.ReceiverPhoneCode, row.ReceiverPhone)
				receiverPhone = &phone
			}

			orders[row.Id] = &view_order_information.OrderInformation{
				Id:                row.Id,
				WorkspaceId:       row.WorkspaceId,
				MemberId:          row.MemberId,
				OrderId:           row.OrderId,
				Code:              row.Code,
				Courier:           row.Courier,
				Timezone:          row.Timezone,
				OrdererName:       row.OrdererName,
				OrdererEmail:      ordererEmail,
				OrdererCellPhone:  ordererCellPhone,
				OrdererPhone:      ordererPhone,
				ReceiverName:      row.ReceiverName,
				ReceiverCellPhone: receiverCellPhone,
				ReceiverPhone:     receiverPhone,
				ReceiverPostCode:  row.ReceiverPostCode,
				ReceiverAddress:   row.ReceiverAddress,
				ShopName:          row.ShopName,
				CustomerId:        row.CustomerId,
				OrderedAt:         row.OrderedAt,
				CreatedAt:         row.CreatedAt,
				UpdatedAt:         row.UpdatedAt,
			}
		}

		if orders[row.Id].Items == nil {
			orders[row.Id].Items = make([]view_order_information.OrderInformationItem, 0)
		}

		orders[row.Id].Items = append(orders[row.Id].Items, view_order_information.OrderInformationItem{
			Id:                row.ItemId,
			Price:             row.Price,
			Amount:            row.Amount,
			ProductId:         row.ProductId,
			ProductBarcode:    row.ProductBarcode,
			ProductCustomCode: row.ProductCustomCode,
			ProductName:       row.ProductName,
			ProductLink:       row.ProductLink,
			ProductImageUrl:   row.ProductImageUrl,
			IsInspected:       row.IsInspected,
			InspectedAmount:   row.InspectedAmount,
			CreatedAt:         row.ItemCreatedAt,
			UpdatedAt:         row.ItemUpdatedAt,
		})
	}

	if len(orders) == 0 {
		return nil, nil
	}

	views := make([]*view_order_information.OrderInformation, 0, len(orders))
	for _, order := range orders {
		views = append(views, order)
	}

	return views, nil
}
