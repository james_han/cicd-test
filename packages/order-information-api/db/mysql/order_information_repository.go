package mysql

import (
	"database/sql"
	"rp-order-information-api/modules/order_information"
	"rp-order-information-api/modules/shared/vo"
	"time"

	"github.com/jmoiron/sqlx"
)

var _ order_information.OrderInfomationRepository = (*OrderInformationRepository)(nil)

type OrderInformationRepository struct {
	db     *sqlx.DB
	config *Config
}

type orderInformationRow struct {
	Id                    vo.OrderInformationId `db:"id"`
	WorkspaceId           vo.WorkspaceId        `db:"workspace_id"`
	MemberId              vo.MemberId           `db:"member_id"`
	OrderId               string                `db:"order_id"`
	Code                  string                `db:"code"`
	Courier               string                `db:"courier"`
	Timezone              string                `db:"timezone"`
	OrdererName           string                `db:"orderer_name"`
	OrdererEmail          string                `db:"orderer_email"`
	OrdererCellPhoneCode  string                `db:"orderer_cellphone_code"`
	OrdererCellPhone      string                `db:"orderer_cellphone"`
	OrdererPhoneCode      string                `db:"orderer_phone_code"`
	OrdererPhone          string                `db:"orderer_phone"`
	ReceiverName          string                `db:"receiver_name"`
	ReceiverCellPhoneCode string                `db:"receiver_cellphone_code"`
	ReceiverCellPhone     string                `db:"receiver_cellphone"`
	ReceiverPhoneCode     string                `db:"receiver_phone_code"`
	ReceiverPhone         string                `db:"receiver_phone"`
	ReceiverPostCode      string                `db:"receiver_postcode"`
	ReceiverAddress       string                `db:"receiver_address"`
	ShopName              string                `db:"shop_name"`
	CustomerId            string                `db:"customer_id"`
	OrderedAt             string                `db:"ordered_at"`
	CreatedAt             time.Time             `db:"created_at"`
	UpdatedAt             time.Time             `db:"updated_at"`
}

type orderInformationItemRow struct {
	Id                vo.OrderInformationItemId `db:"id"`
	OrderId           vo.OrderInformationId     `db:"order_id"`
	Price             int                       `db:"price"`
	Amount            int                       `db:"amount"`
	ProductId         string                    `db:"product_id"`
	ProductBarcode    string                    `db:"product_barcode"`
	ProductCustomCode string                    `db:"product_custom_code"`
	ProductName       string                    `db:"product_name"`
	ProductLink       string                    `db:"product_link"`
	ProductImageUrl   string                    `db:"product_image_url"`
	IsInspected       bool                      `db:"is_inspected"`
	InspectedAmount   int                       `db:"inspected_amount"`
	CreatedAt         time.Time                 `db:"created_at"`
	UpdatedAt         time.Time                 `db:"updated_at"`
}

const orderInformationTable = "ORDER_INFORMATION_order_information"
const orderInformationItemTable = "ORDER_INFORMATION_order_information_item"

func NewOrderInformationRepository(config *Config) (*OrderInformationRepository, error) {
	db, err := sqlx.Connect("mysql", config.GetDSN())
	if err != nil {
		return nil, err
	}
	return &OrderInformationRepository{
		db:     db,
		config: config,
	}, nil
}

func (r *OrderInformationRepository) checkAndReconnect() error {
	if err := r.db.Ping(); err != nil {
		db, err := sqlx.Connect("mysql", r.config.GetDSN())
		if err != nil {
			return err
		}
		r.db = db
	}
	return nil
}

func (r *OrderInformationRepository) Save(orderInformation order_information.OrderInformation) error {
	if err := r.checkAndReconnect(); err != nil {
		return err
	}

	orderInformationRow, orderInformationItemRows := entityToRow(orderInformation)

	tx, err := r.db.Beginx()
	if err != nil {
		return err
	}

	query := `INSERT INTO ` + orderInformationTable + `(id, workspace_id, member_id, order_id, code, courier, timezone, orderer_name, orderer_email, orderer_cellphone_code, orderer_cellphone, orderer_phone_code, orderer_phone, receiver_name, receiver_cellphone_code, receiver_cellphone, receiver_phone_code, receiver_phone, receiver_postcode, receiver_address, shop_name, customer_id, ordered_at, created_at, updated_at) VALUES(:id, :workspace_id, :member_id, :order_id, :code, :courier, :timezone, :orderer_name, :orderer_email, :orderer_cellphone_code, :orderer_cellphone, :orderer_phone_code, :orderer_phone, :receiver_name, :receiver_cellphone_code, :receiver_cellphone, :receiver_phone_code, :receiver_phone, :receiver_postcode, :receiver_address, :shop_name, :customer_id, :ordered_at, :created_at, :updated_at) ON DUPLICATE KEY UPDATE workspace_id = VALUES(workspace_id), member_id = VALUES(member_id), order_id = VALUES(order_id), code = VALUES(code), courier = VALUES(courier), timezone = VALUES(timezone), orderer_name = VALUES(orderer_name), orderer_email = VALUES(orderer_email), orderer_phone = VALUES(orderer_phone), receiver_name = VALUES(receiver_name), receiver_cellphone = VALUES(receiver_cellphone), receiver_phone = VALUES(receiver_phone), receiver_postcode = VALUES(receiver_postcode), receiver_address = VALUES(receiver_address), shop_name = VALUES(shop_name), customer_id = VALUES(customer_id), ordered_at = VALUES(ordered_at), updated_at = VALUES(updated_at)`

	if _, err = tx.NamedExec(query, orderInformationRow); err != nil {
		tx.Rollback()
		return err
	}

	itemInsertQuery := `INSERT INTO ` + orderInformationItemTable + `( id, order_id, price, amount, product_id, product_barcode, product_custom_code, product_name, product_link, product_image_url, is_inspected, inspected_amount, created_at, updated_at) VALUES(:id, :order_id, :price, :amount, :product_id, :product_barcode, :product_custom_code, :product_name, :product_link, :product_image_url, :is_inspected, :inspected_amount, :created_at, :updated_at)
		ON DUPLICATE KEY UPDATE price = VALUES(price), amount = VALUES(amount), product_id = VALUES(product_id), product_barcode = VALUES(product_barcode), product_custom_code = VALUES(product_custom_code), product_name = VALUES(product_name), product_link = VALUES(product_link), product_image_url = VALUES(product_image_url), is_inspected = VALUES(is_inspected), inspected_amount = VALUES(inspected_amount), updated_at = VALUES(updated_at)`

	stmt, err := tx.PrepareNamed(itemInsertQuery)
	if err != nil {
		tx.Rollback()
		return err
	}

	for _, item := range orderInformationItemRows {
		_, err = stmt.Exec(item)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	if err = tx.Commit(); err != nil {
		tx.Rollback()
		return err
	}

	return nil
}

func entityToRow(orderInformation order_information.OrderInformation) (*orderInformationRow, []*orderInformationItemRow) {
	var email string
	var ordererCellPhoneCode string
	var ordererCellPhone string
	var ordererPhoneCode string
	var ordererPhone string
	var receiverCellPhoneCode string
	var receiverCellPhone string
	var receiverPhoneCode string
	var receiverPhone string

	if orderInformation.OrdererEmail != nil {
		email = orderInformation.OrdererEmail.String()
	}

	if orderInformation.OrdererCellPhone != nil {
		ordererCellPhoneCode = orderInformation.OrdererCellPhone.Code()
		ordererCellPhone = orderInformation.OrdererCellPhone.Number()
	}

	if orderInformation.OrdererPhone != nil {
		ordererPhoneCode = orderInformation.OrdererPhone.Code()
		ordererPhone = orderInformation.OrdererPhone.Number()
	}

	if orderInformation.ReceiverCellPhone != nil {
		receiverCellPhoneCode = orderInformation.ReceiverCellPhone.Code()
		receiverCellPhone = orderInformation.ReceiverCellPhone.Number()
	}

	if orderInformation.ReceiverPhone != nil {
		receiverPhoneCode = orderInformation.ReceiverPhone.Code()
		receiverPhone = orderInformation.ReceiverPhone.Number()
	}

	orderInformationRow := &orderInformationRow{
		Id:                    orderInformation.Id,
		WorkspaceId:           orderInformation.WorkspaceId,
		MemberId:              orderInformation.MemberId,
		OrderId:               orderInformation.OrderId,
		Code:                  orderInformation.Code,
		Courier:               orderInformation.Courier,
		Timezone:              orderInformation.Timezone,
		OrdererName:           orderInformation.OrdererName,
		OrdererEmail:          email,
		OrdererCellPhoneCode:  ordererCellPhoneCode,
		OrdererCellPhone:      ordererCellPhone,
		OrdererPhoneCode:      ordererPhoneCode,
		OrdererPhone:          ordererPhone,
		ReceiverName:          orderInformation.ReceiverName,
		ReceiverCellPhoneCode: receiverCellPhoneCode,
		ReceiverCellPhone:     receiverCellPhone,
		ReceiverPhoneCode:     receiverPhoneCode,
		ReceiverPhone:         receiverPhone,
		ReceiverPostCode:      orderInformation.ReceiverPostCode,
		ReceiverAddress:       orderInformation.ReceiverAddress,
		ShopName:              orderInformation.ShopName,
		CustomerId:            orderInformation.CustomerId,
		OrderedAt:             orderInformation.OrderedAt,
		CreatedAt:             orderInformation.CreatedAt,
		UpdatedAt:             orderInformation.UpdatedAt,
	}

	orderInformationItemRows := make([]*orderInformationItemRow, 0, len(orderInformation.Items))
	for _, item := range orderInformation.Items {
		orderInformationItemRows = append(orderInformationItemRows, &orderInformationItemRow{
			Id:                item.Id,
			OrderId:           orderInformation.Id,
			Price:             item.Price,
			Amount:            item.Amount,
			ProductId:         item.ProductId,
			ProductBarcode:    item.ProductBarcode,
			ProductCustomCode: item.ProductCustomCode,
			ProductName:       item.ProductName,
			ProductLink:       item.ProductLink,
			ProductImageUrl:   item.ProductImageUrl,
			IsInspected:       item.IsInspected,
			InspectedAmount:   item.InspectedAmount,
			CreatedAt:         item.CreatedAt,
			UpdatedAt:         item.UpdatedAt,
		})
	}

	return orderInformationRow, orderInformationItemRows
}

func (r *OrderInformationRepository) DeleteById(id vo.OrderInformationId) error {
	if err := r.checkAndReconnect(); err != nil {
		return err
	}

	tx, err := r.db.Beginx()
	if err != nil {
		return err
	}

	if _, err = tx.Exec(`DELETE FROM `+orderInformationItemTable+` WHERE order_id = ?`, id); err != nil {
		tx.Rollback()
		return err
	}

	if _, err = tx.Exec(`DELETE FROM `+orderInformationTable+` WHERE id = ?`, id); err != nil {
		tx.Rollback()
		return err
	}

	if err = tx.Commit(); err != nil {
		tx.Rollback()
		return err
	}

	return nil
}

func (r *OrderInformationRepository) FindById(id vo.OrderInformationId) (*order_information.OrderInformation, error) {
	var err error

	query := `SELECT * FROM ` + orderInformationTable + ` WHERE id = ?`
	orderInformationRow := &orderInformationRow{}
	if err = r.db.Get(orderInformationRow, query, id); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	itemQuery := `SELECT * FROM ` + orderInformationItemTable + ` WHERE order_id = ?`
	orderInformationItemRows := []orderInformationItemRow{}
	if err = r.db.Select(&orderInformationItemRows, itemQuery, id); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	return r.rowToEntity(orderInformationRow, orderInformationItemRows)
}

func (r *OrderInformationRepository) rowToEntity(orderInformationRow *orderInformationRow, orderInformationItemRows []orderInformationItemRow) (*order_information.OrderInformation, error) {
	var err error

	var ordererEmail *vo.Email
	var ordererCellPhone *vo.Phone
	var ordererPhone *vo.Phone
	var receiverCellPhone *vo.Phone
	var receiverPhone *vo.Phone

	if orderInformationRow.OrdererEmail != "" {
		ordererEmail, err = vo.NewEmail(orderInformationRow.OrdererEmail)
		if err != nil {
			return nil, err
		}
	}

	if orderInformationRow.OrdererCellPhoneCode != "" && orderInformationRow.OrdererCellPhone != "" {
		phone := vo.NewPhone(orderInformationRow.OrdererCellPhoneCode, orderInformationRow.OrdererCellPhone)
		ordererCellPhone = &phone
	}

	if orderInformationRow.OrdererPhoneCode != "" && orderInformationRow.OrdererPhone != "" {
		phone := vo.NewPhone(orderInformationRow.OrdererPhoneCode, orderInformationRow.OrdererPhone)
		ordererPhone = &phone
	}

	if orderInformationRow.ReceiverCellPhoneCode != "" && orderInformationRow.ReceiverCellPhone != "" {
		phone := vo.NewPhone(orderInformationRow.ReceiverCellPhoneCode, orderInformationRow.ReceiverCellPhone)
		receiverCellPhone = &phone
	}

	if orderInformationRow.ReceiverPhoneCode != "" && orderInformationRow.ReceiverPhone != "" {
		phone := vo.NewPhone(orderInformationRow.ReceiverPhoneCode, orderInformationRow.ReceiverPhone)
		receiverPhone = &phone
	}

	orderInformation := &order_information.OrderInformation{
		Id:                orderInformationRow.Id,
		WorkspaceId:       orderInformationRow.WorkspaceId,
		MemberId:          orderInformationRow.MemberId,
		OrderId:           orderInformationRow.OrderId,
		Code:              orderInformationRow.Code,
		Courier:           orderInformationRow.Courier,
		Timezone:          orderInformationRow.Timezone,
		OrdererName:       orderInformationRow.OrdererName,
		OrdererEmail:      ordererEmail,
		OrdererCellPhone:  ordererCellPhone,
		OrdererPhone:      ordererPhone,
		ReceiverName:      orderInformationRow.ReceiverName,
		ReceiverCellPhone: receiverCellPhone,
		ReceiverPhone:     receiverPhone,
		ReceiverPostCode:  orderInformationRow.ReceiverPostCode,
		ReceiverAddress:   orderInformationRow.ReceiverAddress,
		ShopName:          orderInformationRow.ShopName,
		CustomerId:        orderInformationRow.CustomerId,
		OrderedAt:         orderInformationRow.OrderedAt,
		CreatedAt:         orderInformationRow.CreatedAt,
		UpdatedAt:         orderInformationRow.UpdatedAt,
	}

	orderInformationItems := make([]order_information.OrderInformationItem, 0, len(orderInformationItemRows))
	for _, itemRow := range orderInformationItemRows {
		orderInformationItems = append(orderInformationItems, order_information.OrderInformationItem{
			Id:                itemRow.Id,
			Price:             itemRow.Price,
			Amount:            itemRow.Amount,
			ProductId:         itemRow.ProductId,
			ProductBarcode:    itemRow.ProductBarcode,
			ProductCustomCode: itemRow.ProductCustomCode,
			ProductName:       itemRow.ProductName,
			ProductLink:       itemRow.ProductLink,
			ProductImageUrl:   itemRow.ProductImageUrl,
			IsInspected:       itemRow.IsInspected,
			InspectedAmount:   itemRow.InspectedAmount,
			CreatedAt:         itemRow.CreatedAt,
			UpdatedAt:         itemRow.UpdatedAt,
		})
	}
	orderInformation.Items = orderInformationItems

	return orderInformation, nil
}
