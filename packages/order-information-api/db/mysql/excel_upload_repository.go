package mysql

import (
	"rp-order-information-api/modules/excel_upload"
	"rp-order-information-api/modules/shared/search"

	"github.com/jmoiron/sqlx"
)

type ExcelUploadRepository struct {
	db     sqlx.DB
	config Config
}

func NewExcelUploadRepository(config Config) (*ExcelUploadRepository, error) {
	db, err := sqlx.Connect("mysql", config.GetDSN())
	if err != nil {
		return nil, err
	}

	return &ExcelUploadRepository{
		db:     *db,
		config: config,
	}, nil
}

func (repo *ExcelUploadRepository) checkAndReconnect() error {
	if err := repo.db.Ping(); err != nil {
		repo.db.Close()
		db, err := sqlx.Connect("mysql", repo.config.GetDSN())
		if err != nil {
			return err
		}

		repo.db = *db
	}

	return nil
}

func (repo *ExcelUploadRepository) Save(excelUpload excel_upload.ExcelUpload) error {
	if err := repo.checkAndReconnect(); err != nil {
		return err
	}

	return nil
}

func (repo *ExcelUploadRepository) FindByCriteria(criteria search.Criteria) ([]excel_upload.ExcelUpload, *search.PagingMeta, error) {
	if err := repo.checkAndReconnect(); err != nil {
		return nil, nil, err
	}

	return nil, nil, nil
}
