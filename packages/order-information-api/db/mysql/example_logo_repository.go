package mysql

import (
	"database/sql"
	"errors"
	"fmt"
	"math"
	"rp-order-information-api/modules/logo"
	"rp-order-information-api/modules/shared/search"
	"rp-order-information-api/modules/shared/vo"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
)

var _ logo.LogoRepository = (*LogoRepository)(nil)

type LogoRepository struct {
	db *sqlx.DB
}

const logoTable = "VIDEO_logo"

type logoRow struct {
	Id          vo.LogoId      `db:"id"`
	WorkspaceId vo.WorkspaceId `db:"workspace_id"`
	IsActive    bool           `db:"is_active"`
	FileId      vo.FileId      `db:"file_id"`
	CreatedAt   time.Time      `db:"created_at"`
	UpdatedAt   time.Time      `db:"updated_at"`
}

// DeleteById implements logo.LogoRepository.
func (r *LogoRepository) DeleteById(logo *logo.Logo) error {
	query := fmt.Sprintf("DELETE FROM %s WHERE id = ?", logoTable)
	_, err := r.db.Exec(query, logo.Id)
	return err
}

// FindByCriteria implements logo.LogoRepository.
func (r *LogoRepository) FindByCriteria(criteria *search.Criteria) ([]*logo.Logo, *search.PagingMeta, error) {
	var whereQuery string
	var cursorQuery string
	var sortQuery string
	var pagingQuery string

	// where query
	whereClause, args := buildWhereClause(criteria.Filters)
	if whereClause != nil {
		whereQuery = fmt.Sprintf(" AND %s", strings.Join(whereClause, ` AND `))
	}

	// curosr query
	if criteria.Page != nil && criteria.Page.Cursor != "" {
		if criteria.Sort.Order == search.Ascending {
			cursorQuery = fmt.Sprintf(" AND id > '%s'", criteria.Page.Cursor)
		} else {
			cursorQuery = fmt.Sprintf(" AND id < '%s'", criteria.Page.Cursor)
		}
	}

	// sort query
	if criteria.Sort != nil {
		if criteria.Sort.Order == "" {
			criteria.Sort.Order = search.Descending
		}

		if criteria.Sort.Field == "" {
			criteria.Sort.Field = "id"
		}

		sortQuery = fmt.Sprintf(" ORDER BY %s %s, id %s", criteria.Sort.Field, criteria.Sort.Order.String(), criteria.Sort.Order.String())
	}

	// paging query
	if criteria.Page != nil {
		if criteria.Page.Page > 0 {
			pagingQuery = fmt.Sprintf(" LIMIT %d OFFSET %d", criteria.Page.PageSize, criteria.Page.PageSize*(criteria.Page.Page-1))
		} else {
			pagingQuery = fmt.Sprintf(" LIMIT %d", criteria.Page.PageSize)
		}
	}

	query := fmt.Sprintf(`
		SELECT * FROM %s WHERE 1=1 
			%s
			%s
			%s
			%s
	`, logoTable, whereQuery, cursorQuery, sortQuery, pagingQuery)

	logoRows := []*logoRow{}
	if err := r.db.Select(&logoRows, query, args...); err != nil {
		return nil, nil, err
	}

	var pagingCursor string
	logos := []*logo.Logo{}
	for _, row := range logoRows {
		logo := r.rowToEntity(row)
		logos = append(logos, logo)
		pagingCursor = row.Id.String()
	}

	totalCount, err := countTotalRecords(r.db, logoTable, whereClause, args)
	if err != nil {
		return nil, nil, err
	}

	pagingMeta := &search.PagingMeta{
		TotalCount: totalCount,
	}

	if criteria.Page != nil {
		if criteria.Page.Page > 0 {
			pagingMeta.CurrentPage = criteria.Page.Page
			pagingMeta.TotalPages = int(math.Ceil(float64(pagingMeta.TotalCount) / float64(criteria.Page.PageSize)))
		} else {
			pagingMeta.NextCursor = pagingCursor
		}

		if criteria.Page.PageSize > 0 {
			pagingMeta.Size = criteria.Page.PageSize
		}
	}

	return logos, pagingMeta, nil
}

// FindById implements logo.LogoRepository.
func (r *LogoRepository) FindById(id vo.LogoId) (*logo.Logo, error) {
	var sqlxRow *sqlx.Row

	query := fmt.Sprintf("SELECT * FROM %s  WHERE id = ?", logoTable)
	if sqlxRow = r.db.QueryRowx(query, id); sqlxRow.Err() != nil {
		return nil, sqlxRow.Err()
	}

	var logo logoRow
	if err := sqlxRow.StructScan(&logo); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}
	return r.rowToEntity(&logo), nil
}

// FindByWorkspaceId implements logo.LogoRepository.
func (r *LogoRepository) FindByWorkspaceId(workspaceId vo.WorkspaceId) ([]*logo.Logo, error) {
	query := fmt.Sprintf("SELECT * FROM %s  WHERE workspace_id = ?", logoTable)
	if rows, err := r.db.Queryx(query, workspaceId); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	} else {
		var logos []*logo.Logo
		for rows.Next() {
			var row logoRow
			if err := rows.StructScan(&row); err != nil {
				return nil, err
			}
			logos = append(logos, r.rowToEntity(&row))
		}

		return logos, nil
	}
}

// Save implements logo.LogoRepository.
func (r *LogoRepository) Save(logo *logo.Logo) error {
	row := r.entityToRow(logo)
	query := fmt.Sprintf(`
		INSERT INTO %s 
			( id, workspace_id, is_active, file_id, created_at, updated_at) VALUES 
			(:id, :workspace_id, :is_active, :file_id, :created_at, :updated_at) 
			ON DUPLICATE KEY UPDATE is_active = :is_active, file_id = :file_id, updated_at = :updated_at
	`, logoTable)
	_, err := r.db.NamedExec(query, row)
	return err
}

func (r *LogoRepository) SaveMultiple(logos []*logo.Logo) error {
	tx, err := r.db.Beginx()
	if err != nil {
		return err
	}
	for _, logo := range logos {
		row := r.entityToRow(logo)
		query := fmt.Sprintf(`
			INSERT INTO %s
			( id, workspace_id, is_active, file_id, created_at, updated_at) VALUES	
			(:id, :workspace_id, :is_active, :file_id, :created_at, :updated_at)
			ON DUPLICATE KEY UPDATE is_active = :is_active, file_id = :file_id, updated_at = :updated_at
		`, logoTable)

		if _, err := tx.NamedExec(query, row); err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}

func NewLogoRepository(config *Config) (*LogoRepository, error) {
	db, err := sqlx.Connect("mysql", config.GetDSN())
	if err != nil {
		return nil, err
	}
	return &LogoRepository{
		db: db,
	}, nil
}

func (r *LogoRepository) entityToRow(ent *logo.Logo) logoRow {
	return logoRow{
		Id:          ent.Id,
		WorkspaceId: ent.WorkspaceId,
		IsActive:    ent.IsActive,
		FileId:      ent.FileId,
		CreatedAt:   ent.CreatedAt,
		UpdatedAt:   ent.UpdatedAt,
	}
}

func (r *LogoRepository) rowToEntity(row *logoRow) *logo.Logo {
	return &logo.Logo{
		Id:          row.Id,
		WorkspaceId: row.WorkspaceId,
		IsActive:    row.IsActive,
		FileId:      row.FileId,
		CreatedAt:   row.CreatedAt,
		UpdatedAt:   row.UpdatedAt,
	}
}
