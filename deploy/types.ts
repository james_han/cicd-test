export class Route {
    method: HTTP_METHOD; 
    path: string; 
    handler: string;
    authorizerName: string;
    volume: string;

    /**
     * @param method    One of GET POST PUT DELETE ANY
     * @param path      접속시 매칭될 uri. PathParameter? /books/{id}. Match all subdirectories?\n /members/{proxy+}.
     * @param handler   Lambda handler 경로. 단, 프로젝트 루트를 기준으로 작성하세요.
     * @param volume    볼륨 바인딩 "프로젝트 루트기준 상대경로:람다 바이너리 기준 상대경로" ex) "docs:docs"
     */
    constructor(method:HTTP_METHOD, path:string, handler:string, volume:string|null|undefined = undefined) {
        this.method = method;
        this.path = path;
        this.handler = handler;
        this.volume = volume;
    }

    authorizer(authorizerName:string):Route{
        this.authorizerName = authorizerName;
        return this;
    }

    /** @returns string "httpMethod /path" 포맷의 문자열 반환 (ex: "GET /healthz")  */
    getPathString():string{
        return `${this.method.toUpperCase()} ${this.path}`;
    }
};

export class Authorizer {
    name: string; 
    handler: string;
    cacheTTl: string = "30 seconds";
    constructor(name:string, handler:string) {
        this.name = name;
        this.handler = handler;
    }
};

export enum HTTP_METHOD {
    GET = "GET",
    POST = "POST",
    DELETE = "DELETE",
    PUT = "PUT",
    ANY = "ANY",
}

export interface Functions {
    authorizers:Array<Authorizer>;
    routes:Array<Route>;
}
