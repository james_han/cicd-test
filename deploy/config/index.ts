import { config as defaultConfig } from './config.default';
import { config as localConfig } from './config.local';
import { config as devConfig } from './config.dev';
import { config as stageConfig } from './config.stage';
import { config as prodConfig } from './config.prod';
import { readFileSync } from "fs";
import { parse } from "yaml";
import * as types from "../types";


interface Config {
  serviceName: string;
  stage: string;
  domain: string;
  env: Record<string, string>;
  tags: Record<string, string>;
  runtime: string;
  memorySize: Number,
}

const configs: Record<string, Partial<Config>> = {
  default: defaultConfig,
  local: localConfig,
  dev: devConfig,
  stage: stageConfig,
  prod: prodConfig,
};

export function getConfigByStage(stage: string): Config {
  if (!configs.hasOwnProperty(stage)) {
    // throw new Error(`Invalid stage name: ${stage}`);
    return { ...defaultConfig, ...configs['local'] } as Config;
  }

  return { ...defaultConfig, ...configs[stage] } as Config;
}

export function getFunctions():types.Functions{
  let yamlData = ""
  try{
    yamlData = readFileSync(__dirname+"/../functions.yaml", "utf-8");
  }catch(e){
    yamlData = readFileSync("deploy/functions.yaml", "utf-8");
  }
  const parsedData = parse(yamlData);

  return {
    authorizers: parsedData.authorizers.map(
      (a: any) => new types.Authorizer(a.name, a.handler)
    ),
    routes: parsedData.routes.map((r: any) => {
      const route = new types.Route(types.HTTP_METHOD[r.method], r.path, r.handler);
      if (r.authorizerName) {
        route.authorizer(r.authorizerName);
      }
      return route;
    }),
  };
}

console.log(getFunctions());