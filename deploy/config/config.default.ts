export const config = {
    serviceName: "rp-order-information-api",
    stage: "default",
    domain: "api.kyfra.me",
    env : {
        "DB_HOST":"",
        "DB_NAME":"",
        "DB_PORT":"",
        "DB_USER":"",
        "DB_PASS":"",
    },
    tags: {
    },
    runtime:"go1.x",
    memorySize:128,
};
