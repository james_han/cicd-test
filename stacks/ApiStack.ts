import * as sst from "sst/constructs";
import { aws_certificatemanager as acm, aws_apigateway as apigateway, aws_lambda as lambda, aws_dynamodb as ddb, aws_route53 as route53, aws_logs as logs } from "aws-cdk-lib";
import * as AWS from 'aws-sdk';
import { getConfigByStage, getFunctions } from '../deploy/config';

// const acmClient = new AWS.ACM();

// async function getCertificate(domainName) {
//     try {
//       const response = await acmClient.listCertificates().promise();
  
//       if(!response.CertificateSummaryList) {
//         throw Error('인증서가 없슴니다');
//       }
//       const certificateSummary = response.CertificateSummaryList.find(
//         (cert) => cert.DomainName === domainName
//       );
  
//       if (certificateSummary) {
//         const certArn = certificateSummary.CertificateArn;
//         return certArn;
//       }
//       console.log(`No certificate found for domain: ${domainName}`);
//       return null;
//     } catch (error) {
//       console.error(`Error getting certificate for domain: ${domainName}`, error);
//       throw error;
//     }
// }

/** @throws */
function extractSecondaryDomain(domain):string {
    const parsed = new URL(`https://${domain}`);
    const hostname = parsed.hostname;
  
    const parts = hostname.split('.');
    if (parts.length < 2) {
      throw Error("올바른 도메인이 아닙니다.");
    }
  
    const topLevelDomain = parts.pop();
    const secondaryDomain = parts.pop();
  
    return `${secondaryDomain}.${topLevelDomain}`;
  }

export function ApiStack({ stack, app }: sst.StackContext) {
    const stage = app.stage;
    const region = app.region;
    const config = getConfigByStage(stage);

    const hostedZoneName = extractSecondaryDomain(config.domain)

    const hostedZone = route53.HostedZone.fromLookup(stack, "HostedZone", {
        domainName: hostedZoneName,
    });


    const certificate = new acm.Certificate(stack, `Certificate`, {
        domainName: hostedZoneName,
        subjectAlternativeNames: [config.domain],
        validation: acm.CertificateValidation.fromDns(hostedZone),
    });
    
    
    let conf = {
        authorizers: {},
        routes: {},
        customDomain: {
            isExternalDomain: true,
            domainName: config.domain,
            cdk: {
                certificate: certificate,
            },
        },
    }

    const functions = getFunctions()

    functions.authorizers.forEach(_authorizer => {
        conf["authorizers"][_authorizer.name] = {
            type:"lambda",
            function: new sst.Function(stack, _authorizer.name, {
                handler: _authorizer.handler,
            }),
            responseTypes:["simple"],
            resultsCacheTtl: _authorizer.cacheTTl,
            // identitySource: ["$request.header.Authorization"],
        } 
    });
    
    functions.routes.forEach(_route => {
        let funcId =  _route.getPathString().replace(/\W/g, "")
        let funcOption = {
            handler: _route.handler,
        }
        if(_route.volume){
            let volume = _route.volume.split(":");
            if (volume.length !== 2) {
                throw new Error("볼륨 포맷 에러");
            }
            funcOption["copyFiles"] = [{ from: volume[0], to: volume[1] }];
        }
        conf["routes"][_route.getPathString()] = {
            "authorizer" : _route.authorizerName,
            "function" : new sst.Function(stack, funcId, funcOption),
        } 
    });

    console.log(conf);

    // conf["accessLog"] = {
    //     retention: "one_week",
    // }
    
    const api = new sst.Api(stack, "Api", conf);

    api.setCors({
        allowHeaders: ["*"],
        allowMethods: ["ANY"],
        allowOrigins: ["*"],
        maxAge: "1 day", // ${number} second | ${number} seconds | ${number} minute | ${number} minutes | ${number} hour | ${number} hours | ${number} day | ${number} days
    });

    const recordSet = new route53.CfnRecordSet(stack, 'NewRecordSet', {
        name: config.domain, // Replace with your desired domain name
        type: route53.RecordType.CNAME,
        region: `${region}`,
        ttl: '300',
        setIdentifier: `${app.name}-${region}`,
        hostedZoneId: hostedZone.hostedZoneId,
        resourceRecords: [ api.cdk.domainName?.regionalDomainName || `${api.httpApiId}.execute-api.${region}.amazonaws.com` ],
    });

    this.getAllFunctions().forEach(func => {
        new logs.LogGroup(this, `${func.node.id}-LogGroup`, {
            logGroupName: `/aws/lambda/${func.functionName}`,
            retention: logs.RetentionDays.THREE_MONTHS,
            removalPolicy: this.defaultRemovalPolicy
        })
    })

    // Output API domain name and URL
    this.addOutputs({
        "apiGatewayDomain" : api.cdk.domainName?.regionalDomainName,
        "httpApiArn" : api.httpApiArn,
        "httpApiId" : api.httpApiId,
        "url" : api.url,
    });

    return {
        api,
    }
}

