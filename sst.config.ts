import { SSTConfig } from "sst";
import { getConfigByStage } from "./deploy/config";
import { ApiStack } from "./stacks/ApiStack";

export default {
  config(_input) {
    return {
      name: getConfigByStage(_input.stage).serviceName,
      region: _input.region ?? "us-east-1",
    };
  },
  stacks(app) {
    app.setDefaultRemovalPolicy("destroy");
    app.setDefaultFunctionProps({
      runtime: "go1.x",
      memorySize: 128,
      timeout: 10,
    });
    app.stack(ApiStack);
  }
} satisfies SSTConfig;
