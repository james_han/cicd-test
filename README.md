1. DB 접근 방법 - Jay SQLX 활용이 깔끔한듯 (레이어 침범 정도는 ok)
2. 파라미터 바인딩 - query 태그, 공통 함수 활용, govalidator 활용 

디렉토리 구성
```
/modules
    /도메인명
        /domain.go
        /events.go -> EventStructure
        /dto.go
        /interfaces.go
        /도메인명_service.go
    /shared (공통VO, 페이징, 검색 조건, 유틸 관련)
/web
    /lambda_http
        /functions.go
        /types.go
        /authorizer
            /handler.go
        /internal_authorizer
            /handler.go
        /도메인명
            /액션1/handler.go
            /액션2/handler.go
            /액션3/handler.go
/db 
    /mysql
        /config.go 
        /function.go 
        /도메인명_repository.go
    /mongodb
        /config.go 
        /function.go 
        /도메인명_repository.go
```